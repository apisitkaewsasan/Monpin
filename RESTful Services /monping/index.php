<?php
     require 'vendor/autoload.php';
   

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
$str_sql = "";
$app = new \Slim\Slim();
$app->get('/list','list_data');
$app->get('/ViewVedio/:parameter','view_vedio');
$app->get('/ViewMenu/:parameter','view_menu');
$app->get('/MainMenu/:parameter','view_mainmenu');
$app->get('/EbookView/:patameter','view_ebook');
$app->get('/ContactView','view_contact');
$app->get('/AboutView','view_about');
$app->get('/SyncScanner/:patameter','view_scanner');
$app->get('/Login','login');
$app->get('/Register_facebook','login_facebook');
$app->get('/AllVedio','all_vedio');
$app->get('/AllEbook','all_ebook');
$app->post('/Register','register');
$app->get('/Location','view_locaion');
$app->get('/Live','live');
$app->get('/CheckIn','view_checkin');
$app->notFound(function () use ($app) {
      echo json_encode(array("retrun_code"=>"99","return_result"=>"Not Found 404 from Server"));
 });
$app->run();
    
    function notFound(){
        
    }


function live(){
    try{
       
            $str_sql_live= "SELECT * FROM `youtube` ";
       
        
        $db = getDB();
        $stmt = $db->query($str_sql_live); 
        $live = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        if($stmt){
            echo json_encode(array("retrun_code"=>"00","live"=>$live));
        }else{
            echo json_encode(array("retrun_code"=>"99","return_result"=>"ไม่พบรายการ"));
        }
        
    }catch (PDOException $e){
        echo json_encode(array("retrun_code"=>"99","return_result_e"=>$e->getMessage()));
    }
}

function view_checkin(){
    try{
        if($_GET['index']==null){
            $str_sql_locaion = "SELECT * FROM `checking` where topic_id ='".$_GET['topic_id']."' order by date_plase desc LIMIT ";
        }else{
            $str_sql_locaion = "SELECT * FROM `checking` where topic_id ='".$_GET['topic_id']."' order by date_plase desc LIMIT ".$_GET['index'];
        }
        
        $db = getDB();
        $stmt = $db->query($str_sql_locaion); 
        $checkin = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        if($stmt){
            echo json_encode(array("retrun_code"=>"00","checkin"=>$checkin,"count_row"=>getcount_page("C")));
        }else{
            echo json_encode(array("retrun_code"=>"99","return_result"=>"ไม่พบรายการ"));
        }
        
    }catch (PDOException $e){
        echo json_encode(array("retrun_code"=>"99","return_result_e"=>$e->getMessage()));
    }
}

function view_locaion(){
    try{
        if($_GET['index']==null){
            $str_sql_locaion = "SELECT * FROM `history_location` where username ='".$_GET['username']."' order by date_plase desc ";
        }else{
            $str_sql_locaion = "SELECT * FROM `history_location` where username ='".$_GET['username']."' order by date_plase desc LIMIT ".$_GET['index'];
        }
        
        $db = getDB();
        $stmt = $db->query($str_sql_locaion); 
        $location = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        if($stmt){
            echo json_encode(array("retrun_code"=>"00","location"=>$location,"count_row"=>getcount_page("H")));
        }else{
            echo json_encode(array("retrun_code"=>"99","return_result"=>"ไม่พบรายการ"));
        }
        
    }catch (PDOException $e){
        echo json_encode(array("retrun_code"=>"99","return_result_e"=>$e->getMessage()));
    }
}

function view_mainmenu($parameter){
    try{
        if($_GET['index']==null){
            $str_sql_vedio = "SELECT topic.topic_id,topic.topic_name,content_view.content1,content_view.content2,topic.topic_postdate,config.key as api_key,vedio.youtube_key,vedio.time,
            (select picture.path_image from picture where picture.ref_topic_id = topic.topic_id and picture.pic_type  IN ('G') ORDER by RAND() limit 1) as image_rand,
            (select picture.path_image from picture where picture.ref_topic_id = topic.topic_id and picture.pic_type  = '".$parameter."' ORDER by RAND() limit 1) as path_image,contract.contract_name
            ,topic.topic_view_count ,contract.gps_lat,contract.gps_long,contract.contract_address
            FROM topic
             INNER JOIN content_view ON content_view.ref_topic_id =topic.topic_id
            INNER JOIN config ON config.dtl_name = 'youtube_api_key'
            LEFT JOIN vedio ON vedio.ref_topic_id = topic.topic_id
            LEFT JOIN contract ON contract.ref_topic_id = topic.topic_id and contract.ref_type = 'G'
             WHERE topic.topic_type  ='$parameter' ";
        }else{
            $str_sql_vedio = "SELECT topic.topic_id,topic.topic_name,content_view.content1,content_view.content2,topic.topic_postdate,config.key as api_key,vedio.youtube_key,vedio.time,
            (select picture.path_image from picture where picture.ref_topic_id = topic.topic_id and picture.pic_type  IN ('G') ORDER by RAND() limit 1) as image_rand,
            (select picture.path_image from picture where picture.ref_topic_id = topic.topic_id and picture.pic_type  = '".$parameter."' ORDER by RAND() limit 1) as path_image,contract.contract_name
            ,topic.topic_view_count ,contract.gps_lat,contract.gps_long,contract.contract_address
            FROM topic
             INNER JOIN content_view ON content_view.ref_topic_id =topic.topic_id
            INNER JOIN config ON config.dtl_name = 'youtube_api_key'
            LEFT JOIN vedio ON vedio.ref_topic_id = topic.topic_id
            LEFT JOIN contract ON contract.ref_topic_id = topic.topic_id and contract.ref_type = 'G'
             WHERE topic.topic_type  ='$parameter' LIMIT ".$_GET['index'];
        }
       

            $db = getDB();
            $stmt = $db->query($str_sql_vedio);
            $menu = $stmt->fetchAll(PDO::FETCH_OBJ);
     
            $db = null;
        echo json_encode(array("retrun_code"=>"00","context_view"=>$menu,"count_row"=>getcount_page($parameter)));
        }catch (PDOException $e){
            echo json_encode(array("retrun_code"=>"99","return_result_e"=>$e->getMessage()));
        }
    
       
}

function list_menu($parameter){
   
    $str_sql_vedio = "SELECT topic.topic_id,topic.topic_name,content_view.content1,content_view.content2,topic.topic_postdate,config.key as api_key,vedio.youtube_key,vedio.time,
    (select picture.path_image from picture where picture.ref_topic_id = topic.topic_id and picture.pic_type  = 'G' ORDER by RAND() limit 1) as image_rand,contract.contract_name,
    (select picture.path_image from picture where picture.ref_topic_id = topic.topic_id  and picture.pic_type  != 'G' ORDER by RAND() limit 1) as path_image
    ,topic.topic_view_count,contract.contract_address
    FROM topic
     INNER JOIN content_view ON content_view.ref_topic_id =topic.topic_id 
    INNER JOIN config ON config.dtl_name = 'youtube_api_key' 
    LEFT JOIN vedio ON vedio.ref_topic_id = topic.topic_id 
    LEFT JOIN contract ON contract.ref_topic_id = topic.topic_id and contract.ref_type = 'G'
     WHERE topic.topic_id ='$parameter' ";

        $db = getDB();
        $stmt = $db->query($str_sql_vedio); 
        $menu = $stmt->fetchAll(PDO::FETCH_OBJ);
 
        $db = null;
      return $menu;
       
}


function all_ebook(){
    try{
        echo json_encode(array("retrun_code"=>"00","ebooks"=>ebook($_GET['index']),"count_row"=>getcount_page("E")));
    }catch (PDOException $e){
       echo json_encode(array("retrun_code"=>"99","return_result_e"=>$e->getMessage()));
    }
}

function all_vedio(){
    try{
        echo json_encode(array("retrun_code"=>"00","vedio"=>vedio($_GET['index']),"count_row"=>getcount_page("V")));
    }catch (PDOException $e){
       echo json_encode(array("retrun_code"=>"99","return_result_e"=>$e->getMessage()));
    }
}

function login_facebook(){
    $out = array();
    $sql = "CALL SP_RESIGTER('".$_GET['fullname']."','".$_GET['username']."','".$_GET['password']."','".$_GET['tel']."','".$_GET['image_url']."','1')";
        $db = getDB();
        $stmt = $db->query($sql); 
        $result = $stmt->fetchAll();
        
        foreach($result as $index){
            
            if($index["retrun_code"]=='99'){
                $out = array("retrun_code"=>$index["retrun_code"],"return_result"=>"Username : is available (มีข้อมูลแล้ว)");
            }else{
                $out = array("retrun_code"=>$index["retrun_code"],"return_result"=>$index["return_result"]);
            }
           
        }
    
   // $outp = $stmt->fetchAll(PDO::FETCH_OBJ);
    echo json_encode($out);
}

function login(){
    $sql = "SELECT account.id,account.username,account.fullname,account.password,account.tel,picture.path_image from account 
    INNER JOIN topic on topic.topic_name = account.username
    LEFT JOIN picture on picture.ref_topic_id = topic.topic_id where account.username = '".$_GET['username']."' and account.password = '".$_GET['password']."'";
    $db = getDB();
    $stmt = $db->query($sql); 
    $result = $stmt->fetchAll(PDO::FETCH_OBJ);

    if($result){
        $out = array("retrun_code"=>"00","return_result"=>"login สำเร็จ","retult"=>$result);
   }else{
    $out = array("retrun_code"=>"99","return_result"=>"incorrect username or password. try again","retult"=>array());
   }
            
    
    echo json_encode($out);
}


function register(){
    

//$path_link=$newname;
    $file_path =convert_filename($_FILES['image'],$_FILES['image']['name']);
    $out = array();
    if(move_uploaded_file($_FILES['image']['tmp_name'],$file_path)){
        
        $sql = "CALL SP_RESIGTER('".$_GET['username']."','".$_GET['fullname']."','".$_GET['password']."','".$_GET['tel']."','".$file_path."','0')";
        $db = getDB();
        $stmt = $db->query($sql); 
        $result = $stmt->fetchAll();
        
        foreach($result as $index){
            
            if($index["retrun_code"]=='99'){
                unlink($file_path);
                $out = array("retrun_code"=>$index["retrun_code"],"return_result"=>"Username : is available (มีข้อมูลแล้ว)");
            }else{
                $out = array("retrun_code"=>$index["retrun_code"],"return_result"=>$index["return_result"]);
            }
           
        }

    }else{
        $out = array("retrun_code"=>"99","return_result"=>"Error  upload file");
    }

    
   // $outp = $stmt->fetchAll(PDO::FETCH_OBJ);
    echo json_encode($out);
}


function view_scanner($patameter){
    try{
        $str_sql_scan = "SELECT topic.topic_id,topic.topic_name,topic.topic_type,config.dtl_name FROM  topic
        LEFT JOIN config ON config.key = topic.topic_id 
        WHERE topic.topic_id = '".base64_decode(base64_decode($patameter))."' and topic.topic_type not in('G','S','#','P')";

        $db = getDB();
        $stmt = $db->query($str_sql_scan); 
        $scan = $stmt->fetchAll(PDO::FETCH_OBJ);
       
        if(count($scan)>0){
           Insert_Checking(base64_decode(base64_decode($patameter)),$_GET['username']);
            echo json_encode(array("retrun_code"=>"00","view_scanner"=>$scan));
        }else{
            echo json_encode(array("retrun_code"=>"99","return_result_e"=>"ไม่พบรายการกรุณาลองใหม่"));
        }
    }catch (PDOException $e){
        echo json_encode(array("retrun_code"=>"99","return_result_e"=>$e->getMessage()));
    }
}

function Insert_Checking($top_id,$username){
    
    $check = "SELECT * FROM `place` WHERE place.ref_top_id = '".$top_id."' and place.ref_account =  '".$username."'";
    $db = getDB();
    $stmt_check = $db->query($check);
    $scan_check = $stmt_check->fetchAll(PDO::FETCH_OBJ);
    
    if(count($scan_check)< 1){
        $Insert_Checking = "INSERT INTO `place` (`id_place`, `ref_top_id`, `ref_account`, `date_plase`) VALUES (NULL, '".$top_id."', '".$username."',NOW());";
        $stmt = $db->query($Insert_Checking);
    }
    $db = null;
  
}

function view_about(){
    try{
        $str_sql_about = "SELECT topic.topic_id,content_view.content1,content_view.content2,picture.path_image FROM topic
        INNER JOIN content_view on content_view.ref_topic_id = topic.topic_id
        INNER JOIN config on config.dtl_name = 'About'
        INNER JOIN picture on picture.ref_topic_id =topic.topic_id
        WHERE topic.topic_type = '#' and config.key = topic.topic_id";

        $db = getDB();
        $stmt = $db->query($str_sql_about); 
        $about = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo json_encode(array("view_about"=>$about));
    }catch (PDOException $e){
        echo json_encode(array("retrun_code"=>"99","return_result_e"=>$e->getMessage()));
    }
}

function view_contact(){
    try{
        $str_sql_contact = "SELECT contract.contract_id,contract.contract_name,contract.web,contract.contract_address
        ,contract.web,contract.email,contract.tel,contract.gps_lat,contract.gps_long,
        content_view.content1 as link
        FROM contract 
        INNER JOIN config ON config.dtl_name = 'Contact'
        INNER JOIN topic on topic.topic_id = config.key and topic.topic_type = '#'
        INNER JOIN content_view on content_view.ref_topic_id = topic.topic_id
        WHERE contract.ref_type = '#'";

        $db = getDB();
        $stmt = $db->query($str_sql_contact); 
        $contact = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo json_encode(array("view_contact"=>$contact));
    }catch (PDOException $e){
         echo json_encode(array("retrun_code"=>"99","return_result_e"=>$e->getMessage()));
    }
}


function view_ebook($parameter){
        try{
        $str_sql_slide = "SELECT topic.topic_id,content_view.content1,content_view.content2 FROM topic 
        INNER JOIN content_view ON content_view.ref_topic_id = topic.topic_id
        WHERE topic.topic_id = '$parameter'";

        $db = getDB();
        $stmt = $db->query($str_sql_slide); 
        $slide = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo json_encode(array("view_ebooks"=>$slide));
    }catch (PDOException $e){
        echo json_encode(array("retrun_code"=>"99","return_result_e"=>$e->getMessage()));
    }
}

function view_menu($parameter){
    try{
     
        echo json_encode(array("retrun_code"=>"00","context_view"=>list_menu($parameter),"image_rand"=>rand_image_view($parameter)));
        if(list_menu($parameter)!=null){
            update_view($parameter); 
        }
    }catch (PDOException $e){
        echo json_encode(array("retrun_code"=>"99","return_result_e"=>$e->getMessage()));
    }
    
}

function Slide_Image1($view){
  
         $str_sql_slide = "select * from sile_image where topic_type != 'N7' order by rand() limit 10";
    
    $db = getDB();
    $stmt = $db->query($str_sql_slide); 
    $slide = $stmt->fetchAll(PDO::FETCH_OBJ);
    $db = null;
    return $slide;
}
    
    function Slide_Image2($view){
       
            $str_sql_slide = "select * from sile_image where topic_type = 'N2' order by rand() limit 10";
        $db = getDB();
        $stmt = $db->query($str_sql_slide);
        $slide = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        return $slide;
    }
    
    function Slide_Image3($view){
       
            $str_sql_slide = "select * from sile_image where topic_type = 'N4' order by rand() limit 10";
        
        
        
        $db = getDB();
        $stmt = $db->query($str_sql_slide);
        $slide = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        return $slide;
    }

function rand_image_view($parameter){
    $str_sql_menu = "select topic.topic_name,picture.pic_detail,picture.path_image,topic.topic_postdate,topic.topic_view_count from topic INNER JOIN picture ON picture.ref_topic_id = topic.topic_id
    where topic.topic_id = '$parameter' and picture.pic_type  = 'G'";

    $db = getDB();
    $stmt = $db->query($str_sql_menu); 
    $menu = $stmt->fetchAll(PDO::FETCH_OBJ);
    $db = null;
    return $menu;
}



function update_view($parameter){ 
    $sql = "UPDATE topic SET topic_view_count=topic_view_count+1 WHERE topic_id = '".$parameter."' ";
    $db = getDB();
    $stmt = $db->query($sql); 
  
}

function getMenu_id($parameter){
    $str_sql= "SELECT config.key FROM config WHERE config.dtl_name = '$parameter' ";

    $db = getDB();
    $stmt = $db->query($str_sql); 
    $query = $stmt->fetchAll(PDO::FETCH_OBJ);
    $db = null;

    foreach($query as $row){
       return $row->key;
    }
  
}

function view_vedio($parameter){
    try{
        echo json_encode(array("retrun_code"=>"00","context_view"=>list_view_vedio($parameter),"vedio_rand"=>list_view_vedio_rand($parameter)));
    }catch (PDOException $e){
        echo json_encode(array("retrun_code"=>"99","return_result_e"=>$e->getMessage()));
    }
}

function list_view_vedio($parameter){
    $str_sql_vedio = "SELECT topic.topic_id,topic.topic_name,content_view.content1,content_view.content2,(select picture.path_image from picture where picture.ref_topic_id = topic.topic_id and picture.pic_type = 'V' ) as path_image ,topic.topic_postdate,config.key as api_key,vedio.youtube_key,vedio.time,contract.contract_name
     FROM topic
    INNER JOIN content_view ON content_view.ref_topic_id =topic.topic_id 
    INNER JOIN config ON config.dtl_name = 'youtube_api_key' 
    INNER JOIN vedio ON vedio.ref_topic_id = topic.topic_id
    LEFT JOIN contract ON contract.ref_topic_id = topic.topic_id and contract.ref_type = 'G'
    WHERE topic.topic_type = 'V' and topic.topic_id=$parameter";
    
        $db = getDB();
        $stmt = $db->query($str_sql_vedio); 
        $vedio = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        return $vedio;
}
function list_view_vedio_rand($parameter){
    $str_sql_vedio = "SELECT topic.topic_id,topic.topic_name,content_view.content1,content_view.content2,picture.path_image,topic.topic_postdate,config.key as api_key,vedio.youtube_key,vedio.time FROM topic 
    INNER JOIN content_view ON content_view.ref_topic_id =topic.topic_id
     INNER JOIN picture ON picture.ref_topic_id = topic.topic_id and picture.pic_type = 'V' 
     INNER JOIN config ON config.dtl_name = 'youtube_api_key' 
     INNER JOIN vedio ON vedio.ref_topic_id = topic.topic_id WHERE topic.topic_id != $parameter and  topic.topic_type = 'V' ORDER BY RAND()
LIMIT 3"; 
    
        $db = getDB();
        $stmt = $db->query($str_sql_vedio); 
        $vedio = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        return $vedio;
}

function list_data(){
    try{
        echo json_encode(array("retrun_code"=>"00","return_result"=>"00","ebooks"=>ebook(0),"vedio"=>vedio(0),"slide_image1"=>Slide_Image1("1"),"slide_image2"=>Slide_Image2("2"),"slide_image3"=>Slide_Image3("3")));
    }catch (PDOException $e){
        echo json_encode(array("retrun_code"=>"99","return_result_e"=>$e->getMessage()));

    }
}
function  ebook($index){
    
    if($index == 0){
        $str_sql_ebook = "SELECT topic.topic_id,content_view.content1,content_view.content2,picture.path_image,topic.topic_name FROM topic 
        INNER JOIN content_view ON content_view.ref_topic_id =topic.topic_id 
        INNER JOIN picture ON picture.ref_topic_id = topic.topic_id and picture.pic_type = 'E'
        WHERE topic.topic_type = 'E'"  ;
    }else{
       
        $str_sql_ebook = "SELECT topic.topic_id,content_view.content1,content_view.content2,picture.path_image,topic.topic_name FROM topic 
        INNER JOIN content_view ON content_view.ref_topic_id =topic.topic_id 
        INNER JOIN picture ON picture.ref_topic_id = topic.topic_id and picture.pic_type = 'E'
        WHERE topic.topic_type = 'E' LIMIT $index"  ;
    }
    
        $db = getDB();
        $stmt = $db->query($str_sql_ebook); 
        $ebook = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        return $ebook;
   
}

function getcount_page($key){
        $out="";
       if($key=="E"){
        $str_sql = "SELECT count(*) as  count from topic where topic.topic_type = 'E'";
        $db = getDB();
        $stmt = $db->query($str_sql); 
        $result = $stmt->fetchAll();
        
        foreach($result as $index){
            $out = $index['count'];
        }
       }else if($key=="V"){
        $str_sql = "SELECT count(*) as  count from topic where topic.topic_type = 'V'";
        $db = getDB();
        $stmt = $db->query($str_sql); 
        $result = $stmt->fetchAll();
        
        foreach($result as $index){
            $out = $index['count'];
        }
      }else if($key=="H"){
        $str_sql = "SELECT count(*) as  count FROM `history_location` where username ='".$_GET['username']."'";
        $db = getDB();
        $stmt = $db->query($str_sql); 
        $result = $stmt->fetchAll();
        
        foreach($result as $index){
            $out = $index['count'];
        }

      }else if($key=="C"){
        $str_sql = "SELECT count(*) as  count FROM `checking` where topic_id ='".$_GET['topic_id']."'";
        $db = getDB();
        $stmt = $db->query($str_sql); 
        $result = $stmt->fetchAll();
        
        foreach($result as $index){
            $out = $index['count'];
        }
      }else{
        $str_sql = "SELECT count(*) as  count from topic where topic.topic_type = '".$key."'";
        $db = getDB();
        $stmt = $db->query($str_sql); 
        $result = $stmt->fetchAll();
        
        foreach($result as $index){
            $out = $index['count'];
        }
       }
        return (int)$out;
}

function vedio($index){

     
    if($index == 0){
        $str_sql = "SELECT topic.topic_id,topic.topic_name,content_view.content1,content_view.content2,picture.path_image,vedio.youtube_key FROM topic 
        INNER JOIN content_view ON content_view.ref_topic_id = topic.topic_id 
        LEFT JOIN picture ON picture.ref_topic_id = topic.topic_id and picture.pic_type = 'V'
        INNER JOIN config ON config.dtl_name = 'youtube_api_key'
        INNER JOIN vedio ON vedio.ref_topic_id = topic.topic_id
        WHERE topic.topic_type = 'V'  order by topic.topic_postdate desc";
    }else{
       
        $str_sql = "SELECT topic.topic_id,topic.topic_name,content_view.content1,content_view.content2,picture.path_image,vedio.youtube_key FROM topic 
        INNER JOIN content_view ON content_view.ref_topic_id = topic.topic_id 
        LEFT JOIN picture ON picture.ref_topic_id = topic.topic_id and picture.pic_type = 'V'
        INNER JOIN config ON config.dtl_name = 'youtube_api_key'
        INNER JOIN vedio ON vedio.ref_topic_id = topic.topic_id
        WHERE topic.topic_type = 'V'  order by topic.topic_postdate desc LIMIT $index";
    }
  


        $db = getDB();
        $stmt = $db->query($str_sql); 
        $retult = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        return $retult;
   
}

function getDB() {
    $dbhost="localhost";
    $dbuser="xnxxfnaj_monpin3d";
    $dbpass="2Dz4QZyT";
    $dbname="xnxxfnaj_monpin3d";
    $dbConnection = new PDO("mysql:host=$dbhost;dbname=$dbname;charset=utf8", $dbuser, $dbpass); 
    $dbConnection->exec("set names utf8");
    $dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $dbConnection;
    }

    function convert_filename($file,$file_name){
        date_default_timezone_set('Asia/Bangkok');
        $date = date("Ymd");	
        $numrand = (mt_rand());
        $upload=$file;
        if($upload <> '') { 
        $path="uploads/";  
        $type = strrchr($file_name,".");
        $newname = $date.$numrand.$type;
        $path_copy=$path.$newname;
        return $path_copy;
        }
    }

?>
