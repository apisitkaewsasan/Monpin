<?php  

/**
 * code for compute distance of GPS 2 points
 *
 * @author ศุภฤกษ์ ศึกษาพรต
 * create: 08/02/2016
 * Tel:    086-977-8349 (08:00-23:00)
 * E-Mail: suphalerk.suksaprot@gmail.com
 * Skype:  spiritstorm11
 * Line:   spiritstorm
 */
define('DISTANCE_KM', ' กม.'); // ถ้าต้องการใส่ค่าว่างให้ใส่ '' เปล่าๆ
define('DISTANCE_METER', ' เมตร'); // ถ้าต้องการใส่ค่าว่างให้ใส่ '' เปล่าๆ
define('TIME_TITLE', 'ใช้เวลาในการเดิน '); // ถ้าต้องการใส่ค่าว่างให้ใส่ '' เปล่าๆ
define('TIME_HOUR', ' ชั่วโมง'); // ถ้าต้องการใส่ค่าว่างให้ใส่ '' เปล่าๆ
define('TIME_MINUTE', ' นาที'); // ถ้าต้องการใส่ค่าว่างให้ใส่ '' เปล่าๆ

/*
********************** CODING **********************
 */

define('BETWEEN_DEGREE', 15);
define('THOUSAND_METER', 1000);

$surface_distance_per_one_degree = array(
	array( 'latitude' => 110.574, 'longitude' => 111.320 ),
	array( 'latitude' => 110.649, 'longitude' => 107.551 ),
	array( 'latitude' => 110.852, 'longitude' => 96.486 ), 
	array( 'latitude' => 111.132, 'longitude' => 78.847 ), 
	array( 'latitude' => 111.412, 'longitude' => 55.800 ), 
	array( 'latitude' => 111.618, 'longitude' => 28.902 ), 
	array( 'latitude' => 111.694, 'longitude' => 0.000 )
);

class GPS {
	var $latitude = 0;
	var $longitude = 0;

	function __construct($latitude, $longitude){
		$this->latitude = $latitude;
		$this->longitude = $longitude;
	}
}

 
function getSurfaceDistance($gps){
	global $surface_distance_per_one_degree;
	
	return $surface_distance_per_one_degree[intval($gps->latitude / BETWEEN_DEGREE)];
}
 
function getLatitudeDistance($gps){
	$surface_distance = getSurfaceDistance($gps);

	return $surface_distance['latitude'] * THOUSAND_METER;
}
 
function getLongitudeDistance($gps){
	$surface_distance = getSurfaceDistance($gps);

	return $surface_distance['longitude'] * THOUSAND_METER;
}
  
function findDistance($gps1, $gps2){
 
	$latitudeDistance1 = getLatitudeDistance($gps1);
	$latitudeDistance2 = getLatitudeDistance($gps2);
  
	$longitudeDistance1 = getLongitudeDistance($gps1);
	$longitudeDistance2 = getLongitudeDistance($gps2);
  
	// (X2 * a2 - X1 * a1) ^ 2
	$power1 = pow(($gps2->latitude * $latitudeDistance2) - ($gps1->latitude * $latitudeDistance1), 2);
	// (Y2 * b2 - Y1 * b1) ^ 2
	$power2 = pow(($gps2->longitude * $longitudeDistance2) - ($gps1->longitude * $longitudeDistance1), 2);
 
	return sqrt($power1 + $power2);
}

function calculate_meter($distance, $type = 'M'){
	
	if($type == 'K'){
		if($distance > 1000)
			$output = number_format( round( $distance/1000 ) ,0,'.',',') .'.'. ( $distance%1000 ) . DISTANCE_KM ;
		else
			$output = ( $distance%1000 ) . DISTANCE_METER ;
	}else{
		$output = number_format($distance,0,'.',',')  . DISTANCE_METER ;
	}

	return $output ;
}

function calculate_time($distance){
	// 1 kilo = >  40 minute
	
	$total_minute = $distance * 0.04;

	if($total_minute >= 60){
		$output = TIME_TITLE . number_format( round( $total_minute/60 ) ,0,'.',',') . TIME_HOUR . ( $total_minute%60 ) . TIME_MINUTE ;

	}else{
		$output = TIME_TITLE . number_format( $total_minute  ,0,'.',',')  . TIME_MINUTE ;
	}

	return $output ;
}
/**
 * define gps1 and gps2 location
 */
if( !isset($_GET['gps1']) || !isset($_GET['gps2']) ){
	echo 'กรุณาระบุ gps1 และ gps2';
	exit();
}
$get_gps1 = explode(',', addslashes($_GET['gps1']));
if( !isset($get_gps1['0']) || !isset($get_gps1['1']) ){
	echo 'กรุณาระบุ gps1 ให้ถูกต้องเช่น 13.35069,100.345425';
	exit();
}
$get_gps2 = explode(',', addslashes($_GET['gps2']));
if( !isset($get_gps2['0']) || !isset($get_gps2['1']) ){
	echo 'กรุณาระบุ gps2 ให้ถูกต้องเช่น 13.35069,100.345425';
	exit();
}
$type = strtoupper($_GET['type']);
if($type != 'M' && $type != 'K'){
	echo 'กรุณาระบุ type ให้ถูกต้องเช่น K , M';
	exit();
}

$gps1 = new GPS($get_gps1['0'], $get_gps1['1']);
$gps2 = new GPS($get_gps2['0'], $get_gps2['1']);

header('Content-Type: text/html; charset=utf-8');

$distance = findDistance($gps1, $gps2) ;

echo calculate_meter( $distance , $type).' ';

if(isset($_GET['time']) && strtolower($_GET['time']) == 'true'){
	echo calculate_time( $distance);
}
?>