package chiangmai.fang.monping.dev.utils;


import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public  class AppUtils {


    public String Now_time(String dateStart){
        String[] date_fix = {"","ม.ค","ก.พ","มี.ค","เม.ย","พ.ค","มิ.ย","ก.ค","ส.ค","ก.ย","ต.ค","พ.ย","ธ.ค"};

        String text= null;
        String[] date = dateStart.split(" ")[0].split("-");
//        String[] time_show = dateStart.split(" ")[1].split(":");
        text = date[2]+" "+date_fix[Integer.parseInt(date[1])]+" "+(Integer.valueOf(date[0])+543);


        return text.toString();
    }

    public static String MakePrettyDate(String getCreatedTime) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        ParsePosition pos = new ParsePosition(0);
        long then = formatter.parse(getCreatedTime, pos).getTime();
        long now = new Date().getTime();

        long seconds = (now - then) / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        long days = hours / 24;

        String friendly = null;
        long num = 0;
        if (days > 0) {
            num = days;

            friendly = days + " day";
        } else if (hours > 0) {
            num = hours;
            friendly = hours + " hour";
        } else if (minutes > 0) {
            num = minutes;
            friendly = minutes + " minute";
        } else {
            num = seconds;
            friendly = seconds + " second";
        }
        if (num > 1) {
            friendly += "s";
        }
        String postTimeStamp = friendly + " ago";
        return postTimeStamp;
    }

    public static String fb_date(String dateStart){
        String[] date_fix = {"","ม.ค","ก.พ","มี.ค","เม.ย","พ.ค","มิ.ย","ก.ค","ส.ค","ก.ย","ต.ค","พ.ย","ธ.ค"};
        String text= null;
        Calendar calendar = Calendar.getInstance();
        //HH converts hour in 24 hours format (0-23), day calculation
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateStop = format.format(calendar.getTime());

        Date d1 = null;
        Date d2 = null;

        try {
            d1 = format.parse(dateStart);
            d2 = format.parse(dateStop);

            //in milliseconds
            long diff = d2.getTime() - d1.getTime();

            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);
            String[] date = dateStart.split(" ")[0].split("-");
            String[] time_show = dateStart.split(" ")[1].split(":");

            if(diffDays>1){

                text = date[2]+" "+date_fix[Integer.parseInt(date[1])]+" "+date[0];
            }else if(diffDays>=1) {
                text = "เมื่อวานนี้ "+time_show[0]+":"+time_show[1];
            }else if(diffHours>0){
                text = diffHours+" ชั่วโมงที่แล้ว";
            }else if(diffMinutes>0){
                text = diffMinutes+" นาที่ที่แล้ว";
            }else if(diffSeconds>0){
                text = diffSeconds+" วินาทีที่แล้ว";
            }else{
                text = chang_date(dateStart,dateStop);
            }


        } catch (Exception e) {
            e.printStackTrace();
            text = e.getMessage();
        }
        return text;
    }

    public static String fb_date1(String dateStart){
        String[] date_fix = {"","ม.ค","ก.พ","มี.ค","เม.ย","พ.ค","มิ.ย","ก.ค","ส.ค","ก.ย","ต.ค","พ.ย","ธ.ค"};
        String text= null;
        Calendar calendar = Calendar.getInstance();
        //HH converts hour in 24 hours format (0-23), day calculation
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String dateStop = format.format(calendar.getTime());

        Date d1 = null;
        Date d2 = null;

        try {
            d1 = format.parse(dateStart);
            d2 = format.parse(dateStop);

            //in milliseconds
            long diff = d2.getTime() - d1.getTime();

            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);
            String[] date = dateStart.split(" ")[0].split("-");

            if(diffDays>1){

                text = date[2]+" "+date_fix[Integer.parseInt(date[1])]+" "+date[0];
            }else if(diffDays>=1) {
                text = "เมื่อวานนี้ ";
            }else if(diffHours>0){
                text = diffHours+" ชั่วโมงที่แล้ว";
            }else if(diffMinutes>0){
                text = diffMinutes+" นาที่ที่แล้ว";
            }else if(diffSeconds>0){
                text = diffSeconds+" วินาทีที่แล้ว";
            }else{
                text = chang_date(dateStart,dateStop);
            }


        } catch (Exception e) {
            e.printStackTrace();
            text = e.getMessage();
        }
        return text;
    }




    public static String chang_date(String date_from, String date_to){

        String[] date_fix = {"","ม.ค","ก.พ","มี.ค","เม.ย","พ.ค","มิ.ย","ก.ค","ส.ค","ก.ย","ต.ค","พ.ย","ธ.ค"};
        String[] date = date_from.split(" ");
        String[] date_convert = date[0].split("-");
        //return date_convert[0]+" "+date_fix[Integer.valueOf(date_convert[1])]+" "+(Integer.valueOf("20"+date_convert[2])+543);
        return date_convert[2]+" "+date_fix[Integer.valueOf(date_convert[1])]+" "+(Integer.valueOf(date_convert[0])+543);
    }


    public static String removeHTML(String htmlString)
    {
        // Remove HTML tag from java String
        String noHTMLString = htmlString.replaceAll("\\<.*?\\>", "");

// Remove Carriage return from java String
        noHTMLString = noHTMLString.replaceAll("\r", "<br/>");
        noHTMLString = noHTMLString.replaceAll("<([bip])>.*?</\1>", "");
// Remove New line from java string and replace html break
        noHTMLString = noHTMLString.replaceAll("\n", " ");
        noHTMLString = noHTMLString.replaceAll("\"", "&quot;");
        noHTMLString = noHTMLString.replaceAll("<(.*?)\\>"," ");//Removes all items in brackets
        noHTMLString = noHTMLString.replaceAll("<(.*?)\\\n"," ");//Must be undeneath
        noHTMLString = noHTMLString.replaceFirst("(.*?)\\>", " ");
        noHTMLString = noHTMLString.replaceAll("&nbsp;"," ");
        noHTMLString = noHTMLString.replaceAll("&amp;"," ");
        return noHTMLString;

    }



}
