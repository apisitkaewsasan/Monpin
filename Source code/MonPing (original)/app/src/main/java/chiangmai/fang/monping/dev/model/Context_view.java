package chiangmai.fang.monping.dev.model;

public class Context_view {
    int topic_id;
    String topic_name;
    String content1;
    String content2;
    String path_image;
    String topic_postdate;
    String api_key;
    String youtube_key;
    String time;
    String image_rand;
    String contract_name;
    String topic_view_count;
    String gps_lat;
    String gps_long;
    String contract_address;


    public Context_view(int topic_id, String topic_name, String content1, String content2, String path_image, String topic_postdate, String api_key, String youtube_key, String time, String image_rand, String contract_name, String topic_view_count, String gps_lat, String gps_long, String contract_address) {
        this.topic_id = topic_id;
        this.topic_name = topic_name;
        this.content1 = content1;
        this.content2 = content2;
        this.path_image = path_image;
        this.topic_postdate = topic_postdate;
        this.api_key = api_key;
        this.youtube_key = youtube_key;
        this.time = time;
        this.image_rand = image_rand;
        this.contract_name = contract_name;
        this.topic_view_count = topic_view_count;
        this.gps_lat = gps_lat;
        this.gps_long = gps_long;
        this.contract_address = contract_address;
    }

    public int getTopic_id() {
        return topic_id;
    }

    public void setTopic_id(int topic_id) {
        this.topic_id = topic_id;
    }

    public String getTopic_name() {
        return topic_name;
    }

    public void setTopic_name(String topic_name) {
        this.topic_name = topic_name;
    }

    public String getContent1() {
        return content1;
    }

    public void setContent1(String content1) {
        this.content1 = content1;
    }

    public String getContent2() {
        return content2;
    }

    public void setContent2(String content2) {
        this.content2 = content2;
    }

    public String getPath_image() {
        return path_image;
    }

    public void setPath_image(String path_image) {
        this.path_image = path_image;
    }

    public String getTopic_postdate() {
        return topic_postdate;
    }

    public void setTopic_postdate(String topic_postdate) {
        this.topic_postdate = topic_postdate;
    }

    public String getApi_key() {
        return api_key;
    }

    public void setApi_key(String api_key) {
        this.api_key = api_key;
    }

    public String getYoutube_key() {
        return youtube_key;
    }

    public void setYoutube_key(String youtube_key) {
        this.youtube_key = youtube_key;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getImage_rand() {
        return image_rand;
    }

    public void setImage_rand(String image_rand) {
        this.image_rand = image_rand;
    }

    public String getContract_name() {
        return contract_name;
    }

    public void setContract_name(String contract_name) {
        this.contract_name = contract_name;
    }

    public String getTopic_view_count() {
        return topic_view_count;
    }

    public void setTopic_view_count(String topic_view_count) {
        this.topic_view_count = topic_view_count;
    }

    public String getGps_lat() {
        return gps_lat;
    }

    public void setGps_lat(String gps_lat) {
        this.gps_lat = gps_lat;
    }

    public String getGps_long() {
        return gps_long;
    }

    public void setGps_long(String gps_long) {
        this.gps_long = gps_long;
    }

    public String getContract_address() {
        return contract_address;
    }

    public void setContract_address(String contract_address) {
        this.contract_address = contract_address;
    }
}
