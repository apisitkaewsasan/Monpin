package chiangmai.fang.monping.dev.model;

import android.net.Uri;

public class User {
    private final Uri picture;
    private final String name;
    private final String id;
    private final String email;
    private final String permissions;
    private  final String last_name;
    private  final  String first_name;


    public User(Uri picture, String name, String id, String email, String permissions, String last_name, String first_name) {
        this.picture = picture;
        this.name = name;
        this.id = id;
        this.email = email;
        this.permissions = permissions;
        this.last_name = last_name;
        this.first_name = first_name;
    }

    public Uri getPicture() {
        return picture;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getPermissions() {
        return permissions;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getFirst_name() {
        return first_name;
    }
}

