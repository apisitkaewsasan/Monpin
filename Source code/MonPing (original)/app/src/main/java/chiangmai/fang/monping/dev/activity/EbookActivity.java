package chiangmai.fang.monping.dev.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import chiangmai.fang.monping.dev.R;
import chiangmai.fang.monping.dev.model.Ebook;
import chiangmai.fang.monping.dev.model.ListData;
import chiangmai.fang.monping.dev.model.view_ebook;
import chiangmai.fang.monping.dev.service.Api;
import chiangmai.fang.monping.dev.utils.Contact;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class EbookActivity extends AppCompatActivity {

    WebView webView;
    private TextView Error;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

//Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_ebook);
       Error = (TextView)findViewById(R.id.Error);
        getData();

    }

    private void getData(){
        // display a progress dialog
        final ProgressDialog progressDialog = new ProgressDialog(EbookActivity.this);
        progressDialog.setCancelable(false); // set cancelable to false
        progressDialog.setMessage("Please Wait"); // set message
        progressDialog.show(); // show progress dialog

        // Api is a class in which we define a method getClient() that returns the API Interface class object
        // getUsersList() is a method in API Interface class, in this method we define our API sub url


        Api.getClient().getEbookView(getIntent().getStringExtra("Topic_id")).enqueue(new Callback<ListData>() {
            @Override
            public void onResponse(Call<ListData> call, Response<ListData> response) {
                try{

                    WebView webView = (WebView) findViewById(R.id.webview);
                    WebSettings webSettings = webView.getSettings();
                    webSettings.setJavaScriptEnabled(true);
                    webView.loadUrl(new Contact().Base_url.split("/")[0]+"//"+new Contact().Base_url.split("/")[2]+"/"+response.body().getView_ebooks().get(0).getContent1());

                }catch (Exception e){
                    Error.setText(e.getStackTrace()+"\n "+e.getMessage()+"\n"+" กรุณาติดต่อผู้พัฒนาแอพเคชั่น");
                }finally {
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ListData> call, Throwable t) {
                Toast.makeText(EbookActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
            }
        });

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
