package chiangmai.fang.monping.dev.model;

import java.io.Serializable;

import chiangmai.fang.monping.dev.utils.Contact;

public class gallery implements Serializable {
    String topic_name;
    String pic_detail;
    String path_image;
    String topic_postdate;
    String topic_view_count;

    public gallery(String topic_name, String pic_detail, String path_image, String topic_postdate, String topic_view_count) {
        this.topic_name = topic_name;
        this.pic_detail = pic_detail;
        this.path_image = path_image;
        this.topic_postdate = topic_postdate;
        this.topic_view_count = topic_view_count;
    }

    public String getTopic_name() {
        return topic_name;
    }

    public void setTopic_name(String topic_name) {
        this.topic_name = topic_name;
    }

    public String getPic_detail() {
        return pic_detail;
    }

    public void setPic_detail(String pic_detail) {
        this.pic_detail = pic_detail;
    }

    public String getPath_image() {
        return path_image;
    }

    public void setPath_image(String path_image) {
        this.path_image = path_image;
    }

    public String getTopic_postdate() {
        return topic_postdate;
    }

    public void setTopic_postdate(String topic_postdate) {
        this.topic_postdate = topic_postdate;
    }

    public String getTopic_view_count() {
        return topic_view_count;
    }

    public void setTopic_view_count(String topic_view_count) {
        this.topic_view_count = topic_view_count;
    }
}
