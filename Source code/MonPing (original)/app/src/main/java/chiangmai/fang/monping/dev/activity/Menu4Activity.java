package chiangmai.fang.monping.dev.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import chiangmai.fang.monping.dev.R;
import chiangmai.fang.monping.dev.adepter.FragmentDAdapter;
import chiangmai.fang.monping.dev.adepter.FragmentFAdapter;
import chiangmai.fang.monping.dev.adepter.Menu4Adepter;
import chiangmai.fang.monping.dev.adepter.Pager1Adepter;
import chiangmai.fang.monping.dev.model.Context_view;
import chiangmai.fang.monping.dev.model.ListData;
import chiangmai.fang.monping.dev.service.Api;
import chiangmai.fang.monping.dev.utils.EndlessRecyclerViewScrollListener;
import chiangmai.fang.monping.dev.utils.RecycleViewClickListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Menu4Activity extends AppCompatActivity {


    private ShimmerFrameLayout mShimmerViewContainer;
    Toolbar toolbar;RecyclerView recyclerView;
    List<Context_view> e;
    Menu4Adepter menu4sup;
    int Count_row=2;
    private EndlessRecyclerViewScrollListener scrollListener;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu4);

        e = new ArrayList<>();
        loadData(Count_row);

        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_keyboard_arrow_left_black_24dp);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });


        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        setRecyclerView(); // set LayoutManager to RecyclerView


    }

    public void setRecyclerView(){
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(),2);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(gridLayoutManager); // set LayoutManager to RecyclerView

        menu4sup = new Menu4Adepter(getApplicationContext(),e, new RecycleViewClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                //startActivity(new Intent(getContext().getApplicationContext(), MenuActivity.class).putExtra("activity","menu2"));
                statactivity(e.get(position).getTopic_id()+"");
            }
        });
        recyclerView.setAdapter(menu4sup);


        scrollListener = new EndlessRecyclerViewScrollListener(gridLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {

                if(e.size()<Count_row){
                    loadData(e.size()+2);
                }
            }


        };
        recyclerView.addOnScrollListener(scrollListener);
    }

    public  void statactivity(String x){
        // Toast.makeText(getContext() , ""+x, Toast.LENGTH_SHORT).show();
        startActivity(new Intent(getApplicationContext(), SubMenu4Activity.class).putExtra("activity","menu4").putExtra("topic_id",x));

    }



    private void loadData(int index) {

        Api.getClient().getMenuMain("N7",index).enqueue(new Callback<ListData>() {
            @Override
            public void onResponse(Call<ListData> call, Response<ListData> response) {
                try{
                    if(response.body().getRetrun_code().equals("00")){
                        if(response.body().getContext_view().size()==0) {

                            alert("ไม่พบรายการค้นหา");
                        }else{
                            e.clear();
                            e.addAll(response.body().getContext_view());
                            Count_row = response.body().getCount_row();
                            mShimmerViewContainer.stopShimmerAnimation();
                            mShimmerViewContainer.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.VISIBLE);
                            menu4sup.notifyDataSetChanged();

                        }

                    }else{
                        alert(response.body().getReturn_result_e());
                    }
                }catch (Exception e){
                    alert(response.body().getReturn_result_e()+" "+e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ListData> call, Throwable t) {
                mShimmerViewContainer.stopShimmerAnimation();
                mShimmerViewContainer.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
            }
        });

    }

    public void alert(String message){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage(message);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        onBackPressed();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();
    }



    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        super.onPause();
    }
}
