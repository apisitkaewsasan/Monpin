package chiangmai.fang.monping.dev.adepter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;

import java.util.List;

import chiangmai.fang.monping.dev.R;
import chiangmai.fang.monping.dev.model.Player;
import chiangmai.fang.monping.dev.model.vedio;
import chiangmai.fang.monping.dev.utils.Contact;
import chiangmai.fang.monping.dev.utils.RecycleViewClickListener;

public class RecyclerViewCustomVideo extends RecyclerView.Adapter<RecyclerViewCustomVideo.ViewHolder> {
    private List<vedio> mvedio;
    private Context mContext;
    public int mWidth;
    RecycleViewClickListener listener;

    public RecyclerViewCustomVideo(Context context, List<vedio> dataset,int width, RecycleViewClickListener listener) {
        mvedio = dataset;
        mContext = context;
        mWidth = width;
        this.listener = listener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView showItem;
        public RelativeLayout layout;

        public ViewHolder(View view) {
            super(view);
            showItem = (ImageView) view.findViewById(R.id.showItem);
            layout = (RelativeLayout)view.findViewById(R.id.loyout);


        }
    }


    public RecyclerViewCustomVideo.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.recycler_view_row_vedio, viewGroup, false);

        RecyclerViewCustomVideo.ViewHolder viewHolder = new RecyclerViewCustomVideo.ViewHolder(view);
        //viewHolder.showItem.getLayoutParams().height = 200;
       // viewHolder.showItem.getLayoutParams().width = mWidth;

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewCustomVideo.ViewHolder viewHolder, int i) {
        vedio vedio = mvedio.get(i);


        Picasso.with(mContext).load(new Contact().get_image_youtube(vedio.getYoutube_key())).resize(400, 280).into(viewHolder.showItem);

        viewHolder.showItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // Toast.makeText(mContext.getApplicationContext(), "item : "+viewHolder.getPosition(), Toast.LENGTH_SHORT).show();
                listener.onItemClick(view, viewHolder.getPosition());
            }
        });

       // Toast.makeText(mContext.getApplicationContext(), "sfgvefd", Toast.LENGTH_SHORT).show();
    }





    @Override
    public int getItemCount() {
        return mvedio.size();
    }
}
