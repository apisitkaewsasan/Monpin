package chiangmai.fang.monping.dev.adepter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import chiangmai.fang.monping.dev.R;
import chiangmai.fang.monping.dev.model.Ebook;
import chiangmai.fang.monping.dev.model.vedio;
import chiangmai.fang.monping.dev.utils.Contact;
import chiangmai.fang.monping.dev.utils.RecycleViewClickListener;

public class Pager2Adepter extends RecyclerView.Adapter<Pager2Adepter.ViewHolder>  {

    private List<vedio> vedio;
    private Context context;
    RecycleViewClickListener listener;

    public final int TYPE_MOVIE = 0;
    public final int TYPE_LOAD = 1;
    Pager1Adepter.OnLoadMoreListener loadMoreListener;
    boolean isLoading = false, isMoreDataAvailable = true;

    public Pager2Adepter(Context context, List<vedio> vedio,RecycleViewClickListener listener) {
        this.context = context;
        this.vedio = vedio;
        this.listener = listener;

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        ImageView image;

        public ViewHolder(View view) {
            super(view);
            name = (TextView) itemView.findViewById(R.id.name);
            image = (ImageView) itemView.findViewById(R.id.image);
        }
    }


    public ViewHolder onCreateViewHolder( ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context)
                .inflate(R.layout.rowlayout2, viewGroup, false);


        ViewHolder viewHolder = new ViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(view, viewHolder.getPosition());
            }
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        if(i>=getItemCount()-1 && isMoreDataAvailable && !isLoading && loadMoreListener!=null){
            isLoading = true;
            loadMoreListener.onLoadMore();
        }

        if(getItemViewType(i)==TYPE_MOVIE){
            viewHolder.name.setText((i+1)+" "+vedio.get(i).getTopic_name());
            Picasso.with(context).load(new Contact().Base_url+vedio.get(i).getPath_image()).placeholder(R.drawable.noimage).resize(300, 200).into(viewHolder.image);

        }


    }


    @Override
    public int getItemCount() {
        return vedio.size();
    }


    static class LoadHolder extends RecyclerView.ViewHolder{
        public LoadHolder(View itemView) {
            super(itemView);
        }
    }

    public void setMoreDataAvailable(boolean moreDataAvailable) {
        isMoreDataAvailable = moreDataAvailable;
    }

    /* notifyDataSetChanged is final method so we can't override it
         call adapter.notifyDataChanged(); after update the list
         */
    public void notifyDataChanged(){
        notifyDataSetChanged();
        isLoading = false;
    }


    public interface OnLoadMoreListener{
        void onLoadMore();
    }

    public void setLoadMoreListener(Pager1Adepter.OnLoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }
}
