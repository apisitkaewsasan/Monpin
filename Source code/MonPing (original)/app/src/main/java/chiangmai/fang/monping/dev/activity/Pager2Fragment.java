package chiangmai.fang.monping.dev.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import chiangmai.fang.monping.dev.R;
import chiangmai.fang.monping.dev.adepter.Pager1Adepter;
import chiangmai.fang.monping.dev.adepter.Pager2Adepter;
import chiangmai.fang.monping.dev.model.Ebook;
import chiangmai.fang.monping.dev.model.ListData;
import chiangmai.fang.monping.dev.model.vedio;
import chiangmai.fang.monping.dev.service.Api;
import chiangmai.fang.monping.dev.utils.RecycleViewClickListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Pager2Fragment extends Fragment {

    RecyclerView recyclerView;

    private ShimmerFrameLayout mShimmerViewContainer;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadData(2);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.activity_menu2, container, false);
        mShimmerViewContainer = rootView.findViewById(R.id.shimmer_view_container);
        ImageView imageView=(ImageView) rootView.findViewById(R.id.back);
        imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_keyboard_arrow_left_black_24dp));
        Button back_home = (Button)rootView.findViewById(R.id.button2);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);

        back_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
                getActivity().finish();
            }

        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
                getActivity().finish();
            }
        });

        // set a GridLayoutManager with default vertical orientation and 2 number of columns

        return  rootView;
    }


    public  void statactivity(String x){
       // Toast.makeText(getContext() , ""+x, Toast.LENGTH_SHORT).show();
        startActivity(new Intent(getContext().getApplicationContext(), VideoActivity.class).putExtra("Topic_id",x));

    }

    public void setRecyclerView(List<vedio> ve,int Count_row){

        // set a GridLayoutManager with default vertical orientation and 2 number of columns
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext().getApplicationContext(),2);
        recyclerView.setLayoutManager(gridLayoutManager); // set LayoutManager to RecyclerView
        //  call the constructor of CustomAdapter to send the reference and data to Adapter
        // mShimmerViewContainer.stopShimmerAnimation();
        //mShimmerViewContainer.setVisibility(View.GONE);
        Pager2Adepter customAdapter = new Pager2Adepter(getContext(), ve, new RecycleViewClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                statactivity(ve.get(position).getTopic_id()+"");
            }
        });
        customAdapter.setLoadMoreListener(new Pager1Adepter.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                recyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        //int index = movies.size() - 1;
                        // loadMore(index);
                        if(ve.size()<Count_row){
                            loadData(ve.size()+2);
                        }
                    }
                });
            }
        });
        recyclerView.setAdapter(customAdapter);
    }


    private void loadData(int index) {

        Api.getClient().getAllVedio(index).enqueue(new Callback<ListData>() {
            @Override
            public void onResponse(Call<ListData> call, Response<ListData> response) {

                try{
                    if(response.body().getRetrun_code().equals("00")){
                        if(response.body().getVedio().size()==0) {

                            alert("ไม่พบรายการค้นหา");
                        }else{
                            setRecyclerView(response.body().getVedio(),response.body().getCount_row());

                            mShimmerViewContainer.stopShimmerAnimation();
                            mShimmerViewContainer.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.VISIBLE);

                        }

                    }else{
                        alert(response.body().getReturn_result_e());
                    }
                }catch (Exception e){
                    alert(response.body().getReturn_result_e()+" "+e.getMessage());
                }
            }


            @Override
            public void onFailure(Call<ListData> call, Throwable t) {
                mShimmerViewContainer.stopShimmerAnimation();
                mShimmerViewContainer.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
            }
        });
    }

    public void alert(String message){
        mShimmerViewContainer.stopShimmerAnimation();
      try{
          AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
          builder1.setMessage(message);
          builder1.setCancelable(true);

          builder1.setPositiveButton(
                  "OK",
                  new DialogInterface.OnClickListener() {
                      public void onClick(DialogInterface dialog, int id) {
                          dialog.dismiss();
                          getActivity().onBackPressed();
                      }
                  });


          AlertDialog alert11 = builder1.create();
          alert11.show();
      }catch (Exception e){

      }
    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        super.onPause();
    }



}
