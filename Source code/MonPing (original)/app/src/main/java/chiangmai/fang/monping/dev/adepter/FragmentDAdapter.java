package chiangmai.fang.monping.dev.adepter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import chiangmai.fang.monping.dev.R;
import chiangmai.fang.monping.dev.model.Context_view;
import chiangmai.fang.monping.dev.model.Ebook;
import chiangmai.fang.monping.dev.utils.Contact;
import chiangmai.fang.monping.dev.utils.RecycleViewClickListener;

public class FragmentDAdapter extends RecyclerView.Adapter<FragmentDAdapter.ViewHolder>  {

    private List<Context_view> item;
    private Context context;
    RecycleViewClickListener listener;

    public FragmentDAdapter(Context context,List<Context_view> item , RecycleViewClickListener listener) {
        this.context = context;
        this.item = item;
        this.listener = listener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        ImageView image;

        public ViewHolder(View view) {
            super(view);
            name = (TextView) itemView.findViewById(R.id.name);
            image = (ImageView) itemView.findViewById(R.id.image);
        }
    }


    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context)
                .inflate(R.layout.fragmentdadapter, viewGroup, false);


        ViewHolder viewHolder = new ViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(view, viewHolder.getPosition());
            }
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {


        viewHolder.name.setText(item.get(i).getTopic_name());
        Picasso.with(context).load(new Contact().Base_url+item.get(i).getPath_image()+"").placeholder(R.drawable.noimage).resize(300, 200).into(viewHolder.image);

        // viewHolder.image.setImageResource(personImages.get(position));

    }


    @Override
    public int getItemCount() {
        return item.size();
    }
}

