package chiangmai.fang.monping.dev.utils;

import android.view.View;

public interface RecycleViewClickListener {
    public void onItemClick(View v, int position);
}
