package chiangmai.fang.monping.dev.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PointF;
import android.media.MediaPlayer;
import android.os.Build;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.vision.barcode.Barcode;

import java.util.ArrayList;
import java.util.List;

import chiangmai.fang.monping.dev.R;
import chiangmai.fang.monping.dev.model.ListData;
import chiangmai.fang.monping.dev.model.SyncScanner;
import chiangmai.fang.monping.dev.service.Api;
import info.androidhive.barcode.BarcodeReader;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CodeScannerActivity extends AppCompatActivity  implements BarcodeReader.BarcodeReaderListener{
    private static final String TAG = CodeScannerActivity.class.getSimpleName();

    private static final int RC_PERMISSION = 10;
    private boolean mPermissionGranted;
    SharedPreferences mPrefs = null;
    private BarcodeReader barcodeReader;
    TextView result;
    String confirm_result;
    String confirm_type;
    Button confirm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_code_scanner);


        barcodeReader = (BarcodeReader) getSupportFragmentManager().findFragmentById(R.id.barcode_fragment);
        barcodeReader.setBeepSoundFile("message.mp3");
        result = (TextView)findViewById(R.id.result);
        confirm = (Button)findViewById(R.id.confirm);
        confirm.setVisibility(View.GONE);
        confirm.setEnabled(false);
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moveStaivity(confirm_result,confirm_type);
            }
        });

        mPrefs = this.getSharedPreferences("lend_system", Context.MODE_PRIVATE);
    }



    public void SyncServer(String index){
        // display a progress dialog
        final ProgressDialog progressDialog = new ProgressDialog(CodeScannerActivity.this);
        progressDialog.setCancelable(false); // set cancelable to false
        progressDialog.setMessage("Please Wait"); // set message
        progressDialog.show(); // show progress dialog


        // Api is a class in which we define a method getClient() that returns the API Interface class object
        // getUsersList() is a method in API Interface class, in this method we define our API sub url

        Api.getClient().SyncScanner(index,mPrefs.getString("username","")).enqueue(new Callback<ListData>() {
            @Override
            public void onResponse(Call<ListData> call, Response<ListData> response) {
                try {
                    if(response.body().getRetrun_code().equals("00")){

                        //
                        confirm_result = response.body().getView_scanner().get(0).getTopic_id();
                        confirm_type = response.body().getView_scanner().get(0).getTopic_type();
                        moveStaivity(confirm_result,confirm_type);
                        result.setText("ค้นหาเจอ : "+response.body().getView_scanner().get(0).getTopic_name());
                        result.setTextColor(Color.parseColor("#4CAF50"));
                        confirm.setEnabled(true);
                    }else{
                       // alert(response.body().getReturn_result_e());
                        result.setText(response.body().getReturn_result_e());
                        result.setTextColor(Color.parseColor("#A62C23"));
                        confirm.setEnabled(false);
                    }

                }catch (Exception e){
                    confirm.setEnabled(false);
                    result.setText("สแกน QR Code ไม่ถูกต้องกรุณาลองใหม่");
                    result.setTextColor(Color.parseColor("#A62C23"));
                }


                //// Toast.makeText(MenuActivity.this, "OK "+list.getImage_rand().size(), Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ListData> call, Throwable t) {
                progressDialog.dismiss();
                alert(t.getMessage());
                //dismiss progress dialog
            }
        });

    }

    public void alert(String message){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage(message);
        builder1.setCancelable(true);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "ทำรายการอีกครั่ง",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                }).setNegativeButton(
                "กลับสู่เมนูหลัก",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        onBackPressed();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }


    public String convert_menu(String index){
        String out= "";
        if(index.equals("N1") || index.equals("N2") ||index.equals("N3")){
            out= "menu2";
        }else if(index.equals("N4") || index.equals("N5") ){
            out = "menu3";
        }else if(index.equals("N6")){
            out = "menu4";
        }
        return out;
    }


    public void moveStaivity(String value,String index){

        if(index.equals("V") || index == "V"){
            startActivity(new Intent(getBaseContext(), VideoActivity.class).putExtra("Topic_id",value));
            finish();
        }else if(index.equals("E") || index == "E"){
            startActivity(new Intent(getBaseContext(), EbookActivity.class).putExtra("Topic_id",value).putExtra("activity",convert_menu(index)));
            finish();
        }else if(index.equals("N7") || index == "N7"){
            startActivity(new Intent(getBaseContext(), SubMenu4Activity.class).putExtra("topic_id",value).putExtra("activity",convert_menu(index)));
            finish();
        } else{
            startActivity(new Intent(getBaseContext(), MenuActivity.class).putExtra("topic_id",value).putExtra("activity",convert_menu(index)));
            finish();
        }


    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    public void onScanned(Barcode barcode) {
        barcodeReader.playBeep();

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
               //Toast.makeText(getApplicationContext(), "Barcode: " + barcode.displayValue, Toast.LENGTH_SHORT).show();
                if(barcode.displayValue.equals("") || barcode.displayValue==""){
                    result.setText("ไม่พบรหัสคีย์ในบาร์โค้ดนี้กรุณาลองใหม่อีกครั้ง");
                    confirm.setEnabled(false);
                    result.setTextColor(Color.parseColor("#A62C23"));
                }else{
                    SyncServer(barcode.displayValue);
                }

            }
        });
    }

    @Override
    public void onScannedMultiple(List<Barcode> barcodes) {

    }

    @Override
    public void onBitmapScanned(SparseArray<Barcode> sparseArray) {

    }

    @Override
    public void onScanError(String errorMessage) {

    }

    @Override
    public void onCameraPermissionDenied() {
        Toast.makeText(getApplicationContext(), "Camera permission denied!", Toast.LENGTH_LONG).show();
        finish();

    }
}
