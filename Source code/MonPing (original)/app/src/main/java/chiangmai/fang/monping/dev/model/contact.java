package chiangmai.fang.monping.dev.model;

public class contact {
    String contract_id;
    String contract_name;
    String web;
    String contract_address;
    String email;
    String tel;
    String gps_lat;
    String gps_long;
    String link;

    public contact(String contract_id, String contract_name, String web, String contract_address, String email, String tel, String gps_lat, String gps_long, String link) {
        this.contract_id = contract_id;
        this.contract_name = contract_name;
        this.web = web;
        this.contract_address = contract_address;
        this.email = email;
        this.tel = tel;
        this.gps_lat = gps_lat;
        this.gps_long = gps_long;
        this.link = link;
    }

    public String getContract_id() {
        return contract_id;
    }

    public void setContract_id(String contract_id) {
        this.contract_id = contract_id;
    }

    public String getContract_name() {
        return contract_name;
    }

    public void setContract_name(String contract_name) {
        this.contract_name = contract_name;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getContract_address() {
        return contract_address;
    }

    public void setContract_address(String contract_address) {
        this.contract_address = contract_address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getGps_lat() {
        return gps_lat;
    }

    public void setGps_lat(String gps_lat) {
        this.gps_lat = gps_lat;
    }

    public String getGps_long() {
        return gps_long;
    }

    public void setGps_long(String gps_long) {
        this.gps_long = gps_long;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
