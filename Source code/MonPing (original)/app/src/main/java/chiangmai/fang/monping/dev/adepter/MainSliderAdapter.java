package chiangmai.fang.monping.dev.adepter;

import android.content.Context;
import android.view.View;
import android.widget.Toast;

import java.util.List;

import chiangmai.fang.monping.dev.model.slide_image;
import chiangmai.fang.monping.dev.utils.Contact;
import ss.com.bannerslider.adapters.SliderAdapter;
import ss.com.bannerslider.viewholder.ImageSlideViewHolder;

public class MainSliderAdapter extends SliderAdapter {

   public Context mcontext;
   public  List<slide_image> ListData;

    public MainSliderAdapter(Context mcontext, List<slide_image> listData) {
        this.mcontext = mcontext;
        this.ListData = listData;


    }

    @Override
    public int getItemCount() {
        return this.ListData.size();
    }

    @Override
    public void onBindImageSlide(int position, ImageSlideViewHolder viewHolder) {


            viewHolder.bindImageSlide(new Contact().Base_url+ListData.get(position).getPath_image());



    }
}
