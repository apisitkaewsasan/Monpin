package chiangmai.fang.monping.dev.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;

import chiangmai.fang.monping.dev.R;
import chiangmai.fang.monping.dev.model.ListData;
import chiangmai.fang.monping.dev.service.Api;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class YoutubeliveActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {
    public  String VIDEO_ID = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_youtubelive);
        load_data();


    }


    public void load_data(){
        Api.getClient().getLive().enqueue(new Callback<ListData>() {
            @Override
            public void onResponse(Call<ListData> call, Response<ListData> response) {
                if(response.body().getRetrun_code().equals("00")){
                    if(response.body().getLive().get(0).getYoutubeLive().equals("") || response.body().getLive().get(0).getYoutubeLive()==null){
                        alert("ไม่พบรายการในขณะนี้");
                    }else{
                        install_youtube(response.body().getLive().get(0).getKey_api(),response.body().getLive().get(0).getYoutubeLive());
                    }

                }else{
                    alert(response.body().getReturn_result_e());
                }
            }

            @Override
            public void onFailure(Call<ListData> call, Throwable t) {
                alert(t.getMessage());
            }
        });
    }

    public void install_youtube(String api,String live){
        com.google.android.youtube.player.YouTubePlayerView youTubePlayerView = (com.google.android.youtube.player.YouTubePlayerView) findViewById(R.id.youtube_player_view);
        youTubePlayerView.initialize(api, this);
        VIDEO_ID = live;
    }


    public void alert(String message){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage(message);
        builder1.setCancelable(true);
        builder1.setTitle("ข้อผิดพลาด");
        builder1.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        onBackPressed();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();
    }


    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {


        if(null== youTubePlayer) return;

        // Start buffering
        if (!b) {
            youTubePlayer.cueVideo(VIDEO_ID);
        }



        // Add listeners to YouTubePlayer instance
        youTubePlayer.setPlayerStateChangeListener(new com.google.android.youtube.player.YouTubePlayer.PlayerStateChangeListener() {
            @Override public void onAdStarted() { }
            @Override public void onError(com.google.android.youtube.player.YouTubePlayer.ErrorReason arg0) { }
            @Override public void onLoaded(String arg0) { }
            @Override public void onLoading() { }
            @Override public void onVideoEnded() { }
            @Override public void onVideoStarted() { }
        });


        youTubePlayer.setPlaybackEventListener(new com.google.android.youtube.player.YouTubePlayer.PlaybackEventListener() {
            @Override public void onBuffering(boolean arg0) { }
            @Override public void onPaused() { }
            @Override public void onPlaying() { }
            @Override public void onSeekTo(int arg0) { }
            @Override public void onStopped() { }
        });
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        Toast.makeText(this, "Failed to initialize.", Toast.LENGTH_LONG).show();

    }
}
