package chiangmai.fang.monping.dev.model;

import android.widget.Gallery;

import java.util.ArrayList;
import java.util.List;

public class ListData {
    List<Ebook> ebooks;
    List<vedio> vedio;
    List<Context_view> context_view;
    List<Context_view> vedio_rand;
    List<gallery> image_rand;
    List<slide_image> slide_image1;
    List<slide_image> slide_image2;
    List<slide_image> slide_image3;
    List<view_ebook> view_ebooks;
    List<contact> view_contact;
    List<About>  view_about;
    List<SyncScanner> view_scanner;
    List<Demo> demos;
    List<ReturnMessage> returns;
    List<location> location;
    List<youtubelive> live;
    List<checkin> checkin;
    String retrun_code;
    String return_result_e;
    int count_row;

    public ListData(List<Ebook> ebooks, List<chiangmai.fang.monping.dev.model.vedio> vedio, List<Context_view> context_view, List<Context_view> vedio_rand, List<gallery> image_rand, List<slide_image> slide_image1, List<slide_image> slide_image2, List<slide_image> slide_image3, List<view_ebook> view_ebooks, List<contact> view_contact, List<About> view_about, List<SyncScanner> view_scanner, List<Demo> demos, List<ReturnMessage> returns, List<chiangmai.fang.monping.dev.model.location> location, List<youtubelive> live, List<chiangmai.fang.monping.dev.model.checkin> checkin, String retrun_code, String return_result_e, int count_row) {
        this.ebooks = ebooks;
        this.vedio = vedio;
        this.context_view = context_view;
        this.vedio_rand = vedio_rand;
        this.image_rand = image_rand;
        this.slide_image1 = slide_image1;
        this.slide_image2 = slide_image2;
        this.slide_image3 = slide_image3;
        this.view_ebooks = view_ebooks;
        this.view_contact = view_contact;
        this.view_about = view_about;
        this.view_scanner = view_scanner;
        this.demos = demos;
        this.returns = returns;
        this.location = location;
        this.live = live;
        this.checkin = checkin;
        this.retrun_code = retrun_code;
        this.return_result_e = return_result_e;
        this.count_row = count_row;
    }

    public List<Ebook> getEbooks() {
        return ebooks;
    }

    public void setEbooks(List<Ebook> ebooks) {
        this.ebooks = ebooks;
    }

    public List<chiangmai.fang.monping.dev.model.vedio> getVedio() {
        return vedio;
    }

    public void setVedio(List<chiangmai.fang.monping.dev.model.vedio> vedio) {
        this.vedio = vedio;
    }

    public List<Context_view> getContext_view() {
        return context_view;
    }

    public void setContext_view(List<Context_view> context_view) {
        this.context_view = context_view;
    }

    public List<Context_view> getVedio_rand() {
        return vedio_rand;
    }

    public void setVedio_rand(List<Context_view> vedio_rand) {
        this.vedio_rand = vedio_rand;
    }

    public List<gallery> getImage_rand() {
        return image_rand;
    }

    public void setImage_rand(List<gallery> image_rand) {
        this.image_rand = image_rand;
    }

    public List<slide_image> getSlide_image1() {
        return slide_image1;
    }

    public void setSlide_image1(List<slide_image> slide_image1) {
        this.slide_image1 = slide_image1;
    }

    public List<slide_image> getSlide_image2() {
        return slide_image2;
    }

    public void setSlide_image2(List<slide_image> slide_image2) {
        this.slide_image2 = slide_image2;
    }

    public List<slide_image> getSlide_image3() {
        return slide_image3;
    }

    public void setSlide_image3(List<slide_image> slide_image3) {
        this.slide_image3 = slide_image3;
    }

    public List<view_ebook> getView_ebooks() {
        return view_ebooks;
    }

    public void setView_ebooks(List<view_ebook> view_ebooks) {
        this.view_ebooks = view_ebooks;
    }

    public List<contact> getView_contact() {
        return view_contact;
    }

    public void setView_contact(List<contact> view_contact) {
        this.view_contact = view_contact;
    }

    public List<About> getView_about() {
        return view_about;
    }

    public void setView_about(List<About> view_about) {
        this.view_about = view_about;
    }

    public List<SyncScanner> getView_scanner() {
        return view_scanner;
    }

    public void setView_scanner(List<SyncScanner> view_scanner) {
        this.view_scanner = view_scanner;
    }

    public List<Demo> getDemos() {
        return demos;
    }

    public void setDemos(List<Demo> demos) {
        this.demos = demos;
    }

    public List<ReturnMessage> getReturns() {
        return returns;
    }

    public void setReturns(List<ReturnMessage> returns) {
        this.returns = returns;
    }

    public List<chiangmai.fang.monping.dev.model.location> getLocation() {
        return location;
    }

    public void setLocation(List<chiangmai.fang.monping.dev.model.location> location) {
        this.location = location;
    }

    public List<youtubelive> getLive() {
        return live;
    }

    public void setLive(List<youtubelive> live) {
        this.live = live;
    }

    public List<chiangmai.fang.monping.dev.model.checkin> getCheckin() {
        return checkin;
    }

    public void setCheckin(List<chiangmai.fang.monping.dev.model.checkin> checkin) {
        this.checkin = checkin;
    }

    public String getRetrun_code() {
        return retrun_code;
    }

    public void setRetrun_code(String retrun_code) {
        this.retrun_code = retrun_code;
    }

    public String getReturn_result_e() {
        return return_result_e;
    }

    public void setReturn_result_e(String return_result_e) {
        this.return_result_e = return_result_e;
    }

    public int getCount_row() {
        return count_row;
    }

    public void setCount_row(int count_row) {
        this.count_row = count_row;
    }
}
