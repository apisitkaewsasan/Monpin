package chiangmai.fang.monping.dev.utils;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.stfalcon.frescoimageviewer.ImageViewer;

import chiangmai.fang.monping.dev.R;
import chiangmai.fang.monping.dev.activity.MenuActivity;
import chiangmai.fang.monping.dev.activity.collectionImage;

public class ImageOverlayView extends RelativeLayout {

    private TextView tvDescription;

    private String sharingText;

    private  TextView page_number;

    public  Context mContext;

    public ImageOverlayView(Context context) {
        super(context);
        this.mContext = context;
        init();
    }

    public ImageOverlayView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        init();
    }

    public ImageOverlayView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        init();
    }

    public void setDescription(String description) {
        tvDescription.setText(description);
    }

    public void setPage_number(String page_number) {
        this.page_number.setText(page_number);
    }

    public void setShareText(String text) {
        this.sharingText = text;
    }

    private void sendShareIntent() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, sharingText);
        sendIntent.setType("text/plain");
        getContext().startActivity(sendIntent);
    }

    private void init() {
        View view = inflate(getContext(), R.layout.view_image_overlay, this);
        tvDescription = (TextView) view.findViewById(R.id.tvDescription);
        page_number = (TextView)findViewById(R.id.page_number);
        view.findViewById(R.id.btnShare).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                sendShareIntent();
            }
        });


    }
}
