package chiangmai.fang.monping.dev.model;

public class SyncScanner {
    String topic_id;
    String topic_type;
    String dtl_name;
    String topic_name;

    public SyncScanner(String topic_id, String topic_type, String dtl_name, String topic_name) {
        this.topic_id = topic_id;
        this.topic_type = topic_type;
        this.dtl_name = dtl_name;
        this.topic_name = topic_name;
    }

    public String getTopic_id() {
        return topic_id;
    }

    public void setTopic_id(String topic_id) {
        this.topic_id = topic_id;
    }

    public String getTopic_type() {
        return topic_type;
    }

    public void setTopic_type(String topic_type) {
        this.topic_type = topic_type;
    }

    public String getDtl_name() {
        return dtl_name;
    }

    public void setDtl_name(String dtl_name) {
        this.dtl_name = dtl_name;
    }

    public String getTopic_name() {
        return topic_name;
    }

    public void setTopic_name(String topic_name) {
        this.topic_name = topic_name;
    }
}
