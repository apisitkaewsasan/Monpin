package chiangmai.fang.monping.dev.model;

public class checkin {
    String topic_id;
    String date_plase;
    String fullname;
    String path_image;
    String type;

    public checkin(String topic_id, String date_plase, String fullname, String path_image, String type) {
        this.topic_id = topic_id;
        this.date_plase = date_plase;
        this.fullname = fullname;
        this.path_image = path_image;
        this.type = type;
    }

    public String getTopic_id() {
        return topic_id;
    }

    public void setTopic_id(String topic_id) {
        this.topic_id = topic_id;
    }

    public String getDate_plase() {
        return date_plase;
    }

    public void setDate_plase(String date_plase) {
        this.date_plase = date_plase;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getPath_image() {
        return path_image;
    }

    public void setPath_image(String path_image) {
        this.path_image = path_image;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
