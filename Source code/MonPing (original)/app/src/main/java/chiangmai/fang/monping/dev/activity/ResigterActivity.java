package chiangmai.fang.monping.dev.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.florent37.tutoshowcase.TutoShowcase;
import com.squareup.picasso.Picasso;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import chiangmai.fang.monping.dev.Logo;
import chiangmai.fang.monping.dev.R;
import chiangmai.fang.monping.dev.model.ReturnMessage;
import chiangmai.fang.monping.dev.model.User;
import chiangmai.fang.monping.dev.service.Api;
import chiangmai.fang.monping.dev.service.GetUserCallback;
import chiangmai.fang.monping.dev.service.UserRequest;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ResigterActivity extends AppCompatActivity implements GetUserCallback.IGetUserResponse {
    ImageView select;
    Button signup;
    TextView fullname,username,tel;
    EditText password;
    byte[] imageBytes_ref;
    String filePath;
    String mediaPath;
    View focusView = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resigter);

        fullname = (TextView)findViewById(R.id.fullname);
        username = (TextView)findViewById(R.id.username);
        password = (EditText)findViewById(R.id.password);
        tel = (TextView)findViewById(R.id.tel);
        signup = findViewById(R.id.signup);



            fullname.setEnabled(false);
            username.setEnabled(false);
            password.setEnabled(false);
            tel.setEnabled(false);
            signup.setEnabled(false);


        select = findViewById(R.id.select);

        select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"),3);
            }
        });


        fullname.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                check_empty();
                if(id == R.id.next || id == EditorInfo.IME_NULL){

                    return true;
                }


                return false;
            }
        });

        username.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                check_empty();
                if(id == R.id.next || id == EditorInfo.IME_NULL){

                    return true;
                }


                return false;
            }
        });

        password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                check_empty();
                if(id == R.id.next || id == EditorInfo.IME_NULL){

                    return true;
                }


                return false;
            }
        });

        tel.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                check_empty();
                if (id == R.id.next || id == EditorInfo.IME_NULL) {
                    hideSoftKeyboard(ResigterActivity.this);

                    return true;
                }


                return false;
            }
        });



        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //File file = new File(filePath);
                check_empty();

            }
        });

        displayTuto();
    }


    protected void displayTuto() {
        TutoShowcase.from(this)
                .setContentView(R.layout.register_showcase1)
                .setListener(new TutoShowcase.Listener() {
                    @Override
                    public void onDismissed() {
                        displayTuto1();

                    }
                })
                .on(R.id.postion1)
                .addCircle()

                .withBorder()
                .show();
    }

    protected void displayTuto1() {
        TutoShowcase.from(this)
                .setContentView(R.layout.register_showcase2)
                .setListener(new TutoShowcase.Listener() {
                    @Override
                    public void onDismissed() {
                        displayTuto2();

                    }
                })
                .on(R.id.fullname)
                .addRoundRect()
                .withBorder()
                .onClick(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                })
                .show();
    }

    protected void displayTuto2() {
        TutoShowcase.from(this)
                .setContentView(R.layout.register_showcase3)
                .setListener(new TutoShowcase.Listener() {
                    @Override
                    public void onDismissed() {
                        displayTuto3();

                    }
                })
                .on(R.id.username)
                .addRoundRect()
                .withBorder()
                .onClick(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                })
                .show();
    }
    protected void displayTuto3() {
        TutoShowcase.from(this)
                .setContentView(R.layout.register_showcase3)
                .setListener(new TutoShowcase.Listener() {
                    @Override
                    public void onDismissed() {
                        displayTuto4();

                    }
                })
                .on(R.id.password)
                .addRoundRect()
                .withBorder()
                .onClick(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                })
                .show();
    }

    protected void displayTuto4() {
        TutoShowcase.from(this)
                .setContentView(R.layout.register_showcase4)
                .setListener(new TutoShowcase.Listener() {
                    @Override
                    public void onDismissed() {
                        displayTuto5();

                    }
                })
                .on(R.id.tel)
                .addRoundRect()
                .withBorder()
                .onClick(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                })
                .show();
    }

    protected void displayTuto5() {
        TutoShowcase.from(this)
                .setContentView(R.layout.register_showcase5)

                .on(R.id.signup)
                .addRoundRect()
                .withBorder()
                .onClick(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                })
                .show();
    }


    private  void check_empty(){

        fullname.setError(null);
        username.setError(null);
        password.setError(null);
        tel.setError(null);

        if(TextUtils.isEmpty(fullname.getText())){
            fullname.setError(getString(R.string.error_field_required));
            focusView = fullname;
            focusView.requestFocus();
        }else if(TextUtils.isEmpty(username.getText())){
            username.setError(getString(R.string.error_field_required));
            focusView = username;
            focusView.requestFocus();
        }else if(TextUtils.isEmpty(password.getText())){
            password.setError(getString(R.string.error_field_required));
            focusView = password;
            focusView.requestFocus();
        }else if(TextUtils.isEmpty(tel.getText())){
            tel.setError(getString(R.string.error_field_required));
            focusView = tel;
            focusView.requestFocus();
        }else if(tel.length() != 10){
            tel.setError(getString(R.string.error_field_required));
            focusView = tel;
            focusView.requestFocus();
        }else{
            uploadImage(imageBytes_ref,username.getText().toString(),fullname.getText().toString(),password.getText().toString(),tel.getText().toString());

        }
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }


    public byte[] getBytes(InputStream is) throws IOException {
        ByteArrayOutputStream byteBuff = new ByteArrayOutputStream();

        int buffSize = 1024;
        byte[] buff = new byte[buffSize];

        int len = 0;
        while ((len = is.read(buff)) != -1) {
            byteBuff.write(buff, 0, len);
        }

        return byteBuff.toByteArray();
    }


    private void uploadImage(byte[] imageBytes,String fullname,String username,String password,String tel) {


        RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpeg"), imageBytes);

        MultipartBody.Part body = MultipartBody.Part.createFormData("image", "image.jpg", requestFile);
        Api.getClient().uploadImage(body,fullname,username,password,tel).enqueue(new Callback<ReturnMessage>() {
            @Override
            public void onResponse(Call<ReturnMessage> call, Response<ReturnMessage> response) {
             try{
                // Toast.makeText(ResigterActivity.this, ""+response.body().getReturn_result(), Toast.LENGTH_LONG).show();
                 alert(response.body().getReturn_result());

             }catch (Exception e){
                // Toast.makeText(ResigterActivity.this, ""+response.code(), Toast.LENGTH_LONG).show();
                 alert("Error : "+response.code());
             }
            }

            @Override
            public void onFailure(Call<ReturnMessage> call, Throwable t) {
                Toast.makeText(ResigterActivity.this, "onFailure", Toast.LENGTH_LONG).show();
            }
        });

    }

    public void alert(String message){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage(message);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "รับทราบ",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent loginIntent = new Intent(ResigterActivity.this, FacebookLoginActivity.class);
                        startActivity(loginIntent);
                        finish();
                    }
                });

        builder1.setNegativeButton(
                "ยกเลิก",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        //Detects request codes
        if(requestCode==3 && resultCode == Activity.RESULT_OK) {
            Uri selectedImage = data.getData();
            Bitmap bitmap = null;
            try {


                Uri selectedMedia = data.getData();
                ContentResolver cr = getApplication().getContentResolver();
                String mime = cr.getType(selectedMedia);
                if(mime.toLowerCase().contains("image")) {
                    bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                    InputStream is = getContentResolver().openInputStream(data.getData());
                    imageBytes_ref = getBytes(is);
                    // uploadImage(getBytes(is),selectedImage);
                    Picasso.with(this).load(selectedImage).into(select);
                    fullname.setEnabled(true);
                    username.setEnabled(true);
                    password.setEnabled(true);
                    tel.setEnabled(true);
                    signup.setEnabled(true);

                }else{
                    Toast.makeText(this, "ต้องเป็นรูปเท่านั้น", Toast.LENGTH_SHORT).show();
                }



            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
        UserRequest.makeUserRequest(new GetUserCallback(ResigterActivity.this).getCallback());
    }

    @Override
    public void onCompleted(User user) {
        fullname.setText(user.getName());
        username.setText(user.getId());
        password.setText("");
        tel.setText("");
        Picasso.with(this).load(user.getPicture()).into(select);
    }
}
