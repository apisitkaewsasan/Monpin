package chiangmai.fang.monping.dev.adepter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


import com.github.vipulasri.timelineview.TimelineView;
import com.squareup.picasso.Picasso;

import chiangmai.fang.monping.dev.R;
import chiangmai.fang.monping.dev.model.Context_view;
import chiangmai.fang.monping.dev.model.location;
import chiangmai.fang.monping.dev.utils.AppUtils;
import chiangmai.fang.monping.dev.utils.Contact;
import chiangmai.fang.monping.dev.utils.RecycleViewClickListener;

public class TimeLineAdapter extends RecyclerView.Adapter<TimeLineAdapter.ViewHolder> {

    private  List<location> item;
    private Context context;
    TimeLineAdapter.OnLoadMoreListener loadMoreListener;
    boolean isLoading = false, isMoreDataAvailable = true;
    RecycleViewClickListener listener;
    private LayoutInflater mLayoutInflater;

    public TimeLineAdapter(Context context, List<location> item, RecycleViewClickListener listener) {
        this.context = context;
        this.item = item;
        this.listener = listener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView text_timeline_date,text_timeline_title,date_post;
        ImageView tip_image;
        TimelineView mTimelineView;

        public ViewHolder(View view) {
            super(view);
            text_timeline_title = (TextView) itemView.findViewById(R.id.text_timeline_title);
            text_timeline_date = (TextView) itemView.findViewById(R.id.text_timeline_date);
            tip_image = (ImageView) itemView.findViewById(R.id.tip_image);
            date_post = (TextView)itemView.findViewById(R.id.date_post);
            mTimelineView = (TimelineView) itemView.findViewById(R.id.time_marker);
            Typeface tf = Typeface.createFromAsset(itemView.getContext().getAssets(), "fonts/RSU_Regular.ttf");
            date_post.setTypeface(tf);
            text_timeline_title.setTypeface(tf);
            text_timeline_date.setTypeface(tf);
        }
    }


    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        mLayoutInflater = LayoutInflater.from(context);
        View view =mLayoutInflater.inflate(R.layout.item_timeline, viewGroup, false);


        ViewHolder viewHolder = new ViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(view, viewHolder.getPosition());
            }
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.mTimelineView.setMarker(ContextCompat.getDrawable(context, R.drawable.ic_check_box_black_24dp), ContextCompat.getColor(context, R.color.colorPrimary));
        holder.text_timeline_title.setText(item.get(position).getTopic_name());
        holder.text_timeline_date.setText(item.get(position).getContract_address());
        holder.date_post.setText(AppUtils.fb_date(item.get(position).getDate_plase()));
        Picasso.with(context).load(new Contact().Base_url+item.get(position).getPath_image()+"").resize(150, 100).into(holder.tip_image);
    }



    @Override
    public int getItemCount() {
        return item.size();
    }


    public interface OnLoadMoreListener{
        void onLoadMore();
    }

    public void setLoadMoreListener(TimeLineAdapter.OnLoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }
}
