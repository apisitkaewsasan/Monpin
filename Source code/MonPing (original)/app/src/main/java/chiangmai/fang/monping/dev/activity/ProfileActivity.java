package chiangmai.fang.monping.dev.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.squareup.picasso.Picasso;

import java.io.InputStream;

import chiangmai.fang.monping.dev.R;
import chiangmai.fang.monping.dev.model.ReturnMessage;
import chiangmai.fang.monping.dev.model.User;
import chiangmai.fang.monping.dev.service.Api;
import chiangmai.fang.monping.dev.service.GetUserCallback;
import chiangmai.fang.monping.dev.service.UserRequest;
import chiangmai.fang.monping.dev.utils.UserManager;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class ProfileActivity extends Activity implements GetUserCallback.IGetUserResponse {
    private SimpleDraweeView mProfilePhotoView;
    private TextView mName;
    private TextView mId;
    private TextView mEmail;
    private TextView mPermissions;
    private UserManager mManager;
    byte[] imageBytes_ref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);



        mManager  = new UserManager(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        UserRequest.makeUserRequest(new GetUserCallback(ProfileActivity.this).getCallback());
    }

    private void uploadImage(String image_url,String fullname,String username,String password,String tel) {


        Api.getClient().login_facebook(fullname,username,password,tel,image_url).enqueue(new Callback<ReturnMessage>() {
            @Override
            public void onResponse(Call<ReturnMessage> call, Response<ReturnMessage> response) {
                mManager.registerUser(username,"",fullname,"",image_url,"F");
                startActivity(new Intent(getApplicationContext(), Mainactivity.class).putExtra("activity","menu2"));

                finish();
            }

            @Override
            public void onFailure(Call<ReturnMessage> call, Throwable t) {
                Toast.makeText(ProfileActivity.this, "Error "+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void alert(String message){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage(message);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "รับทราบ",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent loginIntent = new Intent(ProfileActivity.this, FacebookLoginActivity.class);
                        startActivity(loginIntent);
                        finish();
                    }
                });

        builder1.setNegativeButton(
                "ยกเลิก",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    @Override
    public void onCompleted(User user) {
        uploadImage(user.getPicture().toString(),user.getName(),user.getId(),"***","***");

    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
