package chiangmai.fang.monping.dev.model;

public class vedio {
    int topic_id;
    String topic_name;
    String path_image;
    String youtube_key;
    String content1;
    String content2;

    public vedio(int topic_id, String topic_name, String path_image, String youtube_key, String content1, String content2) {
        this.topic_id = topic_id;
        this.topic_name = topic_name;
        this.path_image = path_image;
        this.youtube_key = youtube_key;
        this.content1 = content1;
        this.content2 = content2;
    }

    public int getTopic_id() {
        return topic_id;
    }

    public void setTopic_id(int topic_id) {
        this.topic_id = topic_id;
    }

    public String getTopic_name() {
        return topic_name;
    }

    public void setTopic_name(String topic_name) {
        this.topic_name = topic_name;
    }

    public String getPath_image() {
        return path_image;
    }

    public void setPath_image(String path_image) {
        this.path_image = path_image;
    }

    public String getYoutube_key() {
        return youtube_key;
    }

    public void setYoutube_key(String youtube_key) {
        this.youtube_key = youtube_key;
    }

    public String getContent1() {
        return content1;
    }

    public void setContent1(String content1) {
        this.content1 = content1;
    }

    public String getContent2() {
        return content2;
    }

    public void setContent2(String content2) {
        this.content2 = content2;
    }
}
