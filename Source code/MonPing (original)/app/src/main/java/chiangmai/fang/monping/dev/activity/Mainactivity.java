package chiangmai.fang.monping.dev.activity;

import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import java.util.List;

import chiangmai.fang.monping.dev.R;
import chiangmai.fang.monping.dev.adepter.MainSliderAdapter;
import chiangmai.fang.monping.dev.adepter.PicassoImageLoadingService;
import chiangmai.fang.monping.dev.adepter.RecyclerViewCustom;
import chiangmai.fang.monping.dev.adepter.RecyclerViewCustomVideo;
import chiangmai.fang.monping.dev.adepter.RecyclerViewSlideAdept3;
import chiangmai.fang.monping.dev.model.Ebook;
import chiangmai.fang.monping.dev.model.ListData;
import chiangmai.fang.monping.dev.model.slide_image;
import chiangmai.fang.monping.dev.model.vedio;
import chiangmai.fang.monping.dev.service.Api;
import chiangmai.fang.monping.dev.utils.Contact;
import chiangmai.fang.monping.dev.utils.GPSTracker;
import chiangmai.fang.monping.dev.utils.RecycleViewClickListener;
import chiangmai.fang.monping.dev.utils.UserManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ss.com.bannerslider.Slider;
import ss.com.bannerslider.event.OnSlideClickListener;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
@TargetApi(Build.VERSION_CODES.M)
@RequiresApi(api = Build.VERSION_CODES.M)
public class Mainactivity extends AppCompatActivity implements View.OnScrollChangeListener, View.OnClickListener, NavigationView.OnNavigationItemSelectedListener {
    private Slider slider;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private RecyclerView mRecyclerView_vedio,mRecyclerView_slide3,mRecyclerView_slide4;
    private RecyclerView.Adapter mAdapter_vedio;
    private RecyclerView.LayoutManager mLayoutManager_vedio;

    private ScrollView main_scroller;

    private ImageView imgFavorite,prve_vedio,next_vedio,prve_book,next_book,menu1,menu2,menu3,menu4;
    int vedio_temp=0;
    int book_temp = 0;
    int slide3_temp=0;
    int slide4_temp = 0;
    boolean Status_Scroll_Vedio = false;
    boolean Status_Scroll_Book = false;
    boolean Status_Scroll_slide3 = false;
    boolean Status_Scroll_slide4 = false;
    private TextView textView10;
    private LinearLayout qrcode;
    private DrawerLayout drawer;
    SharedPreferences mPrefs = null;
    private static  final String TAG = Mainactivity.class.getSimpleName();
    NavigationView navigationView;
    private static final int PERMISSION_REQUEST_CODE = 200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_mainactivity);


        mPrefs = this.getSharedPreferences("lend_system", Context.MODE_PRIVATE);
       
        Toolbar toolbarWidget;

        toolbarWidget = (Toolbar) findViewById(R.id.toolbar1);

        //toolbarWidget.setTitle("Toolbar With Icon");
        toolbarWidget.setBackgroundColor(Color.parseColor("#303F9F"));
        toolbarWidget.setTitle("");
        toolbarWidget.setNavigationIcon(R.drawable.top_icon);


        setSupportActionBar(toolbarWidget);
        Slider.init(new PicassoImageLoadingService(this));

        menu1 = (ImageView)findViewById(R.id.menu1);
        menu1.setOnClickListener(this);
        menu2 = (ImageView)findViewById(R.id.menu2);
        menu2.setOnClickListener(this);
        menu3 = (ImageView)findViewById(R.id.menu3);
        menu3.setOnClickListener(this);
        menu4= (ImageView)findViewById(R.id.menu4);
        menu4.setOnClickListener(this);

        getEbookData();


        imgFavorite = (ImageView) findViewById(R.id.gotop);
        imgFavorite.setClickable(true);
        imgFavorite.setVisibility(View.GONE);
        main_scroller = (ScrollView) findViewById(R.id.scrollView2);
            imgFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ObjectAnimator anim = ObjectAnimator.ofFloat(main_scroller, "alpha", 0f, 90f);
                anim.setDuration(2000);                  // Duration in milliseconds
                //anim.setInterpolator();  // E.g. Linear, Accelerate, Decelerate
                anim.start();

                main_scroller.scrollTo(0, 0);
            }
        });

       // main_scroller.setOnScrollChangeListener(this);







    }

    public  void allowkPermission(){
        if (!checkPermission()) {

            showMessageOKCancel("แอพพลิเคชั่นต้องการขออนุญาติเข้าใช้ข้อมูลตำแหน่งของพื้นที่",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                requestPermissions(new String[]{ACCESS_FINE_LOCATION},
                                        PERMISSION_REQUEST_CODE);
                            }
                        }
                    });

        }
    }

    private void install_Slide(List<slide_image>  ListData){
        slider = findViewById(R.id.banner_slider1);

        //delay for testing empty view functionality
        //delay for testing empty view functionality
        slider.postDelayed(new Runnable() {
            @Override
            public void run() {
                slider.setAdapter(new MainSliderAdapter(Mainactivity.this,ListData));


                slider.setOnSlideClickListener(new OnSlideClickListener() {
                    @Override
                    public void onSlideClick(int position) {
                        moveStaivity(ListData.get(position).getTopic_id(),ListData.get(position).getTopic_type());

                    }
                });
            }
        }, 1500);



        slider.setInterval(5000);
        slider.setAnimateIndicators(true);




    }


    private void getEbookData() {
        // display a progress dialog
        final ProgressDialog progressDialog = new ProgressDialog(Mainactivity.this);
        progressDialog.setCancelable(false); // set cancelable to false
        progressDialog.setMessage("กำลังโหลดข้อมูล..."); // set message
        progressDialog.show(); // show progress dialog

        Api.getClient().getEbook().enqueue(new Callback<ListData>() {
            @Override
            public void onResponse(Call<ListData> call, Response<ListData> response) {
                try{

                    if(response.body().getRetrun_code().equals("99") || response.body().getRetrun_code() =="99"){


                        progressDialog.dismiss();
                        alert(response.body().getReturn_result_e());


                    }else{
                        Install_Slider_Book(response.body().getEbooks());
                        Install_Slider_Vdeio(response.body().getVedio());
                        install_Slide(response.body().getSlide_image1());
                        Install_Slider_slide3(response.body().getSlide_image2());
                        Install_Slider_slide4(response.body().getSlide_image3());
                        // Toast.makeText(Mainactivity.this, "OK ", Toast.LENGTH_SHORT).show();
                        allowkPermission();
                        progressDialog.dismiss();
                        

                    }


                }catch (Exception e){
                    progressDialog.dismiss();
                    alert(e.getMessage());
                }

            }

            @Override
            public void onFailure(Call<ListData> call, Throwable t) {

                alert(t.getMessage());
                progressDialog.dismiss(); //dismiss progress dialog
            }
        });
    }



    public void Install_Slider_Book(List<Ebook>  ListData){
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_book);

        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new RecyclerViewCustom(this, ListData, new RecycleViewClickListener() {
            @Override
            public void onItemClick(View v, int position) {

                moveStaivity(ListData.get(position).getTopic_id()+"","E");

            }
        });
        mRecyclerView.setAdapter(mAdapter);

        next_book = (ImageView)findViewById(R.id.next_book);
        next_book.setClickable(true);
       // textView10 = (TextView)findViewById(R.id.textView10);
        next_book.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {

                Status_Scroll_Book = true;


                if(book_temp<(ListData.size()-1)){
                    book_temp=book_temp+3;
                }


                    //textView10.setText(""+book_temp);
                    ((LinearLayoutManager)mRecyclerView.getLayoutManager()).scrollToPositionWithOffset(book_temp,10);



            }
        });

        prve_book = (ImageView)findViewById(R.id.prve_book);
        prve_book.setClickable(true);

        prve_book.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {

                Status_Scroll_Book = true;

                if(book_temp>0){
                    book_temp=book_temp-3;
                }



                    ((LinearLayoutManager)mRecyclerView.getLayoutManager()).scrollToPositionWithOffset(book_temp,10);
                   // textView10.setText(""+book_temp);

            }
        });

        mRecyclerView.setNestedScrollingEnabled(true);
        mRecyclerView.canScrollVertically(1);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if(Status_Scroll_Book == false){
                    try{
                        if (dx > 0) {
                            book_temp=getCurrentItem_book();
                            //textView10.setText(""+book_temp);
                        } else {
                            book_temp=getCurrentItem_book();
                            //textView10.setText(""+book_temp);
                        }
                    }catch (Exception e){

                    }
                }


            }

        });

        mRecyclerView.setOnFlingListener(new RecyclerView.OnFlingListener() {
            @Override
            public boolean onFling(int i, int i1) {
                Status_Scroll_Book = false;
                return false;
            }
        });


        qrcode = (LinearLayout) findViewById(R.id.qrcode);
        qrcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(
                        new Intent(getBaseContext(), CodeScannerActivity.class)
                );
            }
        });

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView navigationView1 = (NavigationView) findViewById(R.id.nav_view);
        navigationView1.setNavigationItemSelectedListener(this);

        View hView =  navigationView1.inflateHeaderView(R.layout.nav_header_main);
        ImageView imgvw = (ImageView)hView.findViewById(R.id.icon_profile);
        if(mPrefs.getString("flaug","").equals("F")){
            Picasso.with(this).load(mPrefs.getString("image","")).into(imgvw);
        }else{
            Picasso.with(this).load(new Contact().Base_url+mPrefs.getString("image","")).into(imgvw);
        }


    }



    public void moveStaivity(String value,String index){

        switch (String.valueOf(index.toCharArray()[0])){
            case "N":
                startActivity(new Intent(getBaseContext(), MenuActivity.class).putExtra("topic_id",value).putExtra("activity",convert_menu(index)));
                break;
            case "V":
                startActivity(new Intent(getBaseContext(), VideoActivity.class).putExtra("Topic_id",value));
                break;
            case "E":
                startActivity(new Intent(getBaseContext(), EbookActivity.class).putExtra("Topic_id",value));
                break;

        }


    }

    public String convert_menu(String index){
        String out= "";
        if(index.equals("N1") || index.equals("N2") ||index.equals("N3")){
            out= "menu2";
        }else if(index.equals("N4") || index.equals("N5") ){
            out = "menu3";
        }else if(index.equals("N6")){
            out = "menu4";
        }
        return out;
    }

    public void Install_Slider_Vdeio(List<vedio> list_vedio){

        mRecyclerView_vedio = (RecyclerView) findViewById(R.id.recycler_view);

        mRecyclerView_vedio.setHasFixedSize(true);

        mLayoutManager_vedio = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        mRecyclerView_vedio.setLayoutManager(mLayoutManager_vedio);

        mAdapter_vedio = new RecyclerViewCustomVideo(this,list_vedio,(this.getWindowManager().getDefaultDisplay().getWidth()/3)+45, new RecycleViewClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                moveStaivity(list_vedio.get(position).getTopic_id()+"","V");
            }
        });
        mRecyclerView_vedio.setAdapter(mAdapter_vedio);

        next_vedio = (ImageView)findViewById(R.id.next_vedio);
        next_vedio.setClickable(true);

        next_vedio.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {

                Status_Scroll_Vedio = true;

                if(vedio_temp>0){
                    vedio_temp=vedio_temp-2;
                }

                if(vedio_temp!=list_vedio.size()){
                    ((LinearLayoutManager)mRecyclerView_vedio.getLayoutManager()).scrollToPositionWithOffset(vedio_temp,10);
                }else{
                    vedio_temp = 0;
                }
            }
        });

        prve_vedio = (ImageView)findViewById(R.id.prev_vedio);
        prve_vedio.setClickable(true);

        prve_vedio.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                Status_Scroll_Vedio = true;
                if(vedio_temp!=list_vedio.size()){
                    vedio_temp=vedio_temp+2;
                }

                if(vedio_temp!=list_vedio.size()){
                    ((LinearLayoutManager)mRecyclerView_vedio.getLayoutManager()).scrollToPositionWithOffset(vedio_temp,10);
               }else{
                    vedio_temp = vedio_temp-2;
                }

            }
        });



        mRecyclerView_vedio.setNestedScrollingEnabled(true);
        mRecyclerView_vedio.canScrollVertically(1);
        mRecyclerView_vedio.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if(Status_Scroll_Vedio == false){
                    try{
                        if (dx > 0) {
                            vedio_temp=getCurrentItem()+1;
                        } else {
                            vedio_temp=getCurrentItem();
                        }
                    }catch (Exception e){

                    }
                }


            }

        });

        mRecyclerView_vedio.setOnFlingListener(new RecyclerView.OnFlingListener() {
            @Override
            public boolean onFling(int i, int i1) {
                Status_Scroll_Vedio = false;
                return false;
            }
        });

    }



    public void Install_Slider_slide3(List<slide_image>  slide3){



        mRecyclerView_slide3 = (RecyclerView) findViewById(R.id.recycler_view_slide3);

        mRecyclerView_slide3.setHasFixedSize(true);

        LinearLayoutManager mLayoutManager_slide3 = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        mRecyclerView_slide3.setLayoutManager(mLayoutManager_slide3);

        RecyclerViewSlideAdept3 mAdapter_vedio = new RecyclerViewSlideAdept3(this,slide3,(this.getWindowManager().getDefaultDisplay().getWidth()/3)+45, new RecycleViewClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                moveStaivity(slide3.get(position).getTopic_id()+"",slide3.get(position).getTopic_type());
            }
        });
        mRecyclerView_slide3.setAdapter(mAdapter_vedio);

        ImageView next_slide3 = (ImageView)findViewById(R.id.next_slide3);
        next_slide3.setClickable(true);

        next_slide3.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {

                Status_Scroll_slide3 = true;

                if(slide3_temp>0){
                    slide3_temp=slide3_temp-2;
                }

                if(slide3_temp!=slide3.size()){
                    ((LinearLayoutManager)mRecyclerView_slide3.getLayoutManager()).scrollToPositionWithOffset(slide3_temp,10);
                }else{
                    slide3_temp = 0;
                }
            }
        });

        ImageView prve_slide3 = (ImageView)findViewById(R.id.prev_slide3);
        prve_slide3.setClickable(true);

        prve_slide3.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                Status_Scroll_slide3 = true;
                if(slide3_temp!=slide3.size()){
                    slide3_temp=slide3_temp+2;
                }

                if(slide3_temp!=slide3.size()){
                    ((LinearLayoutManager)mRecyclerView_slide3.getLayoutManager()).scrollToPositionWithOffset(slide3_temp,10);
                }else{
                    slide3_temp = slide3_temp-2;
                }

            }
        });



        mRecyclerView_slide3.setNestedScrollingEnabled(true);
        mRecyclerView_slide3.canScrollVertically(1);
        mRecyclerView_slide3.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if(Status_Scroll_slide3 == false){
                    try{
                        if (dx > 0) {
                            slide3_temp=getCurrentItem_slide3()+1;
                        } else {
                            slide3_temp=getCurrentItem_slide3();
                        }
                    }catch (Exception e){

                    }
                }


            }

        });

        mRecyclerView_slide3.setOnFlingListener(new RecyclerView.OnFlingListener() {
            @Override
            public boolean onFling(int i, int i1) {
                Status_Scroll_slide3 = false;
                return false;
            }
        });

    }


    public void Install_Slider_slide4(List<slide_image> slide4){

        mRecyclerView_slide4 = (RecyclerView) findViewById(R.id.recycler_view_slide4);

        mRecyclerView_slide4.setHasFixedSize(true);

        LinearLayoutManager mLayoutManager_slide3 = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        mRecyclerView_slide4.setLayoutManager(mLayoutManager_slide3);

        RecyclerViewSlideAdept3 mAdapter_vedio = new RecyclerViewSlideAdept3(this,slide4,(this.getWindowManager().getDefaultDisplay().getWidth()/3)+45, new RecycleViewClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                moveStaivity(slide4.get(position).getTopic_id()+"",slide4.get(position).getTopic_type());
            }
        });
        mRecyclerView_slide4.setAdapter(mAdapter_vedio);

        ImageView next_slide4 = (ImageView)findViewById(R.id.next_slide4);
        next_slide4.setClickable(true);

        next_slide4.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {

                Status_Scroll_slide4 = true;

                if(slide4_temp>0){
                    slide4_temp=slide4_temp-2;
                }

                if(slide4_temp!=slide4.size()){
                    ((LinearLayoutManager)mRecyclerView_slide4.getLayoutManager()).scrollToPositionWithOffset(slide4_temp,10);
                }else{
                    slide4_temp = 0;
                }

            }
        });

        ImageView prev_slide4 = (ImageView)findViewById(R.id.prev_slide4);
        prev_slide4.setClickable(true);

        prev_slide4.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                Status_Scroll_slide3 = true;
                if(slide4_temp!=slide4.size()){
                    slide4_temp=slide4_temp+2;
                }

                if(slide4_temp!=slide4.size()){
                    ((LinearLayoutManager)mRecyclerView_slide4.getLayoutManager()).scrollToPositionWithOffset(slide4_temp,10);
                }else{
                    slide4_temp = slide4_temp-2;
                }

            }
        });



        mRecyclerView_slide4.setNestedScrollingEnabled(true);
        mRecyclerView_slide4.canScrollVertically(1);
        mRecyclerView_slide4.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if(Status_Scroll_slide4 == false){
                    try{
                        if (dx > 0) {
                            slide4_temp=getCurrentItem_slide4()+1;
                        } else {
                            slide4_temp=getCurrentItem_slide4();
                        }
                    }catch (Exception e){

                    }
                }


            }

        });

        mRecyclerView_slide4.setOnFlingListener(new RecyclerView.OnFlingListener() {
            @Override
            public boolean onFling(int i, int i1) {
                Status_Scroll_slide4 = false;
                return false;
            }
        });

    }


    private int getCurrentItem(){
        return ((LinearLayoutManager)mRecyclerView_vedio.getLayoutManager())
                .findFirstVisibleItemPosition();
    }

    private int getCurrentItem_book(){
        return ((LinearLayoutManager)mRecyclerView.getLayoutManager())
                .findFirstVisibleItemPosition();
    }

    private int getCurrentItem_slide3(){
        return ((LinearLayoutManager)mRecyclerView_slide3.getLayoutManager())
                .findFirstVisibleItemPosition();
    }

    private int getCurrentItem_slide4(){
        return ((LinearLayoutManager)mRecyclerView_slide4.getLayoutManager())
                .findFirstVisibleItemPosition();
    }






    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_action:
                if (drawer.isDrawerOpen(GravityCompat.END)) {
                    drawer.closeDrawer(GravityCompat.END);
                } else {
                    drawer.openDrawer(GravityCompat.END);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onScrollChange(View view, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {


        if(scrollY >= view.getHeight() - 800){
            System.out.println("view : "+view.getScrollY()+" scrollX "+scrollX+" scrollY "+scrollY+" oldScrollX "+oldScrollX+" oldScrollY "+oldScrollY);
           // imgFavorite.setVisibility(View.VISIBLE);

        }else{
           // imgFavorite.setVisibility(View.GONE);
        }
   }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.menu1:
               // startActivity(new Intent(getBaseContext(), MenuActivity.class).putExtra("activity","menu1"));
                startActivity(new Intent(getBaseContext(), Menu1Activity.class));
                break;
            case R.id.menu2:
               // startActivity(new Intent(getBaseContext(), MenuActivity.class).putExtra("activity","menu2"));
                startActivity(new Intent(getBaseContext(), Menu2Activity.class));

                break;
            case R.id.menu3:
                //startActivity(new Intent(getBaseContext(), MenuActivity.class).putExtra("activity","menu3"));
                startActivity(new Intent(getBaseContext(), Menu3Activity.class));

                break;
            case R.id.menu4:
               // startActivity(new Intent(getBaseContext(), MenuActivity.class).putExtra("activity","menu4"));
                startActivity(new Intent(getBaseContext(), Menu4Activity.class));

                break;
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();
        String text = "";
        if (id == R.id.menu1) {
            startActivity(new Intent(getBaseContext(), Menu1Activity.class).putExtra("activity","menu1"));
        } else if (id == R.id.menu2) {
            startActivity(new Intent(getBaseContext(), Menu2Activity.class).putExtra("activity","menu2"));
        } else if (id == R.id.menu3) {
            startActivity(new Intent(getBaseContext(), Menu3Activity.class).putExtra("activity","menu3"));
        } else if (id == R.id.menu4) {
            startActivity(new Intent(getBaseContext(), Menu4Activity.class).putExtra("activity","menu4"));

        } else if (id == R.id.nav_share) {
            startActivity(new Intent(getBaseContext(), ContactActivity.class));
        } else if (id == R.id.nav_send) {
            startActivity(new Intent(getBaseContext(), AboutActivity.class));
        }else if(id == R.id.nav_timeline){
            startActivity(new Intent(getBaseContext(), TimelineActivity.class));
        }else if(id == R.id.nav_live){
            startActivity(new Intent(getBaseContext(), YoutubeliveActivity.class));
        }
        //Toast.makeText(this, "You have chosen " + text, Toast.LENGTH_LONG).show();
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        //rawer.closeDrawer(GravityCompat);
        drawer.closeDrawer(GravityCompat.END);
        return false;
    }

    public void alert(String message){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage(message);
        builder1.setTitle("เกืดข้อผิดพลาด");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);

    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), ACCESS_FINE_LOCATION);

        return result == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {

                    boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                    if (locationAccepted) {
                        //Toast.makeText(this, "Yes", Toast.LENGTH_SHORT).show();
                        new UserManager(this).insert_location(new GPSTracker(this).getLatitude() + "", new GPSTracker(this).getLongitude() + "");
                        // Snackbar.make(this, "Permission Granted, Now you can access location data and camera.", Snackbar.LENGTH_LONG).show();
                    }else {
                        new UserManager(this).insert_location(0.0 + "", 0.0  + "");
                       // Toast.makeText(this, "No", Toast.LENGTH_SHORT).show();
                       // Snackbar.make(view, "Permission Denied, You cannot access location data and camera.", Snackbar.LENGTH_LONG).show();

                    }
                }


                break;
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new android.support.v7.app.AlertDialog.Builder(this)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("อนุญาติ", okListener)
                .setNegativeButton("ไม่อนุญาติ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        new UserManager(getApplicationContext()).insert_location(0.0 + "", 0.0  + "");
                    }
                })
                .create()
                .show();
    }

    @Override
    public void onBackPressed() {

    }
}
