package chiangmai.fang.monping.dev.service;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;

public class InteractiveScrollView extends ScrollView {

    OnBottomReachedListener mListener;

    public InteractiveScrollView(Context context) {
        super(context);
    }

    public InteractiveScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public InteractiveScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public InteractiveScrollView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public OnBottomReachedListener getmListener() {
        return mListener;
    }

    public void setmListener(OnBottomReachedListener mListener) {
        this.mListener = mListener;
    }

    public interface OnBottomReachedListener{
        public void onBottomReached();
    }
}
