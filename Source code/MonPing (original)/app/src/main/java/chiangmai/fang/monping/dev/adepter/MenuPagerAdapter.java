package chiangmai.fang.monping.dev.adepter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import chiangmai.fang.monping.dev.activity.Pager1Fragment;
import chiangmai.fang.monping.dev.activity.Pager2Fragment;

public class MenuPagerAdapter  extends FragmentPagerAdapter {
    int mNumOfTabs;
    public MenuPagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int index) {

        switch (index) {
            case 0:
                // Top Rated fragment activity
                return new Pager1Fragment();
            case 1:
                // Top Rated fragment activity
                return new Pager2Fragment();

        }

        return null;
    }

    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return mNumOfTabs;
    }

}
