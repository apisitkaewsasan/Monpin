package chiangmai.fang.monping.dev.model;

public class location {
    String topic_id;
    String topic_name;
    String username;
    String fullname;
    String date_plase;
    String contract_address;
    String path_image;
    String topic_type;

    public location(String topic_id, String topic_name, String username, String fullname, String date_plase, String contract_address, String path_image, String topic_type) {
        this.topic_id = topic_id;
        this.topic_name = topic_name;
        this.username = username;
        this.fullname = fullname;
        this.date_plase = date_plase;
        this.contract_address = contract_address;
        this.path_image = path_image;
        this.topic_type = topic_type;
    }

    public String getTopic_id() {
        return topic_id;
    }

    public void setTopic_id(String topic_id) {
        this.topic_id = topic_id;
    }

    public String getTopic_name() {
        return topic_name;
    }

    public void setTopic_name(String topic_name) {
        this.topic_name = topic_name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getDate_plase() {
        return date_plase;
    }

    public void setDate_plase(String date_plase) {
        this.date_plase = date_plase;
    }

    public String getContract_address() {
        return contract_address;
    }

    public void setContract_address(String contract_address) {
        this.contract_address = contract_address;
    }

    public String getPath_image() {
        return path_image;
    }

    public void setPath_image(String path_image) {
        this.path_image = path_image;
    }

    public String getTopic_type() {
        return topic_type;
    }

    public void setTopic_type(String topic_type) {
        this.topic_type = topic_type;
    }
}
