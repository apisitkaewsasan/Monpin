package chiangmai.fang.monping.dev.activity;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LevelListDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.flaviofaria.kenburnsview.KenBurnsView;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;
import com.pixplicity.htmlcompat.HtmlCompat;
import com.squareup.picasso.Picasso;
import com.stfalcon.frescoimageviewer.ImageViewer;

import org.w3c.dom.Text;
import org.xml.sax.Attributes;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import chiangmai.fang.monping.dev.R;
import chiangmai.fang.monping.dev.adepter.CheckingAdepter;
import chiangmai.fang.monping.dev.adepter.FragmentBAdapter;
import chiangmai.fang.monping.dev.model.Context_view;
import chiangmai.fang.monping.dev.model.CustomImage;
import chiangmai.fang.monping.dev.model.Demo;
import chiangmai.fang.monping.dev.model.Ebook;
import chiangmai.fang.monping.dev.model.ListData;
import chiangmai.fang.monping.dev.model.checkin;
import chiangmai.fang.monping.dev.model.gallery;
import chiangmai.fang.monping.dev.service.Api;
import chiangmai.fang.monping.dev.utils.AppUtils;
import chiangmai.fang.monping.dev.utils.Contact;
import chiangmai.fang.monping.dev.utils.EndlessRecyclerViewScrollListener;
import chiangmai.fang.monping.dev.utils.ImageOverlayView;
import chiangmai.fang.monping.dev.utils.RecycleViewClickListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class MenuActivity extends AppCompatActivity implements HtmlCompat.ImageGetter, NavigationView.OnNavigationItemSelectedListener {

    ConstraintLayout layout_image;
    private LinearLayout LinearLayout;
    private ViewGroup mainLayout;
    private TextView header,header_detail,collectionImage,contract_name;
    private TextView text_bar,context1,context2,Error;
    private ImageView showimage,showimage1,image_context,icon_page,select_font,checkin_btn;
    private  ImageView background;
    private FrameLayout page_mask;
    private ImageOverlayView overlayView;
    private List<gallery> images;
    public  ImageViewer cc;
    private DrawerLayout drawer;
    Spanned fromHtml;
    SharedPreferences mPrefs = null;
    private static final String TAG = MenuActivity.class.getSimpleName();
    protected String[] posters, descriptions,image;
    public  String VIDEO_ID = "";
    KenBurnsView header_bg;
    View customView;
     MaterialStyledDialog.Builder dialogHeader;
    private ScrollView main_scroller;
    private ProgressBar progressBar;
    List<checkin> e;
    CheckingAdepter checkingadepter;
    int Count_row=2;


    private EndlessRecyclerViewScrollListener scrollListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        header_bg= (KenBurnsView)findViewById(R.id.background);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        Error = (TextView)findViewById(R.id.Error);
        page_mask = (FrameLayout)findViewById(R.id.page_mask);
        layout_image = (ConstraintLayout) findViewById(R.id.layout_image);

        setSupportActionBar(toolbar);


        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        getEbookData();

        mPrefs = this.getSharedPreferences("lend_system", Context.MODE_PRIVATE);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView navigationView1 = (NavigationView) findViewById(R.id.nav_view);
        navigationView1.setNavigationItemSelectedListener(this);




        icon_page = (ImageView)findViewById(R.id.icon_page);
        background = (ImageView)findViewById(R.id.background);

        checkin_btn = (ImageView)findViewById(R.id.checkin);

        checkin_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {e = new ArrayList<>();
                e = new ArrayList<>();
                checkin_btn.setEnabled(false);
                load_checking(Count_row);

            }
        });


    }

    private void install_header(String url){
       String data = getIntent().getExtras().getString("activity");
        if(data.equals("menu1")){
            icon_page.setImageResource(R.drawable.circle1);
        }else if(data.equals("menu2")){
            icon_page.setImageResource(R.drawable.circle2);


        }else if(data.equals("menu3")){
            icon_page.setImageResource(R.drawable.circle3);
            Resources res = getResources(); //resource handle

        }else if(data.equals("menu4")){
            icon_page.setImageResource(R.drawable.circle4);

        }
        Picasso.with(this).load(url).into(header_bg);

    }



    public void load_checking(int index){
        Api.getClient().getCheckIn(getIntent().getExtras().getString("topic_id"),index).enqueue(new Callback<ListData>() {
            @Override
            public void onResponse(Call<ListData> call, Response<ListData> response) {
               // Toast.makeText(MenuActivity.this, ""+response.body().getCheckin().size(), Toast.LENGTH_SHORT).show();
                     if(response.body().getRetrun_code().equals("00")){
                       //  setDialogsRecyclerView(response.body().getCheckin(),response.body().getCount_row());
                         e.clear();
                         e.addAll(response.body().getCheckin());
                        // Count_row = response.body().getCount_row();
                         if(index==2){
                             setDialogsRecyclerView(response.body().getCount_row());
                         }
                         checkingadepter.notifyDataSetChanged();

                     }else{
                         dialogHeader(0,"แจ้งจากระบบ",response.body().getReturn_result_e());
                     }

            }

            @Override
            public void onFailure(Call<ListData> call, Throwable t) {

            }

        });

    }

    public void setDialogsRecyclerView(int index){
        // Button dismissButton = (Button)customView.findViewById(R.id.custom_button);
        dialogHeader(index,"การค้นหาข้อมูล","ไม่พบรายการค้นหา");

       // TextView header_text = (TextView)customView.findViewById(R.id.header_text);
        RecyclerView recycler = (RecyclerView)customView.findViewById(R.id.recycler);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this,1);
        recycler.setLayoutManager(gridLayoutManager); // set LayoutManager to RecyclerView
        recycler.setItemAnimator(new DefaultItemAnimator());
       // header_text.setText("บุคคลที่เคยมาเยี่ยมชม : "+Count_row +" คน");

        checkingadepter = new CheckingAdepter(this,e, new RecycleViewClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                //startActivity(new Intent(getContext().getApplicationContext(), MenuActivity.class).putExtra("activity","menu2"));
                //  statactivity(e.get(position).getTopic_id()+"");
            }
        });

        recycler.setAdapter(checkingadepter);
        scrollListener = new EndlessRecyclerViewScrollListener(gridLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {

                if(e.size()<index){
                    load_checking(e.size()+2);
                }
            }


        };
        recycler.addOnScrollListener(scrollListener);


    }

    public  void dialogHeader(int Count_row,String title,String Message){
        if(Count_row==0){
            LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            customView = inflater.inflate(R.layout.custom_view,null);
            dialogHeader = new MaterialStyledDialog.Builder(MenuActivity.this)
                    .setHeaderDrawable(R.drawable.map)
                    .setIcon(new IconicsDrawable(MenuActivity.this).icon(MaterialDesignIconic.Icon.gmi_map).color(Color.WHITE))
                    .withDialogAnimation(true)
                    .setCancelable(false)
                    .setTitle(title)
                    .setDescription(Message)
                    .setPositiveText("ปิดหน้าต่าง")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            // startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://github.com/javiersantos")));
                            checkin_btn.setEnabled(true);
                        }
                    });
            dialogHeader.show();
        }else{
            LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            customView = inflater.inflate(R.layout.custom_view,null);
            dialogHeader = new MaterialStyledDialog.Builder(MenuActivity.this)
                    .setHeaderDrawable(R.drawable.map)
                    .setIcon(new IconicsDrawable(MenuActivity.this).icon(MaterialDesignIconic.Icon.gmi_map).color(Color.WHITE))
                    .withDialogAnimation(true)
                    .setCancelable(false)
                    .setTitle("บุคคลที่เคยมาเยี่ยมชม : "+Count_row +" คน")
                    .setCustomView(customView, 20, 0, 20, 0)
                    .setPositiveText("ปิดหน้าต่าง")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            // startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://github.com/javiersantos")));
                            checkin_btn.setEnabled(true);
                        }
                    });
            dialogHeader.show();
        }

    }


    private void install_youtube(String key){

        progressBar = (ProgressBar)findViewById(R.id.progressBar);

        String frameVideo = "<html><body>Youtube video .. <br> <iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/"+key+"\" frameborder=\"0\" allowfullscreen></iframe></body></html>";

        WebView displayVideo = (WebView)findViewById(R.id.webView);
        displayVideo.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        if(key==null || key == ""){
            displayVideo.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
        }else{
            WebSettings webSettings = displayVideo.getSettings();
            webSettings.setJavaScriptEnabled(true);
            displayVideo.loadData(frameVideo, "text/html", "utf-8");
        }
        displayVideo.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                // TODO show you progress image
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                // TODO hide your progress image
                progressBar.setVisibility(View.GONE);
                super.onPageFinished(view, url);
            }
        });

    }

    private void getEbookData() {
        // display a progress dialog
        final ProgressDialog progressDialog = new ProgressDialog(MenuActivity.this);
        progressDialog.setCancelable(false); // set cancelable to false
        progressDialog.setMessage("กำลังโหลดข้อมูล...!"); // set message
        progressDialog.show(); // show progress dialog

        // Api is a class in which we define a method getClient() that returns the API Interface class object
        // getUsersList() is a method in API Interface class, in this method we define our API sub url

        Api.getClient().getMenu(getIntent().getExtras().getString("topic_id")).enqueue(new Callback<ListData>() {
            @Override
            public void onResponse(Call<ListData> call, Response<ListData> response) {


                try{
                    if(response.body().getRetrun_code().equals("00")){
                        if(response.body().getContext_view().size()==0) {

                            alert(" ไม่เนื้อพบหาข้อมูลในหน้านี้ "+" กรุณาติดต่อผู้พัฒนาแอพเคชั่น");
                        }else{
                            setupUI(response.body().getContext_view(),response.body().getImage_rand());
                            page_mask.setVisibility(View.GONE);

                        }

                    }else{
                        alert(response.body().getReturn_result_e());
                    }
                }catch (Exception e){
                    alert(response.body().getReturn_result_e()+" "+e.getMessage());
                }

                //// Toast.makeText(MenuActivity.this, "OK "+list.getImage_rand().size(), Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ListData> call, Throwable t) {

                alert(t.getMessage());
                progressDialog.dismiss(); //dismiss progress dialog
            }
        });



    }


    public void alert(String message){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage(message);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        onBackPressed();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();
    }



    public void MaterialDialog(){


        new MaterialDialog.Builder(this)
                .title("เลือกขนาดตัวหนังสือ")
                .iconRes(R.drawable.ic_font_24dp)
                .items(R.array.preference_values)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        //Toast.makeText(getApplicationContext(), which + ": " + text + ", ID = " + view.getId(), Toast.LENGTH_SHORT).show();
                        if(which == 0){
                            //title.setTextSize(18);
                            context1.setTextSize(15);
                            context2.setTextSize(15);
                        }else if(which == 1){
                           // title.setTextSize(19);
                            context1.setTextSize(16);
                            context2.setTextSize(16);
                        }else if(which == 2){
                           // title.setTextSize(20);
                            context1.setTextSize(17);
                            context2.setTextSize(17);
                        }

                    }
                })
                .show();
    }


    public void setupUI(List<Context_view> ListData, List<gallery> image_rand) {

        images = new ArrayList<>();
        mainLayout = (RelativeLayout) findViewById(R.id.main);
        header = (TextView) findViewById(R.id.header);
        header_detail = (TextView) findViewById(R.id.header_detail);
        text_bar = (TextView) findViewById(R.id.text_bar);
        context1 = (TextView) findViewById(R.id.context1);
        context2 = (TextView) findViewById(R.id.context2);
        showimage = (ImageView) findViewById(R.id.showimage);
        showimage1 = (ImageView) findViewById(R.id.showimage1);
        image_context = (ImageView) findViewById(R.id.image_context);
        collectionImage = (TextView) findViewById(R.id.collectionImage);
        contract_name = (TextView) findViewById(R.id.contract_name);
        select_font = (ImageView) findViewById(R.id.select_font);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView navigationView1 = (NavigationView) findViewById(R.id.nav_view);
        navigationView1.setNavigationItemSelectedListener(this);

        View hView =  navigationView1.inflateHeaderView(R.layout.nav_header_main);
        ImageView imgvw = (ImageView)hView.findViewById(R.id.icon_profile);
        if(mPrefs.getString("flaug","").equals("F")){
            Picasso.with(this).load(mPrefs.getString("image","")).resize(100, 100).into(imgvw);
        }else{
            Picasso.with(this).load(new Contact().Base_url+mPrefs.getString("image","")).resize(100, 100).into(imgvw);
        }


        install_header(new Contact().Base_url+ListData.get(0).getPath_image());


        if (ListData.get(0).getImage_rand() == null || ListData.get(0).getImage_rand() == "") {

            contract_name.setVisibility(View.GONE);
            image_context.setVisibility(View.GONE);

        } else {
            if(ListData.get(0).getContract_name()== null || ListData.get(0).getContract_name() == "" ){
                contract_name.setText(contract_name.getText() + " ไม่พบการ Config");

            }else{
                contract_name.setText(contract_name.getText() + " " + ListData.get(0).getContract_name());

            }
            Picasso.with(this).load(new Contact().Base_url + ListData.get(0).getImage_rand()).placeholder(R.drawable.noimage).into(image_context);
        }

         if (image_rand.size() > 1){

            Picasso.with(this).load(new Contact().Base_url + image_rand.get(0).getPath_image()).resize(300, 200).placeholder(R.drawable.noimage).into(showimage1);
            Picasso.with(this).load(new Contact().Base_url + image_rand.get(1).getPath_image()).resize(300, 200).placeholder(R.drawable.noimage).into(showimage);
            collectionImage.setText(image_rand.size()+"+");
             images.addAll(image_rand);
        }else{
            layout_image.setVisibility(View.GONE);

        }

        header.setText(header.getText() + " " + new AppUtils().Now_time(ListData.get(0).getTopic_postdate()));
        header_detail.setText("ม่อนปิ่นเมือง 3ดีวิถีสุข");
        text_bar.setText(ListData.get(0).getTopic_name());



            fromHtml = HtmlCompat.fromHtml(this,ListData.get(0).getContent1(),
                    HtmlCompat.FROM_HTML_SEPARATOR_LINE_BREAK_LIST_ITEM,
                    this);
            context1.setMovementMethod(LinkMovementMethod.getInstance());
            context1.setText(fromHtml);




        fromHtml = HtmlCompat.fromHtml(this,ListData.get(0).getContent2(),
                HtmlCompat.FROM_HTML_SEPARATOR_LINE_BREAK_LIST_ITEM,
                this);
        context2.setMovementMethod(LinkMovementMethod.getInstance());
        context2.setText(fromHtml);


        install_youtube(ListData.get(0).getYoutube_key());


        showimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPicker(1);


            }
        });

        showimage1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPicker(0);


            }
        });

        collectionImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MenuActivity.this, collectionImage.class);
                i.putExtra("topic_id", getIntent().getExtras().getString("topic_id"));
                startActivity(i);
            }
        });

        select_font.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MaterialDialog();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.menu_action:
                //Toast.makeText(this, "OK", Toast.LENGTH_SHORT).show();
                if (drawer.isDrawerOpen(GravityCompat.END)) {
                    drawer.closeDrawer(GravityCompat.END);
                } else {
                    drawer.openDrawer(GravityCompat.END);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }



    protected void showPicker(int startPosition) {

        overlayView = new ImageOverlayView(this);
        cc = new ImageViewer.Builder<>(this, images)
                .setImageChangeListener(getImageChangeListener())
                .setFormatter(getCustomFormatter())
                .setOverlayView(overlayView)
                .setStartPosition(startPosition)
                .show();

    }

    public void CloseImageOver(Context x){
        Toast.makeText(x, "PK", Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(this, MenuActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP);
        startActivity(intent);
    }




    private ImageViewer.Formatter<gallery> getCustomFormatter() {
        return new ImageViewer.Formatter<gallery>() {
            @Override
            public String format(gallery gallery) {
                return  new Contact().Base_url+gallery.getPath_image();
            }
        };
    }

    private ImageViewer.OnImageChangeListener getImageChangeListener() {
        return new ImageViewer.OnImageChangeListener() {
            @Override
            public void onImageChange(int position) {
               // gallery image = images.get(position);
              // overlayView.setShareText(image.getUrl());
                overlayView.setDescription(images.get(position).getPic_detail());
                overlayView.setPage_number("หน้าที่ "+(position+1)+"/"+images.size());
            }
        };
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public Drawable getDrawable(String source, Attributes attributes) {
        LevelListDrawable d = new LevelListDrawable();
        Drawable empty = getResources().getDrawable(R.drawable.ic_add_location_black_24dp);
        d.addLevel(0, 0, empty);
        d.setBounds(0, 0, empty.getIntrinsicWidth(), empty.getIntrinsicHeight());

        new MenuActivity.LoadImage().execute(source, d);

        return d;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();
        String text = "";
        if (id == R.id.menu1) {
            startActivity(new Intent(getBaseContext(), Menu1Activity.class).putExtra("activity","menu1"));
        } else if (id == R.id.menu2) {
            startActivity(new Intent(getBaseContext(), Menu2Activity.class).putExtra("activity","menu2"));
        } else if (id == R.id.menu3) {
            startActivity(new Intent(getBaseContext(), Menu3Activity.class).putExtra("activity","menu3"));
        } else if (id == R.id.menu4) {
            startActivity(new Intent(getBaseContext(), Menu4Activity.class).putExtra("activity","menu4"));

        } else if (id == R.id.nav_share) {
            startActivity(new Intent(getBaseContext(), ContactActivity.class));
        } else if (id == R.id.nav_send) {
            startActivity(new Intent(getBaseContext(), AboutActivity.class));
        }else if(id == R.id.nav_timeline){
            startActivity(new Intent(getBaseContext(), TimelineActivity.class));
        }else if(id == R.id.nav_live){
            startActivity(new Intent(getBaseContext(), YoutubeliveActivity.class));
        }
        //Toast.makeText(this, "You have chosen " + text, Toast.LENGTH_LONG).show();
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        //rawer.closeDrawer(GravityCompat);
        drawer.closeDrawer(GravityCompat.END);
        return false;
    }



    class LoadImage extends AsyncTask<Object, Void, Bitmap> {

        private LevelListDrawable mDrawable;

        @Override
        protected Bitmap doInBackground(Object... params) {
            String source = (String) params[0];
            mDrawable = (LevelListDrawable) params[1];
            Log.d(TAG, "doInBackground " + source);
            try {
                InputStream is = new URL(source).openStream();
                return BitmapFactory.decodeStream(is);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            Log.d(TAG, "onPostExecute drawable " + mDrawable);
            Log.d(TAG, "onPostExecute bitmap " + bitmap);
            if (bitmap != null) {
                BitmapDrawable d = new BitmapDrawable(bitmap);
                mDrawable.addLevel(1, 1, d);
                mDrawable.setBounds(0, 0, bitmap.getWidth(), bitmap.getHeight());
                mDrawable.setLevel(1);
                // i don't know yet a better way to refresh TextView
                // mTv.invalidate() doesn't work as expected
                CharSequence t = context2.getText();
                context2.setText(t);
                CharSequence t1 = context1.getText();
                context1.setText(t1);
            }
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        return true;
    }


}
