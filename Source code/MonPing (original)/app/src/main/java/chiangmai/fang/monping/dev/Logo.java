package chiangmai.fang.monping.dev;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.facebook.AccessToken;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Timer;
import java.util.TimerTask;

import chiangmai.fang.monping.dev.activity.FacebookLoginActivity;
import chiangmai.fang.monping.dev.activity.Mainactivity;
import chiangmai.fang.monping.dev.activity.ProfileActivity;
import chiangmai.fang.monping.dev.utils.BaseCommon;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class Logo extends AppCompatActivity  {

    Handler handler;
    Runnable runnable;
    long delay_time;
    long time = 3000L;
     SharedPreferences mPrefs = null;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_logo);



        mPrefs = this.getSharedPreferences("lend_system", Context.MODE_PRIVATE);

        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {

                if (AccessToken.getCurrentAccessToken() != null || !mPrefs.getString("username","").equals("")) {
                    Intent loginIntent = new Intent(Logo.this, Mainactivity.class);
                    startActivity(loginIntent);
                    finish();
                }else {
                    Intent intent = new Intent(Logo.this,FacebookLoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

                    startActivity(intent);
                    finish();
                }

            }
        };


    }
    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        delay_time = time;
        handler.postDelayed(runnable,delay_time);
        time = System.currentTimeMillis();
    }

    @Override
    protected void onStop() {
        super.onStop();
        handler.removeCallbacks(runnable);
        time = delay_time - (System.currentTimeMillis());
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
