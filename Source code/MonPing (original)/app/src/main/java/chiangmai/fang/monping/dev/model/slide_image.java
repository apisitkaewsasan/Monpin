package chiangmai.fang.monping.dev.model;

public class slide_image {
    String topic_id;
    String topic_type;
    String path_image;
    String dtl_name;


    public slide_image(String topic_id, String topic_type, String path_image, String dtl_name) {
        this.topic_id = topic_id;
        this.topic_type = topic_type;
        this.path_image = path_image;
        this.dtl_name = dtl_name;
    }

    public String getTopic_id() {
        return topic_id;
    }

    public void setTopic_id(String topic_id) {
        this.topic_id = topic_id;
    }

    public String getTopic_type() {
        return topic_type;
    }

    public void setTopic_type(String topic_type) {
        this.topic_type = topic_type;
    }

    public String getPath_image() {
        return path_image;
    }

    public void setPath_image(String path_image) {
        this.path_image = path_image;
    }

    public String getDtl_name() {
        return dtl_name;
    }

    public void setDtl_name(String dtl_name) {
        this.dtl_name = dtl_name;
    }
}
