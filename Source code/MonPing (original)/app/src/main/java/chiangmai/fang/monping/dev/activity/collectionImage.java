package chiangmai.fang.monping.dev.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.stfalcon.frescoimageviewer.ImageViewer;

import java.util.ArrayList;
import java.util.List;

import chiangmai.fang.monping.dev.R;
import chiangmai.fang.monping.dev.model.Context_view;
import chiangmai.fang.monping.dev.model.CustomImage;
import chiangmai.fang.monping.dev.model.Demo;
import chiangmai.fang.monping.dev.model.ListData;
import chiangmai.fang.monping.dev.model.gallery;
import chiangmai.fang.monping.dev.service.Api;
import chiangmai.fang.monping.dev.utils.AppUtils;
import chiangmai.fang.monping.dev.utils.Contact;
import chiangmai.fang.monping.dev.utils.ImageOverlayView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class collectionImage extends AppCompatActivity {

    protected String[] posters, descriptions,image;
    private TextView title,date_post,count_view,ref_name;

    private static final int[] ids = new int[]{
            R.id.firstImage, R.id.secondImage,
            R.id.thirdImage, R.id.fourthImage,
            R.id.fifthImage, R.id.sixthImage,
            R.id.seventhImage, R.id.eighthImage,
            R.id.ninethImage,R.id.seventh1Image,
            R.id.eighth1Image,R.id.nineth1Image
    };
    public List<gallery> images;
    public List<Context_view> context;
    private ImageOverlayView overlayView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collection_image);
        images = new ArrayList<>();
        context = new ArrayList<>();
        getData();

    }


    private void getData() {
        // display a progress dialog
        final ProgressDialog progressDialog = new ProgressDialog(collectionImage.this);
        progressDialog.setCancelable(false); // set cancelable to false
        progressDialog.setMessage("Please Wait"); // set message
        progressDialog.show(); // show progress dialog

        Api.getClient().getMenu(getIntent().getExtras().getString("topic_id")).enqueue(new Callback<ListData>() {
            @Override
            public void onResponse(Call<ListData> call, Response<ListData> response) {
                images.addAll(response.body().getImage_rand());
                context.addAll(response.body().getContext_view());
                setupUI(response.body().getContext_view(),response.body().getImage_rand());
                //// Toast.makeText(MenuActivity.this, "OK "+list.getImage_rand().size(), Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ListData> call, Throwable t) {
                Toast.makeText(collectionImage.this, t.getMessage(), Toast.LENGTH_LONG).show();
                progressDialog.dismiss(); //dismiss progress dialog
            }
        });


    }

    public void setupUI(List<Context_view> ListData, List<gallery> image_rand){
        title = (TextView) findViewById(R.id.title);
        count_view = (TextView)findViewById(R.id.count_view);
        date_post = (TextView)findViewById(R.id.date_post);
        ref_name = (TextView)findViewById(R.id.ref_name);

        title.setText(context.get(0).getTopic_name());
        date_post.setText(date_post.getText()+" "+new AppUtils().Now_time(context.get(0).getTopic_postdate()));
        count_view.setText(count_view.getText()+" "+context.get(0).getTopic_view_count());
        if(context.get(0).getContract_name()==null){
            ref_name.setText(ref_name.getText()+" ไม่พบรายการ Config");
        }else{
            ref_name.setText(ref_name.getText()+" "+context.get(0).getContract_name());
        }
        for (int i = 0; i < ids.length; i++) {
            SimpleDraweeView drawee = (SimpleDraweeView) findViewById(ids[i]);
            initDrawee(drawee, i);
        }
    }



    private void initDrawee(SimpleDraweeView drawee, final int startPosition) {

        if(startPosition<images.size()){
            drawee.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    showPicker(startPosition);

                }
            });
            drawee.setImageURI(new Contact().Base_url+images.get(startPosition).getPath_image());
        }else{
            drawee.setImageURI(new Contact().No_image);

        }


    }


    protected void showPicker(int startPosition) {
        overlayView = new ImageOverlayView(this);
        new ImageViewer.Builder<>(this, images)
                .setImageChangeListener(getImageChangeListener())
                .setFormatter(getCustomFormatter())
                .setOverlayView(overlayView)
                .setStartPosition(startPosition)
                .show();

    }

    private ImageViewer.Formatter<gallery> getCustomFormatter() {
        return new ImageViewer.Formatter<gallery>() {
            @Override
            public String format(gallery gallery) {
                return  new Contact().Base_url+gallery.getPath_image();
            }
        };
    }

    private ImageViewer.OnImageChangeListener getImageChangeListener() {
        return new ImageViewer.OnImageChangeListener() {
            @Override
            public void onImageChange(int position) {
                gallery image = images.get(position);
               // overlayView.setShareText(image.getUrl());
                overlayView.setDescription(images.get(position).getPic_detail());
                overlayView.setPage_number("หน้าที่ "+(position+1)+"/"+images.size());
            }
        };
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}

