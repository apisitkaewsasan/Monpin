package chiangmai.fang.monping.dev.model;

public class About {
    String topic_id;
    String content1;
    String content2;
    String path_image;

    public About(String topic_id, String content1, String content2, String path_image) {
        this.topic_id = topic_id;
        this.content1 = content1;
        this.content2 = content2;
        this.path_image = path_image;
    }

    public String getTopic_id() {
        return topic_id;
    }

    public void setTopic_id(String topic_id) {
        this.topic_id = topic_id;
    }

    public String getContent1() {
        return content1;
    }

    public void setContent1(String content1) {
        this.content1 = content1;
    }

    public String getContent2() {
        return content2;
    }

    public void setContent2(String content2) {
        this.content2 = content2;
    }

    public String getPath_image() {
        return path_image;
    }

    public void setPath_image(String path_image) {
        this.path_image = path_image;
    }
}
