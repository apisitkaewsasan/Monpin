package chiangmai.fang.monping.dev.model;

import java.util.List;

public class LoginDTO {
    String retrun_code;
    String return_result;
    List<login> retult;

    public LoginDTO(String retrun_code, String return_result, List<login> retult) {
        this.retrun_code = retrun_code;
        this.return_result = return_result;
        this.retult = retult;
    }

    public String getRetrun_code() {
        return retrun_code;
    }

    public void setRetrun_code(String retrun_code) {
        this.retrun_code = retrun_code;
    }

    public String getReturn_result() {
        return return_result;
    }

    public void setReturn_result(String return_result) {
        this.return_result = return_result;
    }

    public List<login> getRetult() {
        return retult;
    }

    public void setRetult(List<login> retult) {
        this.retult = retult;
    }
}
