package chiangmai.fang.monping.dev.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import chiangmai.fang.monping.dev.R;
import chiangmai.fang.monping.dev.adepter.FragmentFAdapter;
import chiangmai.fang.monping.dev.adepter.Menu4Adepter;
import chiangmai.fang.monping.dev.adepter.TimeLineAdapter;
import chiangmai.fang.monping.dev.model.Context_view;
import chiangmai.fang.monping.dev.model.ListData;
import chiangmai.fang.monping.dev.model.TimeLineModel;
import chiangmai.fang.monping.dev.model.location;
import chiangmai.fang.monping.dev.service.Api;
import chiangmai.fang.monping.dev.utils.Contact;
import chiangmai.fang.monping.dev.utils.EndlessRecyclerViewScrollListener;
import chiangmai.fang.monping.dev.utils.RecycleViewClickListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class TimelineActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private RecyclerView mRecyclerView;
    private TimeLineAdapter mTimeLineAdapter;
    int Count_row=2;
    private EndlessRecyclerViewScrollListener scrollListener;
    private List<location> mDataList = new ArrayList<>();
    Toolbar toolbar;
    SharedPreferences mPrefs = null;
    private DrawerLayout drawer;
    FrameLayout information;
    TextView alert_frame;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timeline);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_keyboard_arrow_left_black_24dp);
        toolbar.setTitle("ประวัติการสืบค้น");
        setSupportActionBar(toolbar);
        mPrefs = this.getSharedPreferences("lend_system", Context.MODE_PRIVATE);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });


        information = (FrameLayout)findViewById(R.id.information);

        setRecyclerView();
        mPrefs = getSharedPreferences("lend_system", Context.MODE_PRIVATE);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView navigationView1 = (NavigationView) findViewById(R.id.nav_view);
        navigationView1.setNavigationItemSelectedListener(this);
        View hView =  navigationView1.inflateHeaderView(R.layout.nav_header_main);
        ImageView imgvw = (ImageView)hView.findViewById(R.id.icon_profile);
        if(mPrefs.getString("flaug","").equals("F")){
            Picasso.with(this).load(mPrefs.getString("image","")).into(imgvw);
        }else{
            Picasso.with(this).load(new Contact().Base_url+mPrefs.getString("image","")).into(imgvw);
        }
        loadData(2);

    }


    public void setRecyclerView(){
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(linearLayoutManager); // set LayoutManager to RecyclerView

        mTimeLineAdapter = new TimeLineAdapter(getApplicationContext(),mDataList, new RecycleViewClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                //startActivity(new Intent(getContext().getApplicationContext(), MenuActivity.class).putExtra("activity","menu2"));

                moveStaivity(mDataList.get(position).getTopic_id(),mDataList.get(position).getTopic_type());
            }
        });
        mRecyclerView.setAdapter(mTimeLineAdapter);


        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {

                if(mDataList.size()<Count_row){
                    loadData(mDataList.size()+2);
                }
            }


        };
        mRecyclerView.addOnScrollListener(scrollListener);
    }



    public String convert_menu(String index){
        String out= "";
        if(index.equals("N1") || index.equals("N2") ||index.equals("N3")){
            out= "menu2";
        }else if(index.equals("N4") || index.equals("N5") ){
            out = "menu3";
        }else if(index.equals("N6")){
            out = "menu4";
        }
        return out;
    }


    public void moveStaivity(String value,String index){

        if(index.equals("V") || index == "V"){
            startActivity(new Intent(getBaseContext(), VideoActivity.class).putExtra("Topic_id",value));
            //finish();
        }else if(index.equals("E") || index == "E"){
            startActivity(new Intent(getBaseContext(), EbookActivity.class).putExtra("Topic_id",value).putExtra("activity",convert_menu(index)));
            //finish();
        }else if(index.equals("N7") || index == "N7"){
            startActivity(new Intent(getBaseContext(), SubMenu4Activity.class).putExtra("topic_id",value).putExtra("activity",convert_menu(index)));
           // finish();
        } else{
            startActivity(new Intent(getBaseContext(), MenuActivity.class).putExtra("topic_id",value).putExtra("activity",convert_menu(index)));
           //finish();
        }


    }

    private void loadData(int index) {


        Api.getClient().getLocation(mPrefs.getString("username",""),index).enqueue(new Callback<ListData>() {
            @Override
            public void onResponse(Call<ListData> call, Response<ListData> response) {
                try{
                    if(response.body().getRetrun_code().equals("00")){
                        if(response.body().getLocation().size()==0) {

                            alert_frame = (TextView)findViewById(R.id.alert_frame);
                            information.setVisibility(View.VISIBLE);
                            alert_frame.setText("ไม่พบรายการค้นหา");
                        }else{
                            information.setVisibility(View.GONE);
                            mDataList.clear();
                            mDataList.addAll(response.body().getLocation());
                            Count_row = response.body().getCount_row();
                            mTimeLineAdapter.notifyDataSetChanged();

                        }

                    }else{
                        alert(response.body().getReturn_result_e());
                    }
                }catch (Exception e){
                    alert(response.body().getReturn_result_e()+" "+e.getMessage());
                }

            }

            @Override
            public void onFailure(Call<ListData> call, Throwable t) {
                alert(t.getMessage());
            }
        });

    }

    public void alert(String message){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage(message);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        onBackPressed();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.toolbar_menu2, menu);
        return true;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_action:
                //Toast.makeText(this, "OK", Toast.LENGTH_SHORT).show();
                if (drawer.isDrawerOpen(GravityCompat.END)) {
                    drawer.closeDrawer(GravityCompat.END);
                } else {
                    drawer.openDrawer(GravityCompat.END);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();
        String text = "";
        if (id == R.id.menu1) {
            startActivity(new Intent(getBaseContext(), Menu1Activity.class).putExtra("activity","menu1"));
            finish();
        } else if (id == R.id.menu2) {
            startActivity(new Intent(getBaseContext(), Menu2Activity.class).putExtra("activity","menu2"));
            finish();
        } else if (id == R.id.menu3) {
            startActivity(new Intent(getBaseContext(), Menu3Activity.class).putExtra("activity","menu3"));
            finish();
        } else if (id == R.id.menu4) {
            startActivity(new Intent(getBaseContext(), Menu4Activity.class).putExtra("activity","menu4"));
            finish();
        } else if (id == R.id.nav_share) {
            startActivity(new Intent(getBaseContext(), ContactActivity.class));
            finish();
        } else if (id == R.id.nav_send) {
            startActivity(new Intent(getBaseContext(), AboutActivity.class));
            finish();
        }else if(id == R.id.nav_timeline){
            startActivity(new Intent(getBaseContext(), TimelineActivity.class));
            finish();
        }else if(id == R.id.nav_live){
            startActivity(new Intent(getBaseContext(), YoutubeliveActivity.class));
            finish();
        }
        //Toast.makeText(this, "You have chosen " + text, Toast.LENGTH_LONG).show();
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        //rawer.closeDrawer(GravityCompat);
        drawer.closeDrawer(GravityCompat.END);
        return false;
    }

}
