package chiangmai.fang.monping.dev.activity;

import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import chiangmai.fang.monping.dev.R;
import chiangmai.fang.monping.dev.model.ListData;
import chiangmai.fang.monping.dev.model.contact;
import chiangmai.fang.monping.dev.service.Api;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ContactActivity extends AppCompatActivity {

    ConstraintLayout layout1,layout2,layout3,layout4;
    ProgressBar progressBar;
    TextView contact_name,Contact_address,Contact_tel,Contact_email,Error;
    private FrameLayout page_mask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_chevron_left_black_24dp);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Error = (TextView)findViewById(R.id.Error);
        page_mask = (FrameLayout)findViewById(R.id.page_mask);
        getData();

    }


    private void getData() {
        // display a progress dialog
        final ProgressDialog progressDialog = new ProgressDialog(ContactActivity.this);
        progressDialog.setCancelable(false); // set cancelable to false
        progressDialog.setMessage("Please Wait"); // set message
        progressDialog.show(); // show progress dialog

        // Api is a class in which we define a method getClient() that returns the API Interface class object
        // getUsersList() is a method in API Interface class, in this method we define our API sub url

        Api.getClient().getContactView().enqueue(new Callback<ListData>() {
            @Override
            public void onResponse(Call<ListData> call, Response<ListData> response) {
                try{
                    setupUI(response.body().getView_contact());
                    page_mask.setVisibility(View.GONE);
                }catch (Exception e){
                    page_mask.setVisibility(View.VISIBLE);
                    Error.setText("Error -> "+e.getMessage()+"\n ไม่เนื้อพบหาข้อมูลในหน้านี้ "+" กรุณาติดต่อผู้พัฒนาแอพเคชั่น");
                }

                //// Toast.makeText(MenuActivity.this, "OK "+list.getImage_rand().size(), Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ListData> call, Throwable t) {
                Toast.makeText(ContactActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                progressDialog.dismiss(); //dismiss progress dialog
            }
        });

    }

    public  void setupUI(List<contact> list){
        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        layout1 = (ConstraintLayout)findViewById(R.id.layout1);
        layout2 = (ConstraintLayout)findViewById(R.id.layout2);
        layout3 = (ConstraintLayout)findViewById(R.id.layout3);
        layout4 = (ConstraintLayout)findViewById(R.id.layout4);

        contact_name = (TextView)findViewById(R.id.contact_name);
        Contact_address = (TextView)findViewById(R.id.Contact_address);
        Contact_tel = (TextView)findViewById(R.id.Contact_tel);
        Contact_email = (TextView)findViewById(R.id.Contact_email);

        contact_name.setText(contact_name.getText()+" "+list.get(0).getContract_name());
        Contact_address.setText(Contact_address.getText()+" "+list.get(0).getContract_address());
        Contact_tel.setText(Contact_tel.getText()+" "+list.get(0).getTel());
        Contact_email.setText(Contact_email.getText()+" "+list.get(0).getEmail());

        layout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = list.get(0).getWeb();
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });


        layout2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Create a Uri from an intent string. Use the result to create an Intent.
                Uri gmmIntentUri = Uri.parse("google.streetview:cbll="+list.get(0).getGps_lat()+","+list.get(0).getGps_long()+"");

// Create an Intent from gmmIntentUri. Set the action to ACTION_VIEW
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
// Make the Intent explicit by setting the Google Maps package
                mapIntent.setPackage("com.google.android.apps.maps");

// Attempt to start an activity that can handle the Intent
                startActivity(mapIntent);
            }
        });

        layout3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+list.get(0).getTel()));
                startActivity(intent);
            }
        });

        layout4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/html");
                intent.putExtra(Intent.EXTRA_EMAIL, list.get(0).getEmail());
                intent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
                intent.putExtra(Intent.EXTRA_TEXT, "I'm email body.");

                startActivity(Intent.createChooser(intent, "Send Email"));
            }
        });

        String html = "<html><body>"+list.get(0).getLink() +"</body></html>";
        String mime = "text/html";
        String encoding = "utf-8";

        WebView myWebView = (WebView)this.findViewById(R.id.webview);
        myWebView.getSettings().setJavaScriptEnabled(true);
        myWebView.loadDataWithBaseURL(null, html, mime, encoding, null);

        myWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                // TODO show you progress image
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                // TODO hide your progress image
                progressBar.setVisibility(View.GONE);
                super.onPageFinished(view, url);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
