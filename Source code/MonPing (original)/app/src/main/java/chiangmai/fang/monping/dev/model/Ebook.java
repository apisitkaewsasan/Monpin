package chiangmai.fang.monping.dev.model;

public class Ebook {
    private int topic_id;
    private String content1;
    private String content2;
    private String path_image;
    private String topic_name;

    public Ebook(int topic_id, String content1, String content2, String path_image, String topic_name) {
        this.topic_id = topic_id;
        this.content1 = content1;
        this.content2 = content2;
        this.path_image = path_image;
        this.topic_name = topic_name;
    }

    public int getTopic_id() {
        return topic_id;
    }

    public void setTopic_id(int topic_id) {
        this.topic_id = topic_id;
    }

    public String getContent1() {
        return content1;
    }

    public void setContent1(String content1) {
        this.content1 = content1;
    }

    public String getContent2() {
        return content2;
    }

    public void setContent2(String content2) {
        this.content2 = content2;
    }

    public String getPath_image() {
        return path_image;
    }

    public void setPath_image(String path_image) {
        this.path_image = path_image;
    }

    public String getTopic_name() {
        return topic_name;
    }

    public void setTopic_name(String topic_name) {
        this.topic_name = topic_name;
    }
}
