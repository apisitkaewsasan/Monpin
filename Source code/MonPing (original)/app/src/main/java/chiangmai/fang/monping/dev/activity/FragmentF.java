package chiangmai.fang.monping.dev.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import chiangmai.fang.monping.dev.R;
import chiangmai.fang.monping.dev.adepter.FragmentAAdapter;
import chiangmai.fang.monping.dev.adepter.FragmentEAdapter;
import chiangmai.fang.monping.dev.adepter.FragmentFAdapter;
import chiangmai.fang.monping.dev.model.Context_view;
import chiangmai.fang.monping.dev.model.ListData;
import chiangmai.fang.monping.dev.service.Api;
import chiangmai.fang.monping.dev.utils.Contact;
import chiangmai.fang.monping.dev.utils.EndlessRecyclerViewScrollListener;
import chiangmai.fang.monping.dev.utils.RecycleViewClickListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentF  extends Fragment implements NavigationView.OnNavigationItemSelectedListener {

    Toolbar toolbar;
    private RecyclerView recyclerView;
    private ShimmerFrameLayout mShimmerViewContainer;
    private DrawerLayout drawer;
    SharedPreferences mPrefs = null;
    List<Context_view> e;
    FragmentFAdapter customFdapter;
    int Count_row=2;
    private EndlessRecyclerViewScrollListener scrollListener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        e = new ArrayList<>();
        loadData(Count_row);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_f, container, false);
        //imageView5 = (ImageView) rootView.findViewById(R.id.imageView5);
        mShimmerViewContainer = rootView.findViewById(R.id.shimmer_view_container);
        // meesage_error = (TextView) rootView.findViewById(R.id.meesage_error);
        toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_keyboard_arrow_left_black_24dp);

        toolbar.setTitle("ผลิตภัณฑ์ชุมชน");
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
                getActivity().finish();
            }
        });
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        setRecyclerView(rootView); // set LayoutManager to RecyclerView
        mPrefs = getContext().getSharedPreferences("lend_system", Context.MODE_PRIVATE);
        drawer = (DrawerLayout) rootView.findViewById(R.id.drawer_layout);
        drawer = (DrawerLayout) rootView.findViewById(R.id.drawer_layout);
        NavigationView navigationView1 = (NavigationView) rootView.findViewById(R.id.nav_view);
        navigationView1.setNavigationItemSelectedListener(this);
        View hView =  navigationView1.inflateHeaderView(R.layout.nav_header_main);
        ImageView imgvw = (ImageView)hView.findViewById(R.id.icon_profile);
        if(mPrefs.getString("flaug","").equals("F")){
            Picasso.with(getContext()).load(mPrefs.getString("image","")).into(imgvw);
        }else{
            Picasso.with(getContext()).load(new Contact().Base_url+mPrefs.getString("image","")).into(imgvw);
        }



        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        menu.clear();
        inflater.inflate(R.menu.toolbar_menu2, menu);


    }

    public  void statactivity(String x){
        // Toast.makeText(getContext() , ""+x, Toast.LENGTH_SHORT).show();
        startActivity(new Intent(getContext().getApplicationContext(), MenuActivity.class).putExtra("activity","menu3").putExtra("topic_id",x));

    }

    public void setRecyclerView(View view){
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext().getApplicationContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(linearLayoutManager); // set LayoutManager to RecyclerView

        customFdapter = new FragmentFAdapter(getContext(),e, new RecycleViewClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                //startActivity(new Intent(getContext().getApplicationContext(), MenuActivity.class).putExtra("activity","menu2"));
                statactivity(e.get(position).getTopic_id()+"");
            }
        });
        recyclerView.setAdapter(customFdapter);


        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {

                if(e.size()<Count_row){
                    loadData(e.size()+2);
                }
            }


        };
        recyclerView.addOnScrollListener(scrollListener);
    }


    private void loadData(int index) {

        Api.getClient().getMenuMain("N5",index).enqueue(new Callback<ListData>() {
            @Override
            public void onResponse(Call<ListData> call, Response<ListData> response) {
                try{
                    if(response.body().getRetrun_code().equals("00")){
                        if(response.body().getContext_view().size()==0) {

                            alert("ไม่พบรายการค้นหา");
                        }else{
                            e.clear();
                            e.addAll(response.body().getContext_view());
                            Count_row = response.body().getCount_row();
                            mShimmerViewContainer.stopShimmerAnimation();
                            mShimmerViewContainer.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.VISIBLE);
                            customFdapter.notifyDataSetChanged();

                        }

                    }else{
                        alert(response.body().getReturn_result_e());
                    }
                }catch (Exception e){
                    alert(response.body().getReturn_result_e()+" "+e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ListData> call, Throwable t) {
                mShimmerViewContainer.stopShimmerAnimation();
                mShimmerViewContainer.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
            }
        });

    }

    public void alert(String message){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
        builder1.setMessage(message);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        getActivity().onBackPressed();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        super.onPause();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_action:
                //Toast.makeText(this, "OK", Toast.LENGTH_SHORT).show();
                if (drawer.isDrawerOpen(GravityCompat.END)) {
                    drawer.closeDrawer(GravityCompat.END);
                } else {
                    drawer.openDrawer(GravityCompat.END);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        String text = "";
        if (id == R.id.menu1) {
            startActivity(new Intent(getContext(), Menu1Activity.class).putExtra("activity","menu1"));
            getActivity().finish();
        } else if (id == R.id.menu2) {
            startActivity(new Intent(getContext(), Menu2Activity.class).putExtra("activity","menu2"));
            getActivity().finish();
        } else if (id == R.id.menu3) {
            startActivity(new Intent(getContext(), Menu3Activity.class).putExtra("activity","menu3"));
            getActivity().finish();
        } else if (id == R.id.menu4) {
            startActivity(new Intent(getContext(), Menu4Activity.class).putExtra("activity","menu4"));
            getActivity().finish();

        } else if (id == R.id.nav_share) {
            startActivity(new Intent(getContext(), ContactActivity.class));
            getActivity().finish();
        } else if (id == R.id.nav_send) {
            startActivity(new Intent(getContext(), AboutActivity.class));
            getActivity().finish();
        }else if(id == R.id.nav_timeline){
            startActivity(new Intent(getContext(), TimelineActivity.class));
            getActivity().finish();
        }else if(id == R.id.nav_live){
            startActivity(new Intent(getContext(), YoutubeliveActivity.class));
            getActivity().finish();
        }
        //Toast.makeText(this, "You have chosen " + text, Toast.LENGTH_LONG).show();

        //rawer.closeDrawer(GravityCompat);
        drawer.closeDrawer(GravityCompat.END);
        return false;
    }
}
