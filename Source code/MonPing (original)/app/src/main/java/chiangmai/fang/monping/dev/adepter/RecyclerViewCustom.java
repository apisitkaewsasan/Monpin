package chiangmai.fang.monping.dev.adepter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import chiangmai.fang.monping.dev.R;
import chiangmai.fang.monping.dev.model.Ebook;
import chiangmai.fang.monping.dev.model.Player;
import chiangmai.fang.monping.dev.utils.Contact;
import chiangmai.fang.monping.dev.utils.RecycleViewClickListener;

public class RecyclerViewCustom extends RecyclerView.Adapter<RecyclerViewCustom.ViewHolder>  {

    private List<Ebook> mPlayers;
    private Context mContext;
    RecycleViewClickListener listener;

    public RecyclerViewCustom(Context context, List<Ebook> dataset,RecycleViewClickListener listener) {
        mPlayers = dataset;
        mContext = context;
        this.listener = listener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView showItem;

        public ViewHolder(View view) {
            super(view);
            showItem = (ImageView) view.findViewById(R.id.showItem);

        }
    }


    public ViewHolder onCreateViewHolder( ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.recycler_view_row, viewGroup, false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Ebook ebook = mPlayers.get(i);


        Picasso.with(mContext).load(new Contact().Base_url+ebook.getPath_image()).resize(150, 180).into(viewHolder.showItem);

        viewHolder.showItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Toast.makeText(mContext.getApplicationContext(), "item : "+viewHolder.getPosition(), Toast.LENGTH_SHORT).show();
                listener.onItemClick(view, viewHolder.getPosition());
            }
        });
    }





    @Override
    public int getItemCount() {
        return mPlayers.size();
    }
}
