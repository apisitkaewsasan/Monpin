package chiangmai.fang.monping.dev.adepter;

import android.Manifest;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import chiangmai.fang.monping.dev.R;
import chiangmai.fang.monping.dev.model.Context_view;
import chiangmai.fang.monping.dev.model.gps;
import chiangmai.fang.monping.dev.service.DistanceGPS;
import chiangmai.fang.monping.dev.utils.AppUtils;
import chiangmai.fang.monping.dev.utils.Contact;
import chiangmai.fang.monping.dev.utils.RecycleViewClickListener;
import me.grantland.widget.AutofitHelper;

import static java.lang.Math.acos;
import static java.lang.Math.cos;
import static java.lang.Math.sin;

public class FragmentBAdapter extends RecyclerView.Adapter<FragmentBAdapter.ViewHolder> {

    private List<Context_view> item;
    private Context context;
    RecycleViewClickListener listener;
    private   double latitude;
    private double longitude ;
    boolean isLoading = false, isMoreDataAvailable = true;

    public FragmentBAdapter(Context context,double latitude,double longitude, List<Context_view> item, RecycleViewClickListener listener) {
        this.context = context;
        this.item = item;
        this.listener = listener;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView name, detail, topic_view_count, gps;
        ImageView image;

        public ViewHolder(View view) {
            super(view);
            name = (TextView) itemView.findViewById(R.id.name);
            gps = (TextView) itemView.findViewById(R.id.gps);
            topic_view_count = (TextView) itemView.findViewById(R.id.topic_view_count);
            detail = (TextView) itemView.findViewById(R.id.detail);
            image = (ImageView) itemView.findViewById(R.id.image);
        }
    }


    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context)
                .inflate(R.layout.fragmentbadapter, viewGroup, false);


        ViewHolder viewHolder = new ViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(view, viewHolder.getPosition());
            }
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {




        viewHolder.name.setText(item.get(i).getTopic_name());
        viewHolder.detail.setText("     "+ AppUtils.removeHTML(item.get(i).getContent1()));
        if(item.get(i).getGps_lat()==null || this.latitude == 0){
            viewHolder.gps.setText("ไม่พบระยะทาง");

        }else{
            viewHolder.gps.setText(""+new DistanceGPS().run(new gps(latitude,longitude), new gps(Double.parseDouble(item.get(i).getGps_lat()),Double.parseDouble(item.get(i).getGps_long()))));

        }
        viewHolder.topic_view_count.setText(item.get(i).getTopic_view_count());
        Picasso.with(context).load(new Contact().Base_url+item.get(i).getPath_image()).placeholder(R.drawable.noimage).resize(300, 200).into(viewHolder.image);

        // viewHolder.image.setImageResource(personImages.get(position));


    }






    @Override
    public int getItemCount() {
        return item.size();
    }



}

