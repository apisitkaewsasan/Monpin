package chiangmai.fang.monping.dev.service;


import java.util.concurrent.TimeUnit;

import chiangmai.fang.monping.dev.utils.Contact;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Api {



    public static APIService getClient() {

        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .build();

        // change your base URL
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(new Contact().Base_url+"index.php/") //Set the Root URL
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build(); //Finally building the adapter

        //Creating object for our interface
        APIService api = retrofit.create(APIService.class);
        return api; // return the APIInterface object
    }


    public static APIService getClientDemo() {

        // change your base URL
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.androidhive.info/json/shimmer/") //Set the Root URL
                .addConverterFactory(GsonConverterFactory.create())
                .build(); //Finally building the adapter

        //Creating object for our interface
        APIService api = retrofit.create(APIService.class);
        return api; // return the APIInterface object
    }
}
