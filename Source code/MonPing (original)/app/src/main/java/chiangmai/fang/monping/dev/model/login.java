package chiangmai.fang.monping.dev.model;

public class login {
    String id;
    String username;
    String fullname;
    String password;
    String tel;
    String path_image;

    public login(String id, String username, String fullname, String password, String tel, String path_image) {
        this.id = id;
        this.username = username;
        this.fullname = fullname;
        this.password = password;
        this.tel = tel;
        this.path_image = path_image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getPath_image() {
        return path_image;
    }

    public void setPath_image(String path_image) {
        this.path_image = path_image;
    }
}
