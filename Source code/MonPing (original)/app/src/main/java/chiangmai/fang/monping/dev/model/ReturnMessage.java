package chiangmai.fang.monping.dev.model;

public class ReturnMessage {
    String retrun_code;
    String return_result;

    public ReturnMessage(String retrun_code, String return_result) {
        this.retrun_code = retrun_code;
        this.return_result = return_result;
    }

    public String getRetrun_code() {
        return retrun_code;
    }

    public void setRetrun_code(String retrun_code) {
        this.retrun_code = retrun_code;
    }

    public String getReturn_result() {
        return return_result;
    }

    public void setReturn_result(String return_result) {
        this.return_result = return_result;
    }
}
