package chiangmai.fang.monping.dev.model;

public class account {
    String  username;
    String fullname;
    String password;
    String tel;

    public account(String username, String fullname, String password, String tel) {
        this.username = username;
        this.fullname = fullname;
        this.password = password;
        this.tel = tel;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }
}
