package chiangmai.fang.monping.dev.service;

import java.util.ArrayList;
import java.util.List;

import chiangmai.fang.monping.dev.model.gps;

public class DistanceGPS {
    private static final String DISTANCE_KM = " K.";
    private static final String DISTANCE_METER = " M.";
    private static final String TIME_TITLE = "ใช้เวลาในการเดิน ";
    private static final String TIME_HOUR = "ชั่วโมง ";
    private static final String TIME_MINUTE = "นาที ";


    /*
     ********************** CODING **********************
     */
    private static final int BETWEEN_gps = 15;
    private static final int THOUSAND_METER = 1000;

    public List<gps> item;

    public DistanceGPS(){
        item = new ArrayList();
        item.add(new gps(110.574,111.320));
        item.add(new gps(110.649,107.551));
        item.add(new gps(110.852,96.486));
        item.add(new gps(111.132,78.847));
        item.add(new gps(111.412,55.800));
        item.add(new gps(111.618,28.902));
        item.add(new gps(111.694,0.000));




    }

    public  String run(gps gps1,gps gps2){
       //gps gps1 = new gps(18.670077,99.0556043);
        ///gps gps2 = new gps(18.6459334,99.0574775);
        double distance = findDistance(gps1,gps2);
        return calculate_meter(distance);
    }

    public String calculate_meter(double distance){
        String out = "";
        if(distance>1000){
            out =  (int)Math.round(distance/1000)+"."+((int)Math.round(distance%1000))+DISTANCE_KM;
        }else{
            out =  Math.round(distance)+DISTANCE_METER;
        }
        return out;
    }


    public gps getSurfaceDistance(gps gps){
        return item.get((int)(gps.getLatitude()/BETWEEN_gps));
    }

    public double getLatitudeDistance(gps gps){
        gps surface_distance = getSurfaceDistance(gps);
        return surface_distance.getLatitude()*THOUSAND_METER;
    }

    public double getLongitudeDistance(gps gps){
        gps surface_distance = getSurfaceDistance(gps);
        return surface_distance.getLongitude()*THOUSAND_METER;
    }



    public double findDistance(gps gps1,gps gps2){
        double latitudeDistance1 = getLatitudeDistance(gps1);
        double latitudeDistance2 = getLatitudeDistance(gps2);
        double longitudeDistance1 = getLongitudeDistance(gps1);
        double longitudeDistance2 = getLongitudeDistance(gps2);

        double power1 = Math.pow((gps2.getLatitude()*latitudeDistance2)-(gps1.getLatitude()*latitudeDistance1),2);
        double power2 = Math.pow((gps2.getLongitude()*longitudeDistance2)-(gps1.getLongitude()*longitudeDistance1),2);


        return Math.sqrt(power1+power2);
    }


}

