package chiangmai.fang.monping.dev.adepter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import chiangmai.fang.monping.dev.R;
import chiangmai.fang.monping.dev.model.Context_view;
import chiangmai.fang.monping.dev.model.Ebook;
import chiangmai.fang.monping.dev.utils.AppUtils;
import chiangmai.fang.monping.dev.utils.Contact;
import chiangmai.fang.monping.dev.utils.RecycleViewClickListener;

public class FragmentAAdapter extends RecyclerView.Adapter<FragmentAAdapter.ViewHolder>  {

    private List<Context_view> item;
    private Context context;
    boolean isLoading = false, isMoreDataAvailable = true;
    RecycleViewClickListener listener;
    Spanned fromHtml;

    public FragmentAAdapter(Context context,List<Context_view> item, RecycleViewClickListener listener) {
        this.context = context;
        this.item = item;
        this.listener = listener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView name,detail,date;
        ImageView image;

        public ViewHolder(View view) {
            super(view);
            name = (TextView) itemView.findViewById(R.id.name);
            detail = (TextView) itemView.findViewById(R.id.detail);
            date = (TextView) itemView.findViewById(R.id.date);
            image = (ImageView) itemView.findViewById(R.id.image);
        }
    }


    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context)
                .inflate(R.layout.fragmentaadapter, viewGroup, false);


        ViewHolder viewHolder = new ViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(view, viewHolder.getPosition());
            }
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        // viewHolder.image.setImageResource(personImages.get(position));


            viewHolder.name.setText(item.get(i).getTopic_name()+"");
            viewHolder.detail.setText("    "+AppUtils.removeHTML(item.get(i).getContent1())+"");
            viewHolder.date.setText(AppUtils.fb_date1(item.get(i).getTopic_postdate()));
            Picasso.with(context).load(new Contact().Base_url+item.get(i).getPath_image()+"").placeholder(R.drawable.noimage).resize(300, 200).into(viewHolder.image);

    }



    @Override
    public int getItemCount() {
        return item.size();
    }
}

