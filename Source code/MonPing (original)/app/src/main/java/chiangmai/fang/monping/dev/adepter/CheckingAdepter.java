
package chiangmai.fang.monping.dev.adepter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import chiangmai.fang.monping.dev.R;
import chiangmai.fang.monping.dev.activity.MenuActivity;
import chiangmai.fang.monping.dev.model.Context_view;
import chiangmai.fang.monping.dev.model.Ebook;
import chiangmai.fang.monping.dev.model.checkin;
import chiangmai.fang.monping.dev.utils.AppUtils;
import chiangmai.fang.monping.dev.utils.Contact;
import chiangmai.fang.monping.dev.utils.RecycleViewClickListener;

public class CheckingAdepter extends RecyclerView.Adapter<CheckingAdepter.ViewHolder> {

    private List<checkin> item;
    private Context context;
    RecycleViewClickListener listener;

    public CheckingAdepter(Context context, List<checkin> item, RecycleViewClickListener listener) {
        this.context = context;
        this.listener = listener;
        this.item = item;
    }



    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView name,date,number_row;
        ImageView image;

        public ViewHolder(View view) {
            super(view);
            name = (TextView) itemView.findViewById(R.id.name);
            date = (TextView) itemView.findViewById(R.id.date);
            image = (ImageView) itemView.findViewById(R.id.image);
            number_row = (TextView)itemView.findViewById(R.id.number_row);
        }
    }


    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context)
                .inflate(R.layout.checkingadepter, viewGroup, false);


        ViewHolder viewHolder = new ViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(view, viewHolder.getPosition());
            }
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {


        viewHolder.name.setText(item.get(i).getFullname());
        viewHolder.date.setText(AppUtils.fb_date(item.get(i).getDate_plase()));
        viewHolder.number_row.setText("[ "+(i+1)+" ]");
        if(item.get(i).getType().equals("0")){
            Picasso.with(context).load(new Contact().Base_url+item.get(i).getPath_image()).into(viewHolder.image);
        }else{
            Picasso.with(context).load(item.get(i).getPath_image()).into(viewHolder.image);
        }


        // viewHolder.image.setImageResource(personImages.get(position));

    }


    @Override
    public int getItemCount() {
        try{
            return item.size();
        }catch (Exception e){ return 0;}
    }


}

