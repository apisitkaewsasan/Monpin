package chiangmai.fang.monping.dev.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.Login;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import chiangmai.fang.monping.dev.R;
import chiangmai.fang.monping.dev.model.LoginDTO;
import chiangmai.fang.monping.dev.model.login;
import chiangmai.fang.monping.dev.service.Api;
import chiangmai.fang.monping.dev.utils.UserManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.facebook.FacebookSdk.getApplicationContext;

public class FacebookLoginActivity extends AppCompatActivity {
    private static final String EMAIL = "email";
    private static final String USER_POSTS = "user_posts";
    private static final String AUTH_TYPE = "rerequest";
    private  static  final  String PUBLIC_ACTION = "publish_actions";
    private TextView username,password;
    private Button login;
    private CallbackManager mCallbackManager;
    private UserManager mManager;
    View focusView = null;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_facebook_login);
        username = (TextView)findViewById(R.id.username);
        password = (TextView)findViewById(R.id.password);
        login = (Button)findViewById(R.id.login);


        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "chiangmai.fang.monping.dev",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("TAG_KEY_HASH", Base64.encodeToString(md.digest(), Base64.DEFAULT));
                System.out.println("TAG_KEY_HASH "+Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }




        mManager  = new UserManager(this);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                check_empty();

            }
        });

        username.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                check_empty();
                if(id == R.id.next || id == EditorInfo.IME_NULL){

                    return true;
                }
                return false;
            }
        });

        password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                check_empty();
                if(id == R.id.next || id == EditorInfo.IME_NULL){

                    return true;
                }
                return false;
            }
        });


        mCallbackManager = CallbackManager.Factory.create();

        LoginButton mLoginButton = findViewById(R.id.login_button);

        TextView mRegisterButton = findViewById(R.id.create);

        mRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent loginIntent = new Intent(FacebookLoginActivity.this, ResigterActivity.class);
                startActivity(loginIntent);
               //overridePendingTransition(R.anim.scale_anim, R.anim.translate_anim);
            }
        });


        // Set the initial permissions to request from the user while logging in
       mLoginButton.setReadPermissions("email", "public_profile");

       mLoginButton.setAuthType(AUTH_TYPE);

        // Register a callback to respond to the user
        mLoginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                setResult(RESULT_OK);
                Toast.makeText(FacebookLoginActivity.this, "onSuccess", Toast.LENGTH_SHORT).show();
                Intent loginIntent = new Intent(FacebookLoginActivity.this, ProfileActivity.class);
                startActivity(loginIntent);
                finish();
            }

            @Override
            public void onCancel() {
                setResult(RESULT_CANCELED);
                //finish();
                Toast.makeText(FacebookLoginActivity.this, "onCancel", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException e) {
                // Handle exception
                System.out.println("onError "+e.getMessage());
                Toast.makeText(FacebookLoginActivity.this, "onError "+e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void login_from(String username,String password){
        final SharedPreferences mPrefs;

        mPrefs = this.getSharedPreferences("lend_system", Context.MODE_PRIVATE);

        final ProgressDialog dialog = ProgressDialog.show(this, "กรุณารอสักครู่  ...",	"กำลังบันทึกข้อมูลส่วนตัวของคุณกรุณารอสักครู่ ...", true);
        dialog.setCancelable(false);
        dialog.show();

        Api.getClient().login(username,password).enqueue(new Callback<LoginDTO>() {
            @Override
            public void onResponse(Call<LoginDTO> call, Response<LoginDTO> response) {

                List<login> item = response.body().getRetult();

                if(response.body().getRetrun_code().equals("00")){
                    dialog.dismiss();
                    mManager.LogoutUser();
                    mManager.registerUser(item.get(0).getUsername(),item.get(0).getPassword(),item.get(0).getFullname(),item.get(0).getTel(),item.get(0).getPath_image(),"P");
                    Intent loginIntent = new Intent(FacebookLoginActivity.this, Mainactivity.class);
                    startActivity(loginIntent);
                    finish();
                }else{
                    dialog.dismiss();
                    alert(response.body().getReturn_result());
                }


            }

            @Override
            public void onFailure(Call<LoginDTO> call, Throwable t) {
                dialog.dismiss();
                alert("onFailure "+t.getMessage());

            }
        });
    }

    public void alert(String message){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage(message);
        builder1.setCancelable(true);
        builder1.setTitle("ข้อผิดพลาด");
        builder1.setPositiveButton(
                "รับทราบ",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                });

        builder1.setNegativeButton(
                "ยกเลิก",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private  void check_empty(){
        username.setError(null);
        password.setError(null);

        if(TextUtils.isEmpty(username.getText())){
            username.setError(getString(R.string.error_field_required));
            focusView = username;
            focusView.requestFocus();
        }else if(TextUtils.isEmpty(password.getText())){
            password.setError(getString(R.string.error_field_required));
            focusView = password;
            focusView.requestFocus();
        }else{
            hideSoftKeyboard(FacebookLoginActivity.this);
            login_from(username.getText().toString(),password.getText().toString());
        }
    }

}

