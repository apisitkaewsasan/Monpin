package chiangmai.fang.monping.dev.adepter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import chiangmai.fang.monping.dev.R;
import chiangmai.fang.monping.dev.model.Context_view;
import chiangmai.fang.monping.dev.model.Ebook;
import chiangmai.fang.monping.dev.utils.AppUtils;
import chiangmai.fang.monping.dev.utils.Contact;
import chiangmai.fang.monping.dev.utils.RecycleViewClickListener;

public class FragmentCAdapter extends RecyclerView.Adapter<FragmentCAdapter.ViewHolder>  {

    private List<Context_view> item;
    private Context context;
    RecycleViewClickListener listener;
    boolean isLoading = false, isMoreDataAvailable = true;

    public FragmentCAdapter(Context context, List<Context_view> item, RecycleViewClickListener listener) {
        this.context = context;
        this.listener = listener;
        this.item = item;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView name,detail;
        ImageView image;

        public ViewHolder(View view) {
            super(view);
            name = (TextView) itemView.findViewById(R.id.name);
            image = (ImageView) itemView.findViewById(R.id.image);
            detail = (TextView) itemView.findViewById(R.id.detail);
        }
    }


    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context)
                .inflate(R.layout.fragmentcadapter, viewGroup, false);


        ViewHolder viewHolder = new ViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(view, viewHolder.getPosition());
            }
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {


        viewHolder.name.setText((i+1)+". "+item.get(i).getTopic_name());
        if(item.get(i).getContract_address()==null || item.get(i).getContract_address() == ""){
            viewHolder.detail.setText("  ไม่พบรายการ Config");
        }else{
            viewHolder.detail.setText("   "+item.get(i).getContract_address());
        }

        Picasso.with(context).load(new Contact().Base_url+item.get(i).getPath_image()).placeholder(R.drawable.noimage).resize(300, 200).into(viewHolder.image);


        // viewHolder.image.setImageResource(personImages.get(position));

    }


    @Override
    public int getItemCount() {
        return item.size();
    }
}


