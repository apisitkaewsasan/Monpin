package chiangmai.fang.monping.dev.utils;

import android.app.AlertDialog;
import android.app.Application;
import android.content.SharedPreferences;
import android.os.StrictMode;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.core.ImagePipelineConfig;
import com.facebook.imagepipeline.decoder.SimpleProgressiveJpegConfig;

import chiangmai.fang.monping.dev.R;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;


public class AppController extends Application {

    SharedPreferences pref;

    @Override
    public void onCreate() {
        super.onCreate();

       // setupLeakCanary();

        String className = this.getClass().getName();

        FacebookSdk.sdkInitialize(getApplicationContext());


        Fresco.initialize(this);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/RSU_Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        ImagePipelineConfig config = ImagePipelineConfig.newBuilder(this)
                .setProgressiveJpegConfig(new SimpleProgressiveJpegConfig())
                .setResizeAndRotateEnabledForNetwork(true)

                .build();
        Fresco.initialize(this, config);


    }



}
