package chiangmai.fang.monping.dev.model;

public class youtubelive {
    String  youtubeLive;
    String key_api;

    public youtubelive(String youtubeLive, String key_api) {
        this.youtubeLive = youtubeLive;
        this.key_api = key_api;
    }

    public String getYoutubeLive() {
        return youtubeLive;
    }

    public void setYoutubeLive(String youtubeLive) {
        this.youtubeLive = youtubeLive;
    }

    public String getKey_api() {
        return key_api;
    }

    public void setKey_api(String key_api) {
        this.key_api = key_api;
    }
}
