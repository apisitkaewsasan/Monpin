package chiangmai.fang.monping.dev.activity;


import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LevelListDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.YouTubePlayer;
import com.pixplicity.htmlcompat.HtmlCompat;
import com.squareup.picasso.Picasso;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import chiangmai.fang.monping.dev.R;
import chiangmai.fang.monping.dev.adepter.CheckingAdepter;
import chiangmai.fang.monping.dev.adepter.RecyclerViewCustomMore;
import chiangmai.fang.monping.dev.model.Context_view;
import chiangmai.fang.monping.dev.model.ListData;
import chiangmai.fang.monping.dev.model.Player;
import chiangmai.fang.monping.dev.model.checkin;
import chiangmai.fang.monping.dev.service.Api;
import chiangmai.fang.monping.dev.utils.Contact;
import chiangmai.fang.monping.dev.utils.EndlessRecyclerViewScrollListener;
import chiangmai.fang.monping.dev.utils.RecycleViewClickListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import com.google.android.youtube.player.YouTubeBaseActivity;

import org.xml.sax.Attributes;

import static android.content.ContentValues.TAG;
import static android.support.v4.content.ContextCompat.startActivity;

public class VideoActivity extends YouTubeBaseActivity implements com.google.android.youtube.player.YouTubePlayer.OnInitializedListener, HtmlCompat.ImageGetter{


    private ImageView ImageTitle,checkin;
    private ConstraintLayout foolter;
    private TextView title,context1,context2,textFont,header,time,Error,contract_name;
    private FrameLayout page_mask;
    private  ImageView FontSize,context_image,imgFavorite;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    public  String VIDEO_ID = "";
    Spanned fromHtml;
    private ScrollView main_scroller;
    View customView;
    MaterialStyledDialog.Builder dialogHeader;
    List<checkin> e;
    CheckingAdepter checkingadepter;
    private EndlessRecyclerViewScrollListener scrollListener;
    int Count_row=2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        setContentView(R.layout.activity_basic_example);
        Error = (TextView)findViewById(R.id.Error);
        page_mask = (FrameLayout)findViewById(R.id.page_mask);

            getData();



    }



    private void getData(){
        // display a progress dialog
        final ProgressDialog progressDialog = new ProgressDialog(VideoActivity.this);
        progressDialog.setCancelable(false); // set cancelable to false
        progressDialog.setMessage("กำลังโหลดข้อมูล...!"); // set message
        progressDialog.show(); // show progress dialog

        // Api is a class in which we define a method getClient() that returns the API Interface class object
        // getUsersList() is a method in API Interface class, in this method we define our API sub url

        Api.getClient().getViewVedio(getIntent().getStringExtra("Topic_id")).enqueue(new Callback<ListData>() {
            @Override
            public void onResponse(Call<ListData> call, Response<ListData> response) {
                try{
                    Uisetup(response.body().getContext_view().get(0));
                    UiMore(response.body().getVedio_rand());
                    page_mask.setVisibility(View.GONE);
                }catch (Exception e){
                    //Toast.makeText(VideoActivity.this, ""+e.getStackTrace(), Toast.LENGTH_SHORT).show();
                    Error.setText(e.getStackTrace()+"\n "+e.getMessage()+"\n"+" กรุณาติดต่อผู้พัฒนาแอพเคชั่น");
                }

                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ListData> call, Throwable t) {
                Toast.makeText(VideoActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
            }
        });

    }



    private void UiMore(List<Context_view> vedio){
        mRecyclerView = (RecyclerView) findViewById(R.id.RecyclerView1);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new RecyclerViewCustomMore(this, vedio);
        mRecyclerView.setAdapter(mAdapter);

    }
    private void Uisetup(Context_view Context) {
        ImageTitle = findViewById(R.id.ImageTitle);
        title = findViewById(R.id.title);
        context1 = findViewById(R.id.context1);
        context2 = findViewById(R.id.context2);
        textFont = findViewById(R.id.textFont);
        FontSize = findViewById(R.id.FontSize);
        header = findViewById(R.id.header);
        time = findViewById(R.id.time);
        checkin = findViewById(R.id.checkin);
        contract_name = (TextView) findViewById(R.id.contract_name);
        checkin.setImageDrawable(getResources().getDrawable(R.drawable.ic_add_location_black_24dp));
        context_image = findViewById(R.id.context_image);

        header.setText(Context.getTopic_name());
        time.setText("เวลา "+Context.getTime());

        fromHtml = HtmlCompat.fromHtml(this,Context.getTopic_name(),
                HtmlCompat.FROM_HTML_SEPARATOR_LINE_BREAK_LIST_ITEM,
                this);
        title.setMovementMethod(LinkMovementMethod.getInstance());
        title.setText(fromHtml);
        fromHtml = HtmlCompat.fromHtml(this,Context.getContent1(),
                HtmlCompat.FROM_HTML_SEPARATOR_LINE_BREAK_LIST_ITEM,
                this);
        context1.setMovementMethod(LinkMovementMethod.getInstance());
        context1.setText(fromHtml);

        fromHtml = HtmlCompat.fromHtml(this,Context.getContent2(),
                HtmlCompat.FROM_HTML_SEPARATOR_LINE_BREAK_LIST_ITEM,
                this);
        context2.setMovementMethod(LinkMovementMethod.getInstance());
        context2.setText(fromHtml);

        Picasso.with(this).load(new Contact().Base_url + Context.getPath_image()).into(context_image);

        Picasso.with(this).load(new Contact().get_image_youtube(Context.getYoutube_key())).into(ImageTitle);

        if(Context.getContract_name()== null || Context.getContract_name() == "" ){
            contract_name.setText(contract_name.getText() + " ไม่พบการ Config");

        }else{
            contract_name.setText(contract_name.getText() + " " + Context.getContract_name());

        }

        // Initializing YouTube player view
        com.google.android.youtube.player.YouTubePlayerView youTubePlayerView = (com.google.android.youtube.player.YouTubePlayerView) findViewById(R.id.youtube_player_view);
        youTubePlayerView.initialize(Context.getApi_key(), this);
        this.VIDEO_ID = Context.getYoutube_key();

        FontSize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MaterialDialog();
            }
        });

        checkin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                e = new ArrayList<>();
                checkin.setEnabled(false);
                load_checking(Count_row);

            }
        });
    }


    public void setDialogsRecyclerView(int index){
        // Button dismissButton = (Button)customView.findViewById(R.id.custom_button);
        dialogHeader(index,"การค้นหาข้อมูล","ไม่พบรายการค้นหา");

        // TextView header_text = (TextView)customView.findViewById(R.id.header_text);
        RecyclerView recycler = (RecyclerView)customView.findViewById(R.id.recycler);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this,1);
        recycler.setLayoutManager(gridLayoutManager); // set LayoutManager to RecyclerView
        recycler.setItemAnimator(new DefaultItemAnimator());
        // header_text.setText("บุคคลที่เคยมาเยี่ยมชม : "+Count_row +" คน");

        checkingadepter = new CheckingAdepter(this,e, new RecycleViewClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                //startActivity(new Intent(getContext().getApplicationContext(), MenuActivity.class).putExtra("activity","menu2"));
                //  statactivity(e.get(position).getTopic_id()+"");
            }
        });

        recycler.setAdapter(checkingadepter);
        scrollListener = new EndlessRecyclerViewScrollListener(gridLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {

                if(e.size()<index){
                    load_checking(e.size()+2);
                }
            }


        };
        recycler.addOnScrollListener(scrollListener);


    }

    public  void dialogHeader(int Count_row,String title,String Message){
        if(Count_row==0){
            LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            customView = inflater.inflate(R.layout.custom_view,null);
            dialogHeader = new MaterialStyledDialog.Builder(VideoActivity.this)
                    .setHeaderDrawable(R.drawable.map)
                    .setIcon(new IconicsDrawable(VideoActivity.this).icon(MaterialDesignIconic.Icon.gmi_map).color(Color.WHITE))
                    .withDialogAnimation(true)
                    .setCancelable(false)
                    .setTitle(title)
                    .setDescription(Message)
                    .setPositiveText("ปิดหน้าต่าง")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            // startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://github.com/javiersantos")));
                            checkin.setEnabled(true);
                        }
                    });
            dialogHeader.show();
        }else{
            LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            customView = inflater.inflate(R.layout.custom_view,null);
            dialogHeader = new MaterialStyledDialog.Builder(VideoActivity.this)
                    .setHeaderDrawable(R.drawable.map)
                    .setIcon(new IconicsDrawable(VideoActivity.this).icon(MaterialDesignIconic.Icon.gmi_map).color(Color.WHITE))
                    .withDialogAnimation(true)
                    .setCancelable(false)
                    .setTitle("บุคคลที่เคยมาเยี่ยมชม : "+Count_row +" คน")
                    .setCustomView(customView, 20, 0, 20, 0)
                    .setPositiveText("ปิดหน้าต่าง")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            // startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://github.com/javiersantos")));
                            checkin.setEnabled(true);
                        }
                    });
            dialogHeader.show();
        }

    }

    public void load_checking(int index){
        Api.getClient().getCheckIn(getIntent().getExtras().getString("topic_id"),index).enqueue(new Callback<ListData>() {
            @Override
            public void onResponse(Call<ListData> call, Response<ListData> response) {
                // Toast.makeText(MenuActivity.this, ""+response.body().getCheckin().size(), Toast.LENGTH_SHORT).show();
                if(response.body().getRetrun_code().equals("00")){
                    //  setDialogsRecyclerView(response.body().getCheckin(),response.body().getCount_row());
                    e.clear();
                    e.addAll(response.body().getCheckin());
                    // Count_row = response.body().getCount_row();
                    if(index==2){
                        setDialogsRecyclerView(response.body().getCount_row());
                    }
                    checkingadepter.notifyDataSetChanged();

                }else{
                    dialogHeader(0,"แจ้งจากระบบ",response.body().getReturn_result_e());
                }

            }

            @Override
            public void onFailure(Call<ListData> call, Throwable t) {

            }

        });

    }

    @Override
    public Drawable getDrawable(String source, Attributes attributes) {
        LevelListDrawable d = new LevelListDrawable();
        Drawable empty = getResources().getDrawable(R.drawable.ic_add_location_black_24dp);
        d.addLevel(0, 0, empty);
        d.setBounds(0, 0, empty.getIntrinsicWidth(), empty.getIntrinsicHeight());

        new LoadImage().execute(source, d);

        return d;
    }


    class LoadImage extends AsyncTask<Object, Void, Bitmap> {

        private LevelListDrawable mDrawable;

        @Override
        protected Bitmap doInBackground(Object... params) {
            String source = (String) params[0];
            mDrawable = (LevelListDrawable) params[1];
            Log.d(TAG, "doInBackground " + source);
            try {
                InputStream is = new URL(source).openStream();
                return BitmapFactory.decodeStream(is);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            Log.d(TAG, "onPostExecute drawable " + mDrawable);
            Log.d(TAG, "onPostExecute bitmap " + bitmap);
            if (bitmap != null) {
                BitmapDrawable d = new BitmapDrawable(bitmap);
                mDrawable.addLevel(1, 1, d);
                mDrawable.setBounds(0, 0, bitmap.getWidth(), bitmap.getHeight());
                mDrawable.setLevel(1);
                // i don't know yet a better way to refresh TextView
                // mTv.invalidate() doesn't work as expected
                CharSequence t = context2.getText();
                context2.setText(t);
            }
        }
    }


    public void MaterialDialog(){


        new MaterialDialog.Builder(this)
                .title("เลือกขนาดตัวหนังสือ")
                .iconRes(R.drawable.ic_font_24dp)
                .items(R.array.preference_values)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        //Toast.makeText(getApplicationContext(), which + ": " + text + ", ID = " + view.getId(), Toast.LENGTH_SHORT).show();
                        if(which == 0){
                            title.setTextSize(18);
                            context1.setTextSize(15);
                            context2.setTextSize(15);
                        }else if(which == 1){
                            title.setTextSize(19);
                            context1.setTextSize(16);
                            context2.setTextSize(16);
                        }else if(which == 2){
                            title.setTextSize(20);
                            context1.setTextSize(17);
                            context2.setTextSize(17);
                        }

                    }
                })
                .show();
    }

    @Override
    public void onInitializationSuccess(com.google.android.youtube.player.YouTubePlayer.Provider provider, com.google.android.youtube.player.YouTubePlayer youTubePlayer, boolean b) {


        if(null== youTubePlayer) return;

        // Start buffering
        if (!b) {
            youTubePlayer.cueVideo(VIDEO_ID);
        }



        // Add listeners to YouTubePlayer instance
        youTubePlayer.setPlayerStateChangeListener(new com.google.android.youtube.player.YouTubePlayer.PlayerStateChangeListener() {
            @Override public void onAdStarted() { }
            @Override public void onError(com.google.android.youtube.player.YouTubePlayer.ErrorReason arg0) { }
            @Override public void onLoaded(String arg0) { }
            @Override public void onLoading() { }
            @Override public void onVideoEnded() { }
            @Override public void onVideoStarted() { }
        });


        youTubePlayer.setPlaybackEventListener(new com.google.android.youtube.player.YouTubePlayer.PlaybackEventListener() {
            @Override public void onBuffering(boolean arg0) { }
            @Override public void onPaused() { }
            @Override public void onPlaying() { }
            @Override public void onSeekTo(int arg0) { }
            @Override public void onStopped() { }
        });
    }

    @Override
    public void onInitializationFailure(com.google.android.youtube.player.YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        Toast.makeText(this, "Failed to initialize.", Toast.LENGTH_LONG).show();

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


}

