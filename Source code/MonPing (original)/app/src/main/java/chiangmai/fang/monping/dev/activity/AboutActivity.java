package chiangmai.fang.monping.dev.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LevelListDrawable;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pixplicity.htmlcompat.HtmlCompat;
import com.squareup.picasso.Picasso;

import org.xml.sax.Attributes;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import chiangmai.fang.monping.dev.R;
import chiangmai.fang.monping.dev.model.About;
import chiangmai.fang.monping.dev.model.ListData;
import chiangmai.fang.monping.dev.model.contact;
import chiangmai.fang.monping.dev.service.Api;
import chiangmai.fang.monping.dev.utils.Contact;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AboutActivity extends AppCompatActivity implements HtmlCompat.ImageGetter {

    ImageView icon_show;
    TextView text_context1,text_context2,Error;
    private FrameLayout page_mask;
    Spanned fromHtml;
    private static final String TAG = AboutActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_chevron_left_black_24dp);
        setSupportActionBar(toolbar);


        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);



        Error = (TextView)findViewById(R.id.Error);
        page_mask = (FrameLayout)findViewById(R.id.page_mask);
        getData();
    }

    private void getData() {
        // display a progress dialog
        final ProgressDialog progressDialog = new ProgressDialog(AboutActivity.this);
        progressDialog.setCancelable(false); // set cancelable to false
        progressDialog.setMessage("Please Wait"); // set message
        progressDialog.show(); // show progress dialog

        // Api is a class in which we define a method getClient() that returns the API Interface class object
        // getUsersList() is a method in API Interface class, in this method we define our API sub url

        Api.getClient().getAboutView().enqueue(new Callback<ListData>() {
            @Override
            public void onResponse(Call<ListData> call, Response<ListData> response) {
                try{
                    setupUI(response.body().getView_about());
                    page_mask.setVisibility(View.GONE);
                }catch (Exception e){
                    page_mask.setVisibility(View.VISIBLE);
                    Error.setText("Error -> "+e.getMessage()+"\n ไม่เนื้อพบหาข้อมูลในหน้านี้ "+" กรุณาติดต่อผู้พัฒนาแอพเคชั่น");
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ListData> call, Throwable t) {
                Toast.makeText(AboutActivity.this,t.getMessage(), Toast.LENGTH_LONG).show();
                progressDialog.dismiss(); //dismiss progress dialog
            }
        });


    }

    public void setupUI(List<About> list){
        icon_show = (ImageView)findViewById(R.id.icon_show);
        text_context1 = (TextView)findViewById(R.id.text_context1);
        text_context2 = (TextView)findViewById(R.id.text_context2);
        Picasso.with(this).load(new Contact().Base_url+list.get(0).getPath_image()).resize(800, 400).into(icon_show);

        fromHtml = HtmlCompat.fromHtml(this,list.get(0).getContent1(),
                HtmlCompat.FROM_HTML_SEPARATOR_LINE_BREAK_LIST_ITEM,
                this);
        text_context1.setMovementMethod(LinkMovementMethod.getInstance());
        text_context1.setText(fromHtml);

        fromHtml = HtmlCompat.fromHtml(this,list.get(0).getContent2(),
                HtmlCompat.FROM_HTML_SEPARATOR_LINE_BREAK_LIST_ITEM,
                this);
        text_context2.setMovementMethod(LinkMovementMethod.getInstance());
        text_context2.setText(fromHtml);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public Drawable getDrawable(String source, Attributes attributes) {
        LevelListDrawable d = new LevelListDrawable();
        Drawable empty = getResources().getDrawable(R.drawable.ic_add_location_black_24dp);
        d.addLevel(0, 0, empty);
        d.setBounds(0, 0, empty.getIntrinsicWidth(), empty.getIntrinsicHeight());

        new LoadImage().execute(source, d);

        return d;
    }

    class LoadImage extends AsyncTask<Object, Void, Bitmap> {

        private LevelListDrawable mDrawable;

        @Override
        protected Bitmap doInBackground(Object... params) {
            String source = (String) params[0];
            mDrawable = (LevelListDrawable) params[1];
            Log.d(TAG, "doInBackground " + source);
            try {
                InputStream is = new URL(source).openStream();
                return BitmapFactory.decodeStream(is);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            Log.d(TAG, "onPostExecute drawable " + mDrawable);
            Log.d(TAG, "onPostExecute bitmap " + bitmap);
            if (bitmap != null) {
                BitmapDrawable d = new BitmapDrawable(bitmap);
                mDrawable.addLevel(1, 1, d);
                mDrawable.setBounds(0, 0, bitmap.getWidth(), bitmap.getHeight());
                mDrawable.setLevel(1);
                // i don't know yet a better way to refresh TextView
                // mTv.invalidate() doesn't work as expected
                CharSequence t1 = text_context1.getText();
                text_context1.setText(t1);
                CharSequence t2 = text_context2.getText();
                text_context2.setText(t2);
            }
        }
    }
}
