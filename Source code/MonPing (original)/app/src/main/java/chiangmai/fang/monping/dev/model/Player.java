package chiangmai.fang.monping.dev.model;

public class Player {
    private String path_image;
    private String clname_imageub;

    public Player(String path_image, String clname_imageub) {
        this.path_image = path_image;
        this.clname_imageub = clname_imageub;
    }

    public String getPath_image() {
        return path_image;
    }

    public void setPath_image(String path_image) {
        this.path_image = path_image;
    }

    public String getClname_imageub() {
        return clname_imageub;
    }

    public void setClname_imageub(String clname_imageub) {
        this.clname_imageub = clname_imageub;
    }
}
