package chiangmai.fang.monping.dev.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import chiangmai.fang.monping.dev.model.login;

public class UserManager {


    private final String KEY_PREFS = "lend_system";


    private final String KEY_USERNAME = "username";

    private final String KEY_PASSWORD = "password";
    private final String KEY_FULLNAME = "fullname";
    private final String KEY_TEL = "tel";
    private final String KEY_IMAGE = "image";
    private final String KEY_FLAUG = "flaug";
    private final String KEY_LATITUDE = "latitude";
    private final String KEY_LONGITUDE = "longitude";
    private SharedPreferences mPrefs;
    private SharedPreferences.Editor mEditor;


    public UserManager(Context context) {
        mPrefs = context.getSharedPreferences(KEY_PREFS, Context.MODE_PRIVATE);
        mEditor = mPrefs.edit();
    }


    public boolean registerUser(String username, String password,String fullname,String tel,String image,String flaug) {


        mEditor.putString(KEY_USERNAME, username);
        mEditor.putString(KEY_PASSWORD, password);
        mEditor.putString(KEY_FULLNAME, fullname);
        mEditor.putString(KEY_TEL, tel);
        mEditor.putString(KEY_IMAGE, image);
        mEditor.putString(KEY_FLAUG, flaug);
        return mEditor.commit();
    }

    public boolean insert_location(String latitude,String longitude){
        mEditor.putString(KEY_LATITUDE, latitude);
        mEditor.putString(KEY_LONGITUDE, longitude);
        return mEditor.commit();

    }

    public boolean LogoutUser(){
        mEditor.remove(KEY_USERNAME);
        mEditor.remove(KEY_PASSWORD);
        mEditor.remove(KEY_FULLNAME);
        mEditor.remove(KEY_TEL);
        mEditor.remove(KEY_IMAGE);
        mEditor.remove(KEY_FLAUG);
        return mEditor.commit();
    }


}
