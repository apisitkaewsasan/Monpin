package chiangmai.fang.monping.dev.service;


import chiangmai.fang.monping.dev.model.ListData;
import chiangmai.fang.monping.dev.model.ReturnMessage;
import chiangmai.fang.monping.dev.model.LoginDTO;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIService {
    @GET("list")
    Call<ListData> getEbook();

    @GET("Live")
    Call<ListData> getLive();

    @GET("CheckIn")
    Call<ListData> getCheckIn(@Query("topic_id") String topic_id,@Query("index") int index);

    @GET("Location")
    Call<ListData> getLocation(@Query("username") String username,@Query("index") int index);

    @GET("AllEbook")
    Call<ListData> getAllEbook(@Query("index") int index);

    @GET("AllVedio")
    Call<ListData> getAllVedio(@Query("index") int index);

    @GET("SyncScanner/{id}")
    Call<ListData> SyncScanner(@Path("id") String id,@Query("username") String username);

    @GET("EbookView/{id}")
    Call<ListData> getEbookView(@Path("id") String id);

    @GET("ViewVedio/{id}")
    Call<ListData> getViewVedio(@Path("id") String id);

    @GET("ContactView")
    Call<ListData> getContactView();

    @GET("AboutView")
    Call<ListData> getAboutView();

    @GET("MainMenu/{menu}")
    Call<ListData> getMenuMain(@Path("menu")String menu,@Query("index") int index);

    @GET("ViewMenu/{menu}")
    Call<ListData> getMenu(@Path("menu")String menu);

    @GET("menu.php")
    Call<ListData> GetDemo();

    @GET("Login")
    Call<LoginDTO> login(@Query("username") String username, @Query("password") String password);

    @GET("Register_facebook")
    Call<ReturnMessage> login_facebook(@Query("username") String username, @Query("fullname") String fullname, @Query("password") String password, @Query("tel") String tel,@Query("image_url") String image_url);

    @Multipart
    @POST("Register")
    Call<ReturnMessage> uploadImage(@Part MultipartBody.Part file,@Query("username") String username, @Query("fullname") String fullname, @Query("password") String password, @Query("tel") String tel);

}
