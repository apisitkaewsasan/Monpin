package chiangmai.fang.monping.dev.adepter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import chiangmai.fang.monping.dev.R;
import chiangmai.fang.monping.dev.activity.VideoActivity;
import chiangmai.fang.monping.dev.model.Context_view;
import chiangmai.fang.monping.dev.model.Player;
import chiangmai.fang.monping.dev.utils.AppUtils;
import chiangmai.fang.monping.dev.utils.Contact;

import static android.support.v4.content.ContextCompat.startActivity;

public class RecyclerViewCustomMore extends RecyclerView.Adapter<RecyclerViewCustomMore.ViewHolder>  {

    private List<Context_view> mContext_view;
    private Context mContext;


    public RecyclerViewCustomMore(Context context, List<Context_view> dataset) {
        mContext_view = dataset;
        mContext = context;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView ImageTitle1;
        public TextView more_title,more_date,more_time;
        private LinearLayout mLinearLayout;

        public ViewHolder(View view) {
            super(view);
            ImageTitle1 = (ImageView) view.findViewById(R.id.ImageTitle1);
            more_title = (TextView) view.findViewById(R.id.more_title);
            more_date = (TextView) view.findViewById(R.id.more_date);
            more_time = (TextView) view.findViewById(R.id.more_time);
            mLinearLayout = (LinearLayout)view.findViewById(R.id.main_more);

        }
    }


    public RecyclerViewCustomMore.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.recycler_view_row_more, viewGroup, false);

        RecyclerViewCustomMore.ViewHolder viewHolder = new RecyclerViewCustomMore.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewCustomMore.ViewHolder viewHolder, int i) {
        Context_view context_view = mContext_view.get(i);
        Picasso.with(mContext).load(new Contact().get_image_youtube(context_view.getYoutube_key())).placeholder(R.drawable.noimage).into(viewHolder.ImageTitle1);
        viewHolder.more_title.setText(context_view.getTopic_name());
        viewHolder.more_date.setText(AppUtils.fb_date1(context_view.getTopic_postdate()));
        viewHolder.more_time.setText(context_view.getTime());
        viewHolder.mLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(mContext, "OK "+context_view.getYoutube_key(), Toast.LENGTH_SHORT).show();
                ((VideoActivity)mContext).finish();
                Intent intent= new Intent(mContext, VideoActivity.class);
                intent.putExtra("Topic_id",context_view.getTopic_id()+"");
                mContext.startActivity(intent);
            }
        });
    }





    @Override
    public int getItemCount() {
        return mContext_view.size();
    }
}

